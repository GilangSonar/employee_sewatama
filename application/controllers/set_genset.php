<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 6/9/14
 * Time: 5:03 PM
 */
class Set_genset extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('login_status') != TRUE ){
            $this->session->set_flashdata('notif','LOGIN FAILED ! USERNAME OR PASSWORD IS NOT VALID');
            redirect('');
        };
        $this->load->model('custom_model');
    }

    function index(){
        $data=array(
            'title'=>'Setting Genset',
            'act_setting'=>true,
            'act_genset'=>true,
            'css_datatable'=>true,
            'js_datatable'=>true,
            'css_select2'=>true,
            'js_select2'=>true,

            'dt_genset'=>$this->custom_model->getAllData('view_genset'),
            'dt_brand'=>$this->custom_model->getAllData('t_brand'),
        );
        $this->load->view('element/_header',$data);
        $this->load->view('element/_notif');
        $this->load->view('pages/setting_genset/_genset');
        $this->load->view('element/_footer');
    }

    function edit_pages(){
        $id['id_genset']=$this->uri->segment(3);
        $data=array(
            'title'=>'Setting Genset',
            'act_setting'=>true,
            'act_genset'=>true,
            'css_select2'=>true,
            'js_select2'=>true,

            'dt_genset'=>$this->custom_model->getSelectedData('t_genset',$id)->result(),
            'dt_brand'=>$this->custom_model->getAllData('t_brand'),
        );
        $this->load->view('element/_header',$data);
        $this->load->view('pages/setting_genset/_edit_genset');
        $this->load->view('element/_footer');
    }

    function input_data(){
        $data=array(
            'id_brand'=>$this->input->post('id_brand'),
            'sn_genset'=>$this->input->post('sn_genset'),
            'model'=>$this->input->post('model'),
            'spec'=>$this->input->post('spec'),
            'capacity'=>$this->input->post('capacity'),
        );
        $this->custom_model->insertData('t_genset',$data);
        $this->session->set_flashdata('success_add',true);
        redirect('set_genset');
    }

    function update_data(){
        $id['id_genset']=$this->input->post('id_genset');
        $data=array(
            'id_brand'=>$this->input->post('id_brand'),
            'sn_genset'=>$this->input->post('sn_genset'),
            'model'=>$this->input->post('model'),
            'spec'=>$this->input->post('spec'),
            'capacity'=>$this->input->post('capacity'),
        );
        $this->custom_model->updateData('t_genset',$data,$id);
        $this->session->set_flashdata('success_update',true);
        redirect('set_genset');
    }

    function delete_data(){
        $id['id_genset'] = $this->uri->segment(3);
        $this->custom_model->deleteData('t_genset',$id);
        $this->session->set_flashdata('success_delete',true);
        redirect("set_genset");
    }


    //    BRAND
    function input_data_brand(){
        $data=array(
            'brand'=>$this->input->post('brand'),
        );
        $this->custom_model->insertData('t_brand',$data);
        $this->session->set_flashdata('success_add',true);
        $this->session->set_flashdata('act_tab_brand',true);
        redirect('set_genset');
    }

    function update_data_brand(){
        $id['id_brand']=$this->input->post('id_brand');
        $data=array(
            'brand'=>$this->input->post('brand'),
        );
        $this->custom_model->updateData('t_brand',$data,$id);
        $this->session->set_flashdata('success_update',true);
        $this->session->set_flashdata('act_tab_brand',true);
        redirect('set_genset');
    }

    function delete_data_brand(){
        $id['id_brand'] = $this->uri->segment(3);
        $this->custom_model->deleteData('t_brand',$id);
        $this->session->set_flashdata('success_delete',true);
        $this->session->set_flashdata('act_tab_brand',true);
        redirect("set_genset");
    }
}
