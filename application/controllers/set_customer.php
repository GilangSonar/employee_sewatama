<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 6/9/14
 * Time: 5:03 PM
 */
class Set_customer extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('login_status') != TRUE ){
            $this->session->set_flashdata('notif','LOGIN FAILED ! USERNAME OR PASSWORD IS NOT VALID');
            redirect('');
        };
        $this->load->model('custom_model');
    }

    function index(){
        $data=array(
            'title'=>'Setting Customer',
            'act_setting'=>true,
            'act_customer'=>true,
            'css_datatable'=>true,
            'js_datatable'=>true,

            'dt_customer'=>$this->custom_model->getAllDataSorting('t_customer','id_customer','asc'),
        );
        $this->load->view('element/_header',$data);
        $this->load->view('element/_notif');
        $this->load->view('pages/setting_customer/_customer');
        $this->load->view('element/_footer');
    }

    function edit_pages(){
        $id['id_customer']=$this->uri->segment(3);
        $data=array(
            'title'=>'Setting Customer',
            'act_setting'=>true,
            'act_customer'=>true,

            'dt_customer'=>$this->custom_model->getSelectedData('t_customer',$id)->result(),
        );
        $this->load->view('element/_header',$data);
        $this->load->view('pages/setting_customer/_edit_customer');
        $this->load->view('element/_footer');
    }

    function input_data(){
        $data=array(
            'customer'=>$this->input->post('customer'),
            'email'=>$this->input->post('email'),
            'phone'=>$this->input->post('phone'),
            'address'=>$this->input->post('address'),
        );
        $this->custom_model->insertData('t_customer',$data);
        $this->session->set_flashdata('success_add',true);
        redirect('set_customer');
    }

    function update_data(){
        $id['id_customer']=$this->input->post('id_customer');
        $data=array(
            'customer'=>$this->input->post('customer'),
            'email'=>$this->input->post('email'),
            'phone'=>$this->input->post('phone'),
            'address'=>$this->input->post('address'),
        );
        $this->custom_model->updateData('t_customer',$data,$id);
        $this->session->set_flashdata('success_update',true);
        redirect('set_customer');
    }

    function delete_data(){
        $id['id_customer'] = $this->uri->segment(3);
        $this->custom_model->deleteData('t_customer',$id);
        $this->session->set_flashdata('success_delete',true);
        redirect("set_customer");
    }

}
