<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 6/9/14
 * Time: 3:03 PM
 */
class Login extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('custom_model');
    }

    function index(){
        if($this->session->userdata('login_status') == TRUE ){
            redirect('home');
        };
        $data=array(
            'title'=>'Sign In'
        );
        $this->load->view('pages/_login',$data);
    }
    function lock_pages(){
        $this->session->unset_userdata('ID');
        $this->session->unset_userdata('PASS');
        $this->session->unset_userdata('NAME');
        $this->session->unset_userdata('IMG');
        $this->session->unset_userdata('login_status');
        $id['id_employee']=  $this->session->userdata('ID');
        $data=array(
            'title'=>'Lock Account',
        );
        $this->load->view('pages/_lock_pages',$data);
    }

    function cek_login() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $result = $this->custom_model->login($username, $password);
        if($result) {
            $sess_array = array();
            foreach($result as $row) {
                $sess_array = array(
                    'ID' => $row->id_user,
                    'USERNAME' => $row->username,
                    'PASS'=>$row->password,
                    'NAME'=>$row->name,
                    'IMG'=>$row->user_img,
                    'login_status'=>true,
                );
                $this->session->set_userdata($sess_array);
                redirect('home');
            }
        } else {
            redirect('login/logout');
        }
    }

    function logout() {
        $this->session->unset_userdata('ID');
        $this->session->unset_userdata('USERNAME');
        $this->session->unset_userdata('PASS');
        $this->session->unset_userdata('NAME');
        $this->session->unset_userdata('IMG');
        $this->session->unset_userdata('login_status');
        $this->session->set_flashdata('notif','THANK YOU FOR LOGIN IN THIS APP');
        redirect('login');
    }
}