<?php
/**
 * Created by PhpStorm.
 * User: patriciahadi
 * Date: 7/9/14
 * Time: 9:51 PM
 */

class Files extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('custom_model');
        $this->load->helper('download');
    }

    function index(){
        $data=array(
            'title'=>'Employee Files',
            'act_files'=>true,
            'css_datatable'=>true,
            'js_datatable'=>true,
            'css_select2'=>true,
            'js_select2'=>true,
            'js_fileinput'=>true,

            'dt_files'=>$this->custom_model->getDataFiles(),
            'dt_employee'=>$this->custom_model->getAllData('t_employee'),
        );
        $this->load->view('element/_header',$data);
        $this->load->view('element/_notif');
        $this->load->view('pages/files/_files');
        $this->load->view('element/_footer');
    }

    function edit_pages(){
        $id = $this->uri->segment(3);
        $data=array(
            'title'=>'Update Employee Files',
            'act_files'=>true,
            'css_select2'=>true,
            'js_select2'=>true,
            'js_fileinput'=>true,

            'dt_files'=>$this->custom_model->getDataFilesById($id),
            'dt_employee'=>$this->custom_model->getAllData('t_employee'),
        );
        $this->load->view('element/_header',$data);
        $this->load->view('element/_notif');
        $this->load->view('pages/files/_edit_files');
        $this->load->view('element/_footer');
    }

    function upload_file(){
        $config['upload_path'] = './uploads/files/';
        $config['allowed_types'] = 'rar|zip';
        $this->load->library('upload',$config);
        $this->upload->initialize($config);

        if (! $this->upload->do_upload()){
            $this->session->set_flashdata('failed_add',true);
            redirect("files");
        }else{
            $file =  $this->upload->data();
            $namaFile=$file['file_name'];

            $data = array(
                'id_employee'=>$this->input->post('id_employee'),
                'desc'=>$this->input->post('desc'),
                'update'=>date("Y-m-d",strtotime($this->input->post('update'))),
                'files'=>$namaFile,
            );
            $this->custom_model->insertData('t_files',$data);
            $this->session->set_flashdata('success_add',true);
            redirect("files");
        }
    }

    function update_upload_file(){
        $id['id_files']= $this->input->post('id_files');
        $config['upload_path'] = './uploads/files/';
        $config['allowed_types'] = 'rar|zip';
        $this->load->library('upload',$config);
        $this->upload->initialize($config);

        if (! $this->upload->do_upload()){
            $data=array(
                'id_employee'=>$this->input->post('id_employee'),
                'desc'=>$this->input->post('desc'),
                'update'=>date("Y-m-d",strtotime($this->input->post('update'))),
            );
            $this->custom_model->updateData('t_files',$data,$id);
            $this->session->set_flashdata('success_update',true);
            redirect("files");
        }else{
            $this->db->select('files')->from('t_files')->where($id);
            $query = $this->db->get();
            if($query->num_rows() > 0){
                $row = $query->row();
                $file = 'uploads/files/'.$row->files ;
                @unlink($file);
            }
            $file =  $this->upload->data();
            $namaFile=$file['file_name'];
            $data = array(
                'id_employee'=>$this->input->post('id_employee'),
                'desc'=>$this->input->post('desc'),
                'update'=>date("Y-m-d",strtotime($this->input->post('update'))),
                'files'=>$namaFile,
            );
            $this->custom_model->updateData('t_files',$data,$id);
            $this->session->set_flashdata('success_update',true);
            redirect("files");
        }
    }

    function download_file() {
        $file = $this->uri->segment(3);
        $data = file_get_contents("./uploads/files/".$file);
        $name = 'download.zip';
        force_download($name, $data);
    }

    function delete_data(){
        $id['id_files'] = $this->uri->segment(3);
        $this->db->select('files')->from('t_files')->where($id);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $row = $query->row();
            $file = 'uploads/files/'.$row->files ;
            @unlink($file);
        }
        $this->custom_model->deleteData('t_files',$id);
        $this->session->set_flashdata('success_delete',true);
        redirect("files");
    }
}