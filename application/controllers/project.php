<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 6/9/14
 * Time: 5:03 PM
 */
class Project extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('login_status') != TRUE ){
            $this->session->set_flashdata('notif','LOGIN FAILED ! USERNAME OR PASSWORD IS NOT VALID');
            redirect('');
        };
        $this->load->model('custom_model');
    }

    function index(){
        $data=array(
            'title'=>'Project / Retail',
            'act_project'=>true,
            'css_datatable'=>true,
            'js_datatable'=>true,
            'css_select2'=>true,
            'js_select2'=>true,
            'css_datepicker'=>true,
            'js_datepicker'=>true,

            'id_project'=>$this->custom_model->kodeProject(),
            'dt_employee'=>$this->custom_model->getAllData('t_employee'),
            'dt_customer'=>$this->custom_model->getAllData('t_customer'),
            'dt_brand'=>$this->custom_model->getAllData('t_brand'),
            'dt_genset'=>$this->custom_model->getAllData('view_genset'),
            'dt_kota'=>$this->custom_model->getAllData('t_kota'),
            'dt_project'=>$this->custom_model->getProjectData(),

        );
        $this->load->view('element/_header',$data);
        $this->load->view('element/_notif');
        $this->load->view('pages/project/_project');
        $this->load->view('element/_footer');
    }

    function view_pages(){
        $id=$this->uri->segment(3);
        $data=array(
            'title'=>'Project / Retail',
            'act_project'=>true,

            'dt_project'=>$this->custom_model->getProjectDataByID($id),
            'dt_project_employee'=>$this->custom_model->getProjectEmployeeByID($id),
            'dt_project_genset'=>$this->custom_model->getProjectGensetByID($id),
        );
        $this->load->view('element/_header',$data);
        $this->load->view('pages/project/_view_project');
        $this->load->view('element/_footer');
    }

    function edit_pages(){
        $id=$this->uri->segment(3);
        $data=array(
            'title'=>'Project / Retail',
            'act_project'=>true,
            'css_datatable'=>true,
            'js_datatable'=>true,
            'css_select2'=>true,
            'js_select2'=>true,
            'css_datepicker'=>true,
            'js_datepicker'=>true,

            'dt_project'=>$this->custom_model->getProjectDataByID($id),
            'dt_project_employee'=>$this->custom_model->getProjectEmployeeByID($id),
            'dt_project_genset'=>$this->custom_model->getProjectGensetByID($id),

            'dt_customer'=>$this->custom_model->getAllData('t_customer'),
            'dt_employee'=>$this->custom_model->getAllData('t_employee'),
            'dt_genset'=>$this->custom_model->getAllData('view_genset'),
            'dt_kota'=>$this->custom_model->getAllData('t_kota'),

        );
        $this->load->view('element/_header',$data);
        $this->load->view('element/_notif');
        $this->load->view('pages/project/_edit_project');
        $this->load->view('element/_footer');
    }

    function input_data(){
        if($this->input->post('id_employee') != null && $this->input->post('id_genset') != null ){

            $employee=$this->input->post('id_employee');
            foreach( $employee as $row_emp ){
                $prj_emp =array(
                    'id_project'=> $this->input->post('id_project'),
                    'id_employee'=>$row_emp
                );
                $this->custom_model->insertData('t_project_employee',$prj_emp);
            }

            $genset = $this->input->post('id_genset');
            foreach( $genset as $row_genset ){
                $prj_genset =array(
                    'id_project'=> $this->input->post('id_project'),
                    'id_genset'=>$row_genset
                );
                $this->custom_model->insertData('t_project_genset',$prj_genset);
            }
            $data=array(
                'id_project'=>$this->input->post('id_project'),
                'id_customer'=>$this->input->post('id_customer'),
                'start_date'=> date("Y-m-d",strtotime($this->input->post('start_date'))) ,
                'end_date'=> date("Y-m-d",strtotime($this->input->post('end_date'))),
                'lokasi'=>$this->input->post('lokasi'),
                'utility'=>$this->input->post('utility'),
                'update'=>date("Y-m-d",strtotime($this->input->post('update'))),
                'user_update'=>$this->input->post('user_update'),
            );
            $this->custom_model->insertData('t_project',$data);
            $this->session->set_flashdata('success_add',true);
            redirect('project');
        }
    }

    function update_data(){
        $idProject['id_project']=$this->input->post('id_project');

        if( $this->input->post('id_employee') != null && $this->input->post('id_genset') != null){

            $this->custom_model->deleteData('t_project_employee',$idProject);
            $employee=$this->input->post('id_employee');
            foreach( $employee as $row_emp ){
                $prj_emp =array(
                    'id_project'=> $this->input->post('id_project'),
                    'id_employee'=>$row_emp
                );
                $this->custom_model->insertData('t_project_employee',$prj_emp);
            }

            $this->custom_model->deleteData('t_project_genset',$idProject);
            $genset = $this->input->post('id_genset');
            foreach( $genset as $row_genset ){
                $prj_genset =array(
                    'id_project'=> $this->input->post('id_project'),
                    'id_genset'=>$row_genset
                );
                $this->custom_model->insertData('t_project_genset',$prj_genset);
            }
            $data=array(
                'id_customer'=>$this->input->post('id_customer'),
                'start_date'=> date("Y-m-d",strtotime($this->input->post('start_date'))) ,
                'end_date'=> date("Y-m-d",strtotime($this->input->post('end_date'))),
                'lokasi'=>$this->input->post('lokasi'),
                'utility'=>$this->input->post('utility'),
                'update'=>date("Y-m-d",strtotime($this->input->post('update'))),
                'user_update'=>$this->input->post('user_update'),
            );
            $this->custom_model->updateData('t_project',$data,$idProject);
            $this->session->set_flashdata('success_update',true);
            redirect('project/view_pages/'.$this->input->post('id_project'));

        }elseif($this->input->post('id_employee') != null && $this->input->post('id_genset') == null ){

            $this->custom_model->deleteData('t_project_employee',$idProject);
            $employee=$this->input->post('id_employee');
            foreach( $employee as $row_emp ){
                $prj_emp =array(
                    'id_project'=> $this->input->post('id_project'),
                    'id_employee'=>$row_emp
                );
                $this->custom_model->insertData('t_project_employee',$prj_emp);
            }
            $data=array(
                'id_customer'=>$this->input->post('id_customer'),
                'start_date'=> date("Y-m-d",strtotime($this->input->post('start_date'))) ,
                'end_date'=> date("Y-m-d",strtotime($this->input->post('end_date'))),
                'lokasi'=>$this->input->post('lokasi'),
                'utility'=>$this->input->post('utility'),
                'update'=>date("Y-m-d",strtotime($this->input->post('update'))),
                'user_update'=>$this->input->post('user_update'),
            );
            $this->custom_model->updateData('t_project',$data,$idProject);
            $this->session->set_flashdata('success_update',true);
            redirect('project/view_pages/'.$this->input->post('id_project'));

        }elseif($this->input->post('id_employee') == null && $this->input->post('id_genset') != null ){

            $this->custom_model->deleteData('t_project_genset',$idProject);
            $genset = $this->input->post('id_genset');
            foreach( $genset as $row_genset ){
                $prj_genset =array(
                    'id_project'=> $this->input->post('id_project'),
                    'id_genset'=>$row_genset
                );
                $this->custom_model->insertData('t_project_genset',$prj_genset);
            }
            $data=array(
                'id_customer'=>$this->input->post('id_customer'),
                'start_date'=> date("Y-m-d",strtotime($this->input->post('start_date'))) ,
                'end_date'=> date("Y-m-d",strtotime($this->input->post('end_date'))),
                'lokasi'=>$this->input->post('lokasi'),
                'utility'=>$this->input->post('utility'),
                'update'=>date("Y-m-d",strtotime($this->input->post('update'))),
                'user_update'=>$this->input->post('user_update'),
            );
            $this->custom_model->updateData('t_project',$data,$idProject);
            $this->session->set_flashdata('success_update',true);
            redirect('project/view_pages/'.$this->input->post('id_project'));

        }elseif($this->input->post('id_employee') == null && $this->input->post('id_genset') == null ){

            $data=array(
                'id_customer'=>$this->input->post('id_customer'),
                'start_date'=> date("Y-m-d",strtotime($this->input->post('start_date'))) ,
                'end_date'=> date("Y-m-d",strtotime($this->input->post('end_date'))),
                'lokasi'=>$this->input->post('lokasi'),
                'utility'=>$this->input->post('utility'),
                'update'=>date("Y-m-d",strtotime($this->input->post('update'))),
                'user_update'=>$this->input->post('user_update'),
            );
            $this->custom_model->updateData('t_project',$data,$idProject);
            $this->session->set_flashdata('success_update',true);
            redirect('project/view_pages/'.$this->input->post('id_project'));
        }
    }

    function delete_data(){
        $id['id_project'] = $this->uri->segment(3);
        $table=array('t_project','t_project_employee','t_project_genset');
        $this->custom_model->deleteData($table,$id);
        $this->session->set_flashdata('success_delete',true);
        redirect("project");
    }


    //    AJAX LOAD
    function ajaxLoadCust(){
        $id['id_customer'] = $this->input->post('id_customer');
        $data=array(
            'ajax_customer'=>$this->custom_model->getSelectedData('t_customer',$id)->result(),
        );
        $this->load->view('pages/project/_detail_customer',$data);
    }

//    //    PDF
//    function exportAll(){
//        $this->load->helper(array('dompdf', 'file'));
//        $data=array(
//            'dt_project'=>$this->custom_model->getProjectData(),
//        );
//        $html = $this->load->view('pages/project/_pdf_all_data',$data);
//        $data = pdf_create($html, '', false);
//        write_file('Project', $data);
//    }
//
//    function mPDF() {
//        $data['dt_project'] = $this->custom_model->getProjectData();
//        $html = $this->load->view("pages/project/_pdf_all_data",$data,TRUE);
//
//        $this->load->library('mpdf/mpdf');
//        $this->mpdf=new mPDF('c','A4','','',32,25,27,25,16,13);
//        $this->mpdf->WriteHTML($html,2);
//        $this->mpdf->Output('project','I');
//    }

}
