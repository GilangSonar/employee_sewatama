<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 6/9/14
 * Time: 5:03 PM
 */
class Employee extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('login_status') != TRUE ){
            $this->session->set_flashdata('notif','LOGIN FAILED ! USERNAME OR PASSWORD IS NOT VALID');
            redirect('');
        };
        $this->load->model('custom_model');
    }

    function index(){
        $data=array(
            'title'=>'Employee Data',
            'act_emp'=>true,
            'css_datatable'=>true,
            'js_datatable'=>true,
            'css_fuelux'=>true,
            'js_fuelux'=>true,
            'js_parsley'=>true,
            'js_combodate'=>true,
            'css_select2'=>true,
            'js_select2'=>true,
            'css_datepicker'=>true,
            'js_datepicker'=>true,
            'js_fileinput'=>true,

            'id_employee'=>$this->custom_model->kodeEmployee(),
            'dt_employee'=>$this->custom_model->getAllData('t_employee'),
            'dt_propinsi'=>$this->custom_model->getAllData('t_propinsi'),
            'dt_kota'=>$this->custom_model->getAllData('t_kota'),


        );
        $this->load->view('element/_header',$data);
        $this->load->view('element/_notif');
        $this->load->view('pages/employee/_employee');
        $this->load->view('element/_footer');
    }

    function view_pages(){
        $id['id_employee']=$this->uri->segment(3);
        $data=array(
            'title'=>'Detail Employee Data',
            'act_emp'=>true,

            'dt_employee'=>$this->custom_model->getDetailEmployee($id),
            'dt_lokasi'=>$this->custom_model->getLokasiEmployee($id),
            'count_project'=>$this->custom_model->countProjectEmployeeByID($id),
        );
        $this->load->view('element/_header',$data);
        $this->load->view('element/_notif');
        $this->load->view('pages/employee/_view_employee');
        $this->load->view('element/_footer');
    }

    function edit_pages(){
        $id['id_employee']=$this->uri->segment(3);
        $data=array(
            'title'=>'Update Employee Data',
            'act_emp'=>true,
            'css_datatable'=>true,
            'js_datatable'=>true,
            'css_fuelux'=>true,
            'js_fuelux'=>true,
            'js_parsley'=>true,
            'js_combodate'=>true,
            'css_select2'=>true,
            'js_select2'=>true,
            'css_datepicker'=>true,
            'js_datepicker'=>true,
            'js_fileinput'=>true,

            'dt_employee'=>$this->custom_model->getSelectedData('t_employee',$id)->result(),
            'dt_lokasi'=>$this->custom_model->getLokasiEmployee($id),
            'dt_propinsi'=>$this->custom_model->getAllData('t_propinsi'),
            'dt_kota'=>$this->custom_model->getAllData('t_kota'),
        );
        $this->load->view('element/_header',$data);
        $this->load->view('element/_notif');
        $this->load->view('pages/employee/_edit_employee');
        $this->load->view('element/_footer');
    }

    function input_data(){
        $config['upload_path'] = './uploads/photos/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['maintain_ratio']=TRUE;
        $config['max_size'] = '2000';
        $config['file_name'] = 'emp_'.$this->input->post('sn_employee');
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( !$this->upload->do_upload()){

            $data = array(
                // Profil
                'id_employee'=>$this->input->post('id_employee'),
                'sn_employee'=>$this->input->post('sn_employee'),
                'nama'=>$this->input->post('nama'),
                'tempat_lahir'=>$this->input->post('tempat_lahir'),
                'tgl_lahir'=>date("Y-m-d",strtotime($this->input->post('tgl_lahir'))),
                'jns_kelamin'=>$this->input->post('jns_kelamin'),
                'gol_darah'=>$this->input->post('gol_darah'),
                'status_kawin'=>$this->input->post('status_kawin'),
                'tgl_kawin'=>date("Y-m-d",strtotime($this->input->post('tgl_kawin'))),
                'agama'=>$this->input->post('agama'),
                'kewarganegaraan'=>$this->input->post('kewarganegaraan'),

                // No Identitas
                'no_ktp'=>$this->input->post('no_ktp'),
                'no_sim'=>$this->input->post('no_sim'),
                'no_passport'=>$this->input->post('no_passport'),
                'no_npwp'=>$this->input->post('no_npwp'),
                'no_jamsostek'=>$this->input->post('no_jamsostek'),
                'masa_berlaku'=>date("Y-m-d",strtotime($this->input->post('masa_berlaku'))),
                'pendidikan'=>$this->input->post('pendidikan'),

                // Pendidikan
                'akademi'=>$this->input->post('akademi'),
                'jurusan'=>$this->input->post('jurusan'),
                'thn_lulus'=>$this->input->post('thn_lulus'),
                'sertifikat'=>$this->input->post('sertifikat'),

                // Kontak, Alamat & Kontak Darurat
                'email1'=>$this->input->post('email1'),
                'email2'=>$this->input->post('email2'),
                'hp1'=>$this->input->post('hp1'),
                'hp2'=>$this->input->post('hp2'),
                'alamat_ktp'=>$this->input->post('alamat_ktp'),
                'id_propinsi_ktp'=>$this->input->post('id_propinsi_ktp'),
                'id_kota_ktp'=>$this->input->post('id_kota_ktp'),
                'id_kecamatan_ktp'=>$this->input->post('id_kecamatan_ktp'),
                'id_kelurahan_ktp'=>$this->input->post('id_kelurahan_ktp'),
                'kode_pos_ktp'=>$this->input->post('kode_pos_ktp'),
                'tlp_rumah_ktp'=>$this->input->post('tlp_rumah_ktp'),

                'alamat_skr'=>$this->input->post('alamat_skr'),
                'id_propinsi_skr'=>$this->input->post('id_propinsi_skr'),
                'id_kota_skr'=>$this->input->post('id_kota_skr'),
                'id_kecamatan_skr'=>$this->input->post('id_kecamatan_skr'),
                'id_kelurahan_skr'=>$this->input->post('id_kelurahan_skr'),
                'kode_pos_skr'=>$this->input->post('kode_pos_skr'),
                'tlp_rumah_skr'=>$this->input->post('tlp_rumah_skr'),
                'status_rumah_skr'=>$this->input->post('status_rumah_skr'),

                'alamat_mess'=>$this->input->post('alamat_mess'),
                'id_propinsi_mess'=>$this->input->post('id_propinsi_mess'),
                'id_kota_mess'=>$this->input->post('id_kota_mess'),
                'id_kecamatan_mess'=>$this->input->post('id_kecamatan_mess'),
                'id_kelurahan_mess'=>$this->input->post('id_kelurahan_mess'),
                'kode_pos_mess'=>$this->input->post('kode_pos_mess'),
                'tlp_rumah_mess'=>$this->input->post('tlp_rumah_mess'),
                'nama_ecd'=>$this->input->post('nama_ecd'),

                'hubungan_ecd'=>$this->input->post('hubungan_ecd'),
                'alamat_ecd'=>$this->input->post('alamat_ecd'),
                'tlp_rumah_ecd'=>$this->input->post('tlp_rumah_ecd'),
                'tlp_kantor_ecd'=>$this->input->post('tlp_kantor_ecd'),
                'hp_ecd'=>$this->input->post('hp_ecd'),
                'email_ecd'=>$this->input->post('email_ecd'),
                
                // Info Tambahan
                'jml_anak'=>$this->input->post('jml_anak'),
                'kependudukan_istri'=>$this->input->post('kependudukan_istri'),
                'keluarga_onsite'=>$this->input->post('keluarga_onsite'),
                'tgl_keluarga_onsite'=>date("Y-m-d",strtotime($this->input->post('tgl_keluarga_onsite'))),
                'status_sekolah_anak'=>$this->input->post('status_sekolah_anak'),
                'anak_keberapa'=>$this->input->post('anak_keberapa'),
                'alamat_sekolah_anak'=>$this->input->post('alamat_sekolah_anak'),
                'update'=>date("Y-m-d",strtotime($this->input->post('update'))),
                'user_update'=>$this->input->post('user_update')
            );
            $cek = $this->custom_model->insertData('t_employee',$data);
            if($cek)
                $this->session->set_flashdata('success_add',true);
            redirect("employee");

        }else{

            $file =  $this->upload->data();
            $namaFile=$file['file_name'];
            $source = "./uploads/photos/".$file['file_name'];

            // Resizing Original Img
            $resizeImg['quality'] = '100%' ;
            $resizeImg['maintain_ratio']=TRUE;
            $resizeImg['source_image'] = $source ;
            $resizeImg['width'] = 320;
            $resizeImg['height'] = 320;
            $resizeImg['new_image'] = $source ;

            // Do Resizing
            $this->image_lib->clear();
            $this->image_lib->initialize($resizeImg);
            $this->image_lib->resize($resizeImg);
            $this->image_lib->clear();

            $data = array(
                // Profil
                'id_employee'=>$this->input->post('id_employee'),
                'sn_employee'=>$this->input->post('sn_employee'),
                'nama'=>$this->input->post('nama'),
                'tempat_lahir'=>$this->input->post('tempat_lahir'),
                'tgl_lahir'=>date("Y-m-d",strtotime($this->input->post('tgl_lahir'))),
                'jns_kelamin'=>$this->input->post('jns_kelamin'),
                'gol_darah'=>$this->input->post('gol_darah'),
                'status_kawin'=>$this->input->post('status_kawin'),
                'tgl_kawin'=>date("Y-m-d",strtotime($this->input->post('tgl_kawin'))),
                'agama'=>$this->input->post('agama'),
                'kewarganegaraan'=>$this->input->post('kewarganegaraan'),
                'employee_img'=>$namaFile,

                // No Identitas
                'no_ktp'=>$this->input->post('no_ktp'),
                'no_sim'=>$this->input->post('no_sim'),
                'no_passport'=>$this->input->post('no_passport'),
                'no_npwp'=>$this->input->post('no_npwp'),
                'no_jamsostek'=>$this->input->post('no_jamsostek'),
                'masa_berlaku'=>date("Y-m-d",strtotime($this->input->post('masa_berlaku'))),
                'pendidikan'=>$this->input->post('pendidikan'),

                // Pendidikan
                'akademi'=>$this->input->post('akademi'),
                'jurusan'=>$this->input->post('jurusan'),
                'thn_lulus'=>$this->input->post('thn_lulus'),
                'sertifikat'=>$this->input->post('sertifikat'),

                // Kontak, Alamat & Kontak Darurat
                'email1'=>$this->input->post('email1'),
                'email2'=>$this->input->post('email2'),
                'hp1'=>$this->input->post('hp1'),
                'hp2'=>$this->input->post('hp2'),
                'alamat_ktp'=>$this->input->post('alamat_ktp'),
                'id_propinsi_ktp'=>$this->input->post('id_propinsi_ktp'),
                'id_kota_ktp'=>$this->input->post('id_kota_ktp'),
                'id_kecamatan_ktp'=>$this->input->post('id_kecamatan_ktp'),
                'id_kelurahan_ktp'=>$this->input->post('id_kelurahan_ktp'),
                'kode_pos_ktp'=>$this->input->post('kode_pos_ktp'),
                'tlp_rumah_ktp'=>$this->input->post('tlp_rumah_ktp'),

                'alamat_skr'=>$this->input->post('alamat_skr'),
                'id_propinsi_skr'=>$this->input->post('id_propinsi_skr'),
                'id_kota_skr'=>$this->input->post('id_kota_skr'),
                'id_kecamatan_skr'=>$this->input->post('id_kecamatan_skr'),
                'id_kelurahan_skr'=>$this->input->post('id_kelurahan_skr'),
                'kode_pos_skr'=>$this->input->post('kode_pos_skr'),
                'tlp_rumah_skr'=>$this->input->post('tlp_rumah_skr'),
                'status_rumah_skr'=>$this->input->post('status_rumah_skr'),

                'alamat_mess'=>$this->input->post('alamat_mess'),
                'id_propinsi_mess'=>$this->input->post('id_propinsi_mess'),
                'id_kota_mess'=>$this->input->post('id_kota_mess'),
                'id_kecamatan_mess'=>$this->input->post('id_kecamatan_mess'),
                'id_kelurahan_mess'=>$this->input->post('id_kelurahan_mess'),
                'kode_pos_mess'=>$this->input->post('kode_pos_mess'),
                'tlp_rumah_mess'=>$this->input->post('tlp_rumah_mess'),
                'nama_ecd'=>$this->input->post('nama_ecd'),

                'hubungan_ecd'=>$this->input->post('hubungan_ecd'),
                'alamat_ecd'=>$this->input->post('alamat_ecd'),
                'tlp_rumah_ecd'=>$this->input->post('tlp_rumah_ecd'),
                'tlp_kantor_ecd'=>$this->input->post('tlp_kantor_ecd'),
                'hp_ecd'=>$this->input->post('hp_ecd'),
                'email_ecd'=>$this->input->post('email_ecd'),

                // Info Tambahan
                'status_employee'=>$this->input->post('status_employee'),
                'start_contract'=>date("Y-m-d",strtotime($this->input->post('start_contract'))),
                'end_contract'=>date("Y-m-d",strtotime($this->input->post('end_contract'))),
                'jml_anak'=>$this->input->post('jml_anak'),
                'kependudukan_istri'=>$this->input->post('kependudukan_istri'),
                'keluarga_onsite'=>$this->input->post('keluarga_onsite'),
                'tgl_keluarga_onsite'=>date("Y-m-d",strtotime($this->input->post('tgl_keluarga_onsite'))),
                'status_sekolah_anak'=>$this->input->post('status_sekolah_anak'),
                'anak_keberapa'=>$this->input->post('anak_keberapa'),
                'alamat_sekolah_anak'=>$this->input->post('alamat_sekolah_anak'),
                'update'=>date("Y-m-d",strtotime($this->input->post('update'))),
                'user_update'=>$this->input->post('user_update')
            );
            $cek = $this->custom_model->insertData('t_employee',$data);
            if($cek)
                $this->session->set_flashdata('success_add',true);
            redirect("employee");
        }
    }

    function update_data(){
        $id['id_employee']=$this->input->post('id_employee');

        $config['upload_path'] = './uploads/photos/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['maintain_ratio']=TRUE;
        $config['max_size'] = '2000';
        $config['file_name'] = 'emp_'.$this->input->post('sn_employee');
        $this->load->library('upload',$config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload()){

            $data = array(
                // Profil
                'id_employee'=>$this->input->post('id_employee'),
                'sn_employee'=>$this->input->post('sn_employee'),
                'nama'=>$this->input->post('nama'),
                'tempat_lahir'=>$this->input->post('tempat_lahir'),
                'tgl_lahir'=>date("Y-m-d",strtotime($this->input->post('tgl_lahir'))),
                'jns_kelamin'=>$this->input->post('jns_kelamin'),
                'gol_darah'=>$this->input->post('gol_darah'),
                'status_kawin'=>$this->input->post('status_kawin'),
                'tgl_kawin'=>date("Y-m-d",strtotime($this->input->post('tgl_kawin'))),
                'agama'=>$this->input->post('agama'),
                'kewarganegaraan'=>$this->input->post('kewarganegaraan'),

                // No Identitas
                'no_ktp'=>$this->input->post('no_ktp'),
                'no_sim'=>$this->input->post('no_sim'),
                'no_passport'=>$this->input->post('no_passport'),
                'no_npwp'=>$this->input->post('no_npwp'),
                'no_jamsostek'=>$this->input->post('no_jamsostek'),
                'masa_berlaku'=>date("Y-m-d",strtotime($this->input->post('masa_berlaku'))),
                'pendidikan'=>$this->input->post('pendidikan'),

                // Pendidikan
                'akademi'=>$this->input->post('akademi'),
                'jurusan'=>$this->input->post('jurusan'),
                'thn_lulus'=>$this->input->post('thn_lulus'),
                'sertifikat'=>$this->input->post('sertifikat'),

                // Kontak, Alamat & Kontak Darurat
                'email1'=>$this->input->post('email1'),
                'email2'=>$this->input->post('email2'),
                'hp1'=>$this->input->post('hp1'),
                'hp2'=>$this->input->post('hp2'),

                'alamat_ktp'=>$this->input->post('alamat_ktp'),
                'id_propinsi_ktp'=>$this->input->post('id_propinsi_ktp'),
                'id_kota_ktp'=>$this->input->post('id_kota_ktp'),
                'id_kecamatan_ktp'=>$this->input->post('id_kecamatan_ktp'),
                'id_kelurahan_ktp'=>$this->input->post('id_kelurahan_ktp'),
                'kode_pos_ktp'=>$this->input->post('kode_pos_ktp'),
                'tlp_rumah_ktp'=>$this->input->post('tlp_rumah_ktp'),

                'alamat_skr'=>$this->input->post('alamat_skr'),
                'id_propinsi_skr'=>$this->input->post('id_propinsi_skr'),
                'id_kota_skr'=>$this->input->post('id_kota_skr'),
                'id_kecamatan_skr'=>$this->input->post('id_kecamatan_skr'),
                'id_kelurahan_skr'=>$this->input->post('id_kelurahan_skr'),
                'kode_pos_skr'=>$this->input->post('kode_pos_skr'),
                'tlp_rumah_skr'=>$this->input->post('tlp_rumah_skr'),
                'status_rumah_skr'=>$this->input->post('status_rumah_skr'),

                'alamat_mess'=>$this->input->post('alamat_mess'),
                'id_propinsi_mess'=>$this->input->post('id_propinsi_mess'),
                'id_kota_mess'=>$this->input->post('id_kota_mess'),
                'id_kecamatan_mess'=>$this->input->post('id_kecamatan_mess'),
                'id_kelurahan_mess'=>$this->input->post('id_kelurahan_mess'),
                'kode_pos_mess'=>$this->input->post('kode_pos_mess'),
                'tlp_rumah_mess'=>$this->input->post('tlp_rumah_mess'),
                'nama_ecd'=>$this->input->post('nama_ecd'),

                'hubungan_ecd'=>$this->input->post('hubungan_ecd'),
                'alamat_ecd'=>$this->input->post('alamat_ecd'),
                'tlp_rumah_ecd'=>$this->input->post('tlp_rumah_ecd'),
                'tlp_kantor_ecd'=>$this->input->post('tlp_kantor_ecd'),
                'hp_ecd'=>$this->input->post('hp_ecd'),
                'email_ecd'=>$this->input->post('email_ecd'),

                // Info Tambahan
                'status_employee'=>$this->input->post('status_employee'),
                'start_contract'=>date("Y-m-d",strtotime($this->input->post('start_contract'))),
                'end_contract'=>date("Y-m-d",strtotime($this->input->post('end_contract'))),
                'jml_anak'=>$this->input->post('jml_anak'),
                'kependudukan_istri'=>$this->input->post('kependudukan_istri'),
                'keluarga_onsite'=>$this->input->post('keluarga_onsite'),
                'tgl_keluarga_onsite'=>date("Y-m-d",strtotime($this->input->post('tgl_keluarga_onsite'))),
                'status_sekolah_anak'=>$this->input->post('status_sekolah_anak'),
                'anak_keberapa'=>$this->input->post('anak_keberapa'),
                'alamat_sekolah_anak'=>$this->input->post('alamat_sekolah_anak'),
                'update'=>date("Y-m-d",strtotime($this->input->post('update'))),
                'user_update'=>$this->input->post('user_update')
            );
            $cek = $this->custom_model->updateData('t_employee',$data,$id);
            if($cek)
                $this->session->set_flashdata('success_add',true);
            redirect("employee");

        }else{

            $this->db->select('employee_img')->from('t_employee')->where($id);
            $query = $this->db->get();
            if($query->num_rows() > 0){
                $row = $query->row();
                $file = 'uploads/photos/'.$row->employee_img ;
                @unlink($file);
            }
            $file =  $this->upload->data();
            $namaFile=$file['file_name'];
            $source = "./uploads/photos/".$file['file_name'];

            // Resizing Original Img
            $resizeImg['quality'] = '100%' ;
            $resizeImg['maintain_ratio']=TRUE;
            $resizeImg['source_image'] = $source ;
            $resizeImg['width'] = 320;
            $resizeImg['height'] = 320;
            $resizeImg['new_image'] = $source ;

            // Do Resizing
            $this->image_lib->clear();
            $this->image_lib->initialize($resizeImg);
            $this->image_lib->resize($resizeImg);
            $this->image_lib->clear();

            $data = array(

                // Profil
                'sn_employee'=>$this->input->post('sn_employee'),
                'nama'=>$this->input->post('nama'),
                'tempat_lahir'=>$this->input->post('tempat_lahir'),
                'tgl_lahir'=>date("Y-m-d",strtotime($this->input->post('tgl_lahir'))),
                'jns_kelamin'=>$this->input->post('jns_kelamin'),
                'gol_darah'=>$this->input->post('gol_darah'),
                'status_kawin'=>$this->input->post('status_kawin'),
                'tgl_kawin'=>date("Y-m-d",strtotime($this->input->post('tgl_kawin'))),
                'agama'=>$this->input->post('agama'),
                'kewarganegaraan'=>$this->input->post('kewarganegaraan'),
                'employee_img'=>$namaFile,

                // No Identitas
                'no_ktp'=>$this->input->post('no_ktp'),
                'no_sim'=>$this->input->post('no_sim'),
                'no_passport'=>$this->input->post('no_passport'),
                'no_npwp'=>$this->input->post('no_npwp'),
                'no_jamsostek'=>$this->input->post('no_jamsostek'),
                'masa_berlaku'=>date("Y-m-d",strtotime($this->input->post('masa_berlaku'))),
                'pendidikan'=>$this->input->post('pendidikan'),

                // Pendidikan
                'akademi'=>$this->input->post('akademi'),
                'jurusan'=>$this->input->post('jurusan'),
                'thn_lulus'=>$this->input->post('thn_lulus'),
                'sertifikat'=>$this->input->post('sertifikat'),

                // Kontak, Alamat & Kontak Darurat
                'email1'=>$this->input->post('email1'),
                'email2'=>$this->input->post('email2'),
                'hp1'=>$this->input->post('hp1'),
                'hp2'=>$this->input->post('hp2'),

                'alamat_ktp'=>$this->input->post('alamat_ktp'),
                'id_propinsi_ktp'=>$this->input->post('id_propinsi_ktp'),
                'id_kota_ktp'=>$this->input->post('id_kota_ktp'),
                'id_kecamatan_ktp'=>$this->input->post('id_kecamatan_ktp'),
                'id_kelurahan_ktp'=>$this->input->post('id_kelurahan_ktp'),
                'kode_pos_ktp'=>$this->input->post('kode_pos_ktp'),
                'tlp_rumah_ktp'=>$this->input->post('tlp_rumah_ktp'),

                'alamat_skr'=>$this->input->post('alamat_skr'),
                'id_propinsi_skr'=>$this->input->post('id_propinsi_skr'),
                'id_kota_skr'=>$this->input->post('id_kota_skr'),
                'id_kecamatan_skr'=>$this->input->post('id_kecamatan_skr'),
                'id_kelurahan_skr'=>$this->input->post('id_kelurahan_skr'),
                'kode_pos_skr'=>$this->input->post('kode_pos_skr'),
                'tlp_rumah_skr'=>$this->input->post('tlp_rumah_skr'),
                'status_rumah_skr'=>$this->input->post('status_rumah_skr'),

                'alamat_mess'=>$this->input->post('alamat_mess'),
                'id_propinsi_mess'=>$this->input->post('id_propinsi_mess'),
                'id_kota_mess'=>$this->input->post('id_kota_mess'),
                'id_kecamatan_mess'=>$this->input->post('id_kecamatan_mess'),
                'id_kelurahan_mess'=>$this->input->post('id_kelurahan_mess'),
                'kode_pos_mess'=>$this->input->post('kode_pos_mess'),
                'tlp_rumah_mess'=>$this->input->post('tlp_rumah_mess'),
                'nama_ecd'=>$this->input->post('nama_ecd'),

                'hubungan_ecd'=>$this->input->post('hubungan_ecd'),
                'alamat_ecd'=>$this->input->post('alamat_ecd'),
                'tlp_rumah_ecd'=>$this->input->post('tlp_rumah_ecd'),
                'tlp_kantor_ecd'=>$this->input->post('tlp_kantor_ecd'),
                'hp_ecd'=>$this->input->post('hp_ecd'),
                'email_ecd'=>$this->input->post('email_ecd'),

                // Info Tambahan
                'status_employee'=>$this->input->post('status_employee'),
                'start_contract'=>date("Y-m-d",strtotime($this->input->post('start_contract'))),
                'end_contract'=>date("Y-m-d",strtotime($this->input->post('end_contract'))),
                'jml_anak'=>$this->input->post('jml_anak'),
                'kependudukan_istri'=>$this->input->post('kependudukan_istri'),
                'keluarga_onsite'=>$this->input->post('keluarga_onsite'),
                'tgl_keluarga_onsite'=>date("Y-m-d",strtotime($this->input->post('tgl_keluarga_onsite'))),
                'status_sekolah_anak'=>$this->input->post('status_sekolah_anak'),
                'anak_keberapa'=>$this->input->post('anak_keberapa'),
                'alamat_sekolah_anak'=>$this->input->post('alamat_sekolah_anak'),
                'update'=>date("Y-m-d",strtotime($this->input->post('update'))),
                'user_update'=>$this->input->post('user_update')
            );
            $cek = $this->custom_model->updateData('t_employee',$data,$id);
            if($cek)
                $this->session->set_flashdata('success_add',true);
            redirect("employee");
        }
    }

    function delete_data(){
        $id['id_employee'] = $this->uri->segment(3);
        $this->db->select('employee_img')->from('t_employee')->where($id);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $row = $query->row();
            $file = 'uploads/photos/'.$row->employee_img ;
            @unlink($file);
        }
        $this->custom_model->deleteData('t_employee',$id);
        $this->db->select('files')->from('t_files')->where($id);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $row = $query->row();
            $file = 'uploads/files/'.$row->files ;
            @unlink($file);
        }
        $this->custom_model->deleteData('t_files',$id);
        $this->session->set_flashdata('success_delete',true);
        redirect("employee");
    }


    //    AJAX LOKASI
    function ajaxLoadKota(){
        $id_propinsi = $this->input->post('id_propinsi');
        $data=array(
            'kategori'=>$this->input->post('kat_alamat'),
            'ajax_kota'=>$this->custom_model->getAjaxKota($id_propinsi),
        );
        $this->load->view('pages/employee/_opt_kota',$data);
    }
    function ajaxLoadKecamatan(){
        $id_kota = $this->input->post('id_kota');
        $data=array(
            'kategori'=>$this->input->post('kat_alamat'),
            'ajax_kecamatan'=>$this->custom_model->getAjaxKecamatan($id_kota),
        );
        $this->load->view('pages/employee/_opt_kecamatan',$data);
    }
    function ajaxLoadKelurahan(){
        $id_kecamatan = $this->input->post('id_kecamatan');
        $data=array(
            'kategori'=>$this->input->post('kat_alamat'),
            'ajax_kelurahan'=>$this->custom_model->getAjaxKelurahan($id_kecamatan),
        );
        $this->load->view('pages/employee/_opt_kelurahan',$data);
    }


}
