<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 6/9/14
 * Time: 5:03 PM
 */
class Set_user extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('login_status') != TRUE ){
            $this->session->set_flashdata('notif','LOGIN FAILED ! USERNAME OR PASSWORD IS NOT VALID');
            redirect('');
        };
        $this->load->model('custom_model');
    }

    function index(){
        $data=array(
            'title'=>'Setting Users',
            'act_setting'=>true,
            'act_users'=>true,
            'css_datatable'=>true,
            'js_datatable'=>true,
            'js_fileinput'=>true,

            'dt_users'=>$this->custom_model->getAllData('t_user'),
        );
        $this->load->view('element/_header',$data);
        $this->load->view('element/_notif');
        $this->load->view('pages/setting_users/_users');
        $this->load->view('element/_footer');
    }

    function edit_pages(){
        $id['id_user']=$this->uri->segment(3);
        $data=array(
            'title'=>'Setting Users',
            'act_setting'=>true,
            'act_users'=>true,
            'js_fileinput'=>true,

            'dt_users'=>$this->custom_model->getSelectedData('t_user',$id)->result(),
        );
        $this->load->view('element/_header',$data);
        $this->load->view('pages/setting_users/_edit_users');
        $this->load->view('element/_footer');
    }

    function input_data(){
        $config['upload_path'] = './uploads/photos/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['maintain_ratio']=TRUE;
        $config['max_size'] = '2000';
        $config['file_name']='admin_'.$this->input->post('name');
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( !$this->upload->do_upload()){

            $data=array(
                'username'=>$this->input->post('username'),
                'password'=>md5($this->input->post('password')),
                'name'=>$this->input->post('name'),
            );
            $cek = $this->custom_model->insertData('t_user',$data);
            if($cek)
                $this->session->set_flashdata('success_add',true);
            redirect("set_user");

        }else{

            $file =  $this->upload->data();
            $namaFile=$file['file_name'];
            $source = "./uploads/photos/".$file['file_name'];

            // Resizing Original Img
            $resizeImg['quality'] = '100%' ;
            $resizeImg['maintain_ratio']=TRUE;
            $resizeImg['source_image'] = $source ;
            $resizeImg['width'] = 320;
            $resizeImg['height'] = 320;
            $resizeImg['new_image'] = $source ;

            // Do Resizing
            $this->image_lib->clear();
            $this->image_lib->initialize($resizeImg);
            $this->image_lib->resize($resizeImg);
            $this->image_lib->clear();

            $data=array(
                'username'=>$this->input->post('username'),
                'password'=>md5($this->input->post('password')),
                'name'=>$this->input->post('name'),
                'user_img'=>$namaFile,
            );
            $cek = $this->custom_model->insertData('t_user',$data);
            if($cek)
                $this->session->set_flashdata('success_add',true);
            redirect("set_user");
        }
    }

    function update_data(){
        $id['id_user']=$this->input->post('id_user');

        $config['upload_path'] = './uploads/photos/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['maintain_ratio']=TRUE;
        $config['max_size'] = '2000';
        $config['file_name']='admin_'.$this->input->post('name');
        $this->load->library('upload',$config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload()){

            $data = array(
                'username'=>$this->input->post('username'),
                'password'=>md5($this->input->post('password')),
                'name'=>$this->input->post('name'),
            );
            $cek = $this->custom_model->updateData('t_user',$data,$id);
            if($cek)
                $this->session->set_flashdata('success_add',true);
            redirect("set_user");

        }else{

            $this->db->select('user_img')->from('t_user')->where($id);
            $query = $this->db->get();
            if($query->num_rows() > 0){
                $row = $query->row();
                $file = 'uploads/photos/'.$row->user_img ;
                @unlink($file);
            }
            $file =  $this->upload->data();
            $namaFile=$file['file_name'];
            $source = "./uploads/photos/".$file['file_name'];

            // Resizing Original Img
            $resizeImg['quality'] = '100%' ;
            $resizeImg['maintain_ratio']=TRUE;
            $resizeImg['source_image'] = $source ;
            $resizeImg['width'] = 320;
            $resizeImg['height'] = 320;
            $resizeImg['new_image'] = $source ;

            // Do Resizing
            $this->image_lib->clear();
            $this->image_lib->initialize($resizeImg);
            $this->image_lib->resize($resizeImg);
            $this->image_lib->clear();

            $data = array(
                'username'=>$this->input->post('username'),
                'password'=>md5($this->input->post('password')),
                'name'=>$this->input->post('name'),
                'user_img'=>$namaFile,
            );
            $cek = $this->custom_model->updateData('t_user',$data,$id);
            if($cek)
                $this->session->set_flashdata('success_add',true);
            redirect("set_user");
        }
    }

    function delete_data(){
        $id['id_user'] = $this->input->post('id_user');
        $this->db->select('user_img')->from('t_user')->where($id);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $row = $query->row();
            $file = 'uploads/photos/'.$row->user_img ;
            @unlink($file);
        }
        $this->custom_model->deleteData('t_user',$id);
        $this->session->set_flashdata('success_delete',true);
        redirect("set_user");
    }
}
