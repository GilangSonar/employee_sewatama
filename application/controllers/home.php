<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 6/9/14
 * Time: 3:34 PM
 */

class Home extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('login_status') != TRUE ){
            $this->session->set_flashdata('notif','LOGIN FAILED ! USERNAME OR PASSWORD IS NOT VALID');
            redirect('');
        };
        $this->load->model('custom_model');
    }

    function index(){
        $data=array(
            'title'=>'Home',
            'act_home'=>true,
            'js_charts'=>true,

            'count_project'=>$this->db->count_all('t_project'),
            'count_employee'=>$this->db->count_all('t_employee'),
            'count_customer'=>$this->db->count_all('t_customer'),
            'count_genset'=>$this->db->count_all('t_genset'),

            'dt_charts_area'=>$this->custom_model->charts_area(),
            'dt_charts_customer'=>$this->custom_model->charts_customer(),
        );
        $this->load->view('element/_header',$data);
        $this->load->view('pages/home/_home');
        $this->load->view('element/_footer');
    }
}