<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title><?= $title; ?></title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" href="<?= base_url('assets/images/favicon.png')?>">

    <!--  CSS BASE-->
    <link rel="stylesheet" href="<?= base_url('assets/css/app.css')?>" type="text/css" />
    <link rel="stylesheet" href="<?= base_url('assets/css/font.css')?>" type="text/css" />

    <!--  CSS PLUGINS-->
    <?php if(isset($css_fuelux)): ?>
        <link rel="stylesheet" href="<?= base_url('assets/js/fuelux/fuelux.css')?>" type="text/css"/>
    <?php endif; ?>
    <?php if(isset($css_datatable)): ?>
        <link rel="stylesheet" href="<?= base_url('assets/js/datatables/datatables.css')?>" type="text/css"/>
    <?php endif; ?>
    <?php if(isset($css_select2)): ?>
        <link rel="stylesheet" href="<?= base_url('assets/js/select2/select2.css')?>" type="text/css"/>
        <link rel="stylesheet" href="<?= base_url('assets/js/select2/theme.css')?>" type="text/css"/>
    <?php endif; ?>
    <?php if(isset($css_datepicker)): ?>
        <link rel="stylesheet" href="<?= base_url('assets/js/datepicker/datepicker.css')?>" type="text/css"/>
    <?php endif; ?>
    <link rel="stylesheet" href="<?= base_url('assets/css/jquery.gritter.css')?>" type="text/css" />

    <!--    JQUERY-->
    <script src="<?= base_url('assets/js/jquery.min.js')?>"></script>
    <script src="<?= base_url('assets/js/jquery-migrate.min.js')?>"></script>
</head>
<body>
<section class="vbox">

    <header class="bg-dark dk header navbar navbar-fixed-top-xs">

        <div class="navbar-header aside-md">

            <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen,open" data-target="#nav,html">
                <i class="fa fa-bars"></i>
            </a>

            <a href="#" class="navbar-brand" data-toggle="fullscreen">
                <img src="<?= base_url('assets/images/favicon.png')?>" class="m-r-sm">SEWATAMA
            </a>

            <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".nav-user">
                <i class="fa fa-cog"></i>
            </a>
        </div>

        <ul class="nav navbar-nav navbar-right hidden-xs nav-user">

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="thumb-sm avatar pull-left">
                        <img src="<?= base_url('uploads/photos/'.$this->session->userdata('IMG'))?>">
                    </span> <?= $this->session->userdata('NAME')?> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu animated fadeInRight">
                    <span class="arrow top"></span>
                    <li>
                        <a href="<?= site_url('login/lock_pages')?>" data-toggle="ajaxModal" >
                            <i class="fa fa-key"></i> Lock Pages
                        </a>
                    </li>
                    <li>
                        <a href="<?= site_url('login/logout')?>">
                            <i class="fa fa-sign-out"></i> Sign Out
                        </a>
                    </li>
                </ul>
            </li>

        </ul>
    </header>


