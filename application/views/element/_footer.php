<!--================ SCRIPT ====================== -->
<script src="<?= base_url('assets/js/app.js')?>"></script>

<!--Pie Charts (Highcharts)-->
<?php if(isset($js_charts)): ?>
    <script type="text/javascript" src="<?= base_url('assets/js/charts/highcharts/highcharts.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/charts/highcharts/highcharts-3d.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/charts/highcharts/highcharts-exporting.js')?>"></script>
<?php endif; ?>
<!--Form Input File (BS Filestyle)-->
<?php if(isset($js_fileinput)): ?>
    <script src="<?= base_url('assets/js/file-input/bootstrap-filestyle.min.js')?>"></script>
<?php endif; ?>
<!--Form Wizard (Fuelux JS)-->
<?php if(isset($js_fuelux)): ?>
    <script src="<?= base_url('assets/js/fuelux/fuelux.js')?>"></script>
<?php endif; ?>
<!--Validation (Pasrley JS)-->
<?php if(isset($js_parsley)): ?>
    <script src="<?= base_url('assets/js/parsley/parsley.min.js')?>"></script>
<?php endif; ?>
<!--Separated Date (Combodate JS)-->
<?php if(isset($js_combodate)): ?>
    <script src="<?= base_url('assets/js/libs/moment.min.js')?>"></script>
    <script src="<?= base_url('assets/js/combodate/combodate.js')?>"></script>
<?php endif; ?>
<!--Option Dropdown (Select2 JS)-->
<?php if(isset($js_select2)): ?>
    <script src="<?= base_url('assets/js/select2/select2.min.js')?>"></script>
<?php endif; ?>
<!--Date (BS-Datepicker JS)-->
<?php if(isset($js_datepicker)): ?>
    <script src="<?= base_url('assets/js/datepicker/bootstrap-datepicker.js')?>"></script>
<?php endif; ?>
<!--Table (Datatable JS)-->
<?php if(isset($js_datatable)): ?>
    <script src="<?= base_url('assets/js/datatables/jquery.dataTables.min.js')?>"></script>
<?php endif; ?>
<!--Notif Alert (Jquery Gritter JS)-->
<script src="<?= base_url('assets/js/gritter/jquery.gritter.min.js')?>"></script>


</body>
</html>
