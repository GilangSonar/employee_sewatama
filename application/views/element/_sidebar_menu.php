<aside class="bg-dark lter aside-md hidden-print" id="nav">

<section class="vbox">
    <header class="header bg-primary lter text-center clearfix">

        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project">
                <i class="fa fa-bars"></i>
            </button>
            <div class="btn-group hidden-nav-xs">
                <button type="button" class="btn btn-sm btn-primary" data-toggle="dropdown"> <strong>MAIN MENU</strong>
            </div>
        </div>
    </header>
    <section class="w-f scrollable">

        <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="5px" data-color="#333333">
            <nav class="nav-primary hidden-xs">
                <ul class="nav">

                    <li class="<?php if(isset($act_home)){ echo 'active'; } ?>">
                        <a href="<?= site_url('home')?>" class="<?php if(isset($act_home)){ echo 'active'; } ?>">
                            <i class="fa fa-home icon"><b class="bg-danger"></b> </i><span>Home</span>
                        </a>
                    </li>

                    <li class="<?php if(isset($act_project)){ echo 'active'; } ?>">
                        <a href="<?= site_url('project')?>" class="<?php if(isset($act_project)){ echo 'active'; } ?>">
                            <i class="fa fa-tasks icon"><b class="bg-info"></b> </i><span>Project / Retail</span>
                        </a>
                    </li>

                    <li class="<?php if(isset($act_emp)){ echo 'active'; } ?>">
                        <a href="<?= site_url('employee')?>" class="<?php if(isset($act_emp)){ echo 'active'; } ?>">
                            <i class="fa fa-users icon"><b class="bg-success"></b> </i><span>Employee Data</span>
                        </a>
                    </li>

                    <li class="<?php if(isset($act_files)){ echo 'active'; } ?>">
                        <a href="<?= site_url('files')?>" class="<?php if(isset($act_files)){ echo 'active'; } ?>">
                            <i class="fa fa-file-text-o icon"><b class="bg-dark"></b> </i><span>Employee Files</span>
                        </a>
                    </li>

                    <li class="<?php if(isset($act_setting)){ echo 'active'; } ?>">
                        <a href="#Setting" class="<?php if(isset($act_setting)){ echo 'active'; } ?>">
                            <i class="fa fa-cogs icon"> <b class="bg-warning"></b> </i>
                        <span class="pull-right"> <i class="fa fa-angle-down text"></i>
                            <i class="fa fa-angle-up text-active"></i>
                        </span>
                            <span>Setting</span>
                        </a>
                        <ul class="nav lt" <?php if(isset($act_setting)){ echo 'style="display:block"'; } ?>>
                            <li class="<?php if(isset($act_customer)){ echo 'active'; } ?>"> <a href="<?= site_url('set_customer')?>" > <i class="fa fa-angle-right"></i><span>Customer</span> </a> </li>
                            <li class="<?php if(isset($act_users)){ echo 'active'; } ?>"> <a href="<?= site_url('set_user')?>" > <i class="fa fa-angle-right"></i><span>Users</span> </a> </li>
                            <li class="<?php if(isset($act_genset)){ echo 'active'; } ?>"> <a href="<?= site_url('set_genset')?>" > <i class="fa fa-angle-right"></i><span>Gensets</span> </a> </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </section>

    <footer class="footer lt hidden-xs b-t b-dark">
        <a href="#nav" data-toggle="class:nav-xs" class="pull-right btn btn-sm btn-dark btn-icon">
            <i class="fa fa-angle-left text"></i> <i class="fa fa-angle-right text-active"></i>
        </a>
    </footer>

</section>

</aside>