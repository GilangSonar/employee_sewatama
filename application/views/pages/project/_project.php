<section>
    <section class="hbox stretch">
        <!--======================= SIDEBAR MENU ========================= -->
        <?php $this->load->view('element/_sidebar_menu')?>

        <!--======================= CONTENT HERE ========================= -->
        <section id="content">
            <section class="vbox">
                <section class="scrollable padder">

                    <!--======================= HEADER CONTENT ========================= -->
                    <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                        <li><a href="<?= site_url('home')?>"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Project Retail</li>
                    </ul>
                    <div class="m-b-md">
                        <h3 class="m-b-none"><i class="fa fa-2x fa-cogs"></i> Project / Retail</h3>
                    </div>

                    <!--======================= CONTENT WRAPPER ========================= -->
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel panel-default">

                                <header class="panel-heading text-right bg-light">
                                    <ul class="nav nav-tabs pull-left">
                                        <li class="active"><a href="#tab1" data-toggle="tab"> <i class="fa fa-list text-default"></i> All Record </a></li>
                                        <li class=""><a href="#tab2" data-toggle="tab"><i class="fa fa-plus-circle text-default"></i> Add New Project</a></li>
                                    </ul>
                                    <ul class="nav nav-tabs pull-right">
                                        <li><a href="#" class="btn btn-sm"><i class="fa fa-file-o"></i> Export To PDF</a></li>
                                    </ul>
                                    <span class="hidden-sm">&nbsp;</span>
                                </header>

                                <div class="panel-body">

                                    <div class="tab-content">

                                        <div class="tab-pane fade active in" id="tab1">
                                            <div class="table-responsive">
                                                <table class="table table-striped m-b-none" data-ride="datatables">
                                                    <thead>
                                                    <tr>
                                                        <th width="8%">No</th>
                                                        <th>PROJECT ID</th>
                                                        <th>CUSTOMER NAME</th>
                                                        <th>CONTRACT DATE</th>
                                                        <th>UTILITY</th>
                                                        <th>LOCATION</th>
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <?php $no=1; if(isset($dt_project)){ foreach($dt_project as $row) { ?>

                                                        <tr>
                                                            <td><?= $no++; ?></td>
                                                            <td><?= $row->id_project?></td>
                                                            <td><?= $row->customer?></td>
                                                            <td>
                                                                Start : <?= date("d F Y",strtotime($row->start_date));?>
                                                                <br/>
                                                                End : <?= date("d F Y",strtotime($row->end_date));?>
                                                            </td>
                                                            <td><?= $row->customer?></td>
                                                            <td><?= $row->kota?></td>
                                                            <td class="text-center">

                                                                <div class="btn-group">
                                                                    <a href="<?= site_url('project/view_pages/'.$row->id_project)?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Project Detail">
                                                                        <i class="fa fa-eye text-dark"></i>
                                                                    </a>
                                                                    &nbsp;
                                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></a>
                                                                    <ul class="dropdown-menu pull-right">
                                                                        <li>
                                                                            <a href="<?= site_url('project/edit_pages/'.$row->id_project)?>">
                                                                                <i class="fa fa-pencil-square text-info"></i> Edit
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#alertDelete" data-toggle="modal">
                                                                                <i class="fa fa-times-circle text-danger"></i> Delete
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <!-- MODAL ALERT DELETE-->
                                                        <div class="modal" id="alertDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title" id="myModalLabel">DELETE DATA</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="alert alert-danger text-center">
                                                                            <h4>Are You Sure Want To Delete This Data ? </h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        <a href="<?= site_url('project/delete_data/'.$row->id_project)?>" class="btn btn-danger">Delete</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    <?php } } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <!--======================== ADD NEW FORMS ============================= -->
                                        <div class="tab-pane fade m-t" id="tab2">
                                            <form method="post" action="<?= site_url('project/input_data')?>">

                                                <input type="hidden" name="update" value="<?php echo date('d M Y')?>">
                                                <input type="hidden" name="user_update" value="<?= $this->session->userdata('ID')?>">

                                                <div class="bs-example form-horizontal">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <div class="form-group">
                                                                <label class="col-lg-4 control-label">PROJECT ID</label>
                                                                <div class="col-lg-8">
                                                                    <input name="id_project" type="text" class="form-control" value="<?= $id_project; ?>" readonly>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-lg-4 control-label">CUSTOMER</label>
                                                                <div class="col-lg-8">
                                                                    <select id="id_customer" name="id_customer" class="select2-option" style="width:100%">
                                                                        <option value="">:: Pilih Customer :: </option>
                                                                        <?php if(isset($dt_customer)){ foreach ($dt_customer as $row) { ?>
                                                                            <option value="<?= $row->id_customer?>"><?= $row->customer?></option>
                                                                        <?php } } ?>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div id="detailCustomer"></div>
                                                        </div>

                                                        <div class="col-md-7">
                                                            <div class="form-group">
                                                                <label class="col-lg-3 control-label">START OF CONTRACT</label>
                                                                <div class="col-lg-9">
                                                                    <input name="start_date" class="input-sm input-s datepicker-input form-control" type="text" data-date-format="yyyy-mm-dd" placeholder="Start Date...">
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-lg-3 control-label">END OF CONTRACT</label>
                                                                <div class="col-lg-9">
                                                                    <input name="end_date" class="input-sm input-s datepicker-input form-control" type="text" data-date-format="yyyy-mm-dd" placeholder="End Date...">
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-lg-3 control-label">EMPLOYEE</label>
                                                                <div class="col-lg-9">
                                                                    <select name="id_employee[]" class="select2-option" style="width:100%" multiple required="required">
                                                                        <option value="">:: Pilih Employee :: </option>
                                                                        <?php if(isset($dt_employee)){ foreach ($dt_employee as $row) { ?>
                                                                            <option value="<?= $row->id_employee?>"> SN : <?= $row->sn_employee?> ( <?= $row->nama?> ) </option>
                                                                        <?php } } ?>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-lg-3 control-label">UTILITY</label>
                                                                <div class="col-lg-9">
                                                                    <select name="utility" class="select2-option" style="width:100%" required="required">
                                                                        <option value="">:: Pilih :: </option>
                                                                        <option value="utility"> UTILITY </option>
                                                                        <option value="non-utility"> NON-UTILITY </option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-lg-3 control-label">PROJECT LOCATION</label>
                                                                <div class="col-lg-9">
                                                                    <select name="lokasi" class="select2-option" style="width:100%" required="required">
                                                                        <option value="">:: Pilih Employee :: </option>
                                                                        <?php if(isset($dt_kota)){ foreach ($dt_kota as $row) { ?>
                                                                            <option value="<?= $row->id_kota?>"> <?= $row->kota?> </option>
                                                                        <?php } } ?>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">GENSETS LIST <small class="text-danger"><em><u>checklist genset yang ingin dipilih</u></em></small></div>
                                                            <div class="panel-body">
                                                                <div class="table-responsive">
                                                                    <table class="table table-striped" data-ride="datatables">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>
                                                                                <input type="checkbox" style="width: 14px !important;">
                                                                            </th>
                                                                            <th>SN</th>
                                                                            <th>Model</th>
                                                                            <th>Brand</th>
                                                                            <th>Type / Spec</th>
                                                                            <th>Capacity (Mw) </th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>

                                                                        <?php if(isset($dt_genset)){ foreach($dt_genset as $row) { ?>

                                                                            <tr>
                                                                                <td>
                                                                                    <input type="checkbox" name="id_genset[]" value="<?= $row->id_genset?>" style="width: 14px !important;" >
                                                                                </td>
                                                                                <td><?= $row->sn_genset?></td>
                                                                                <td><?= $row->model?></td>
                                                                                <td><?= $row->brand?></td>
                                                                                <td><?= $row->spec?></td>
                                                                                <td><?= $row->capacity?> Mw</td>
                                                                            </tr>
                                                                        <?php } } ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="pull-left">
                                                        <div class="col-lg-offset-2 col-lg-10">
                                                            <button type="submit" class="btn btn-sm btn-dark">Save Data</button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </section>

                        </div>
                    </div>
                </section>
            </section>

            <!--======================= NAV TOGGLE ========================= -->
            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav">
            </a>

        </section>
    </section>
</section>

<script type="text/javascript">
    $(document).ready(function(){
        $("#id_customer").change(function(){
            var id_customer = {id_customer:$("#id_customer option:selected").val()};
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('project/ajaxLoadCust'); ?>",
                data: id_customer,
                success: function(data){
                    $('#detailCustomer').html(data);
                }
            });
        });
    })
</script>