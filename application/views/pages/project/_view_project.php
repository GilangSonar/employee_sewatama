<section>
    <section class="hbox stretch">
        <!--======================= SIDEBAR MENU ========================= -->
        <?php $this->load->view('element/_sidebar_menu')?>

        <!--======================= CONTENT HERE ========================= -->
        <section id="content">
            <section class="vbox">
                <section class="scrollable padder">

                    <!--======================= HEADER CONTENT ========================= -->
                    <ul class="breadcrumb no-border no-radius b-b b-light pull-in hidden-print">
                        <li><a href="<?= site_url('home')?>"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="<?= site_url('project')?>"><i class="fa fa-cogs"></i> Project</a></li>
                        <li class="active">Project Detail</li>
                    </ul>
                    <div class="m-b-md hidden-print">
                        <h3 class="m-b-none"><i class="fa fa-2x fa-cogs"></i> Project Detail</h3>
                    </div>
                    <!--======================= CONTENT WRAPPER ========================= -->
                    <div class="row">
                        <div class="m-b-md col-xs-12 visible-print">
                            <img class="thumb-lg" src="<?= base_url('assets/images/logo-sewatama.png') ?>" alt="Logo Sewatama"/>
                            <div class="pull-right"><?= date("d F Y")?></div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <section class="panel panel-default">

                                <header class="panel-heading text-right bg-light">
                                    <ul class="nav nav-tabs pull-left">
                                        <li class="active"><a href="#tab1" data-toggle="tab"> <i class="fa fa-list text-default"></i> Project Detail </a></li>
                                    </ul>
                                    <span class="hidden-sm">&nbsp;</span>
                                </header>

                                <div class="panel-body">

                                    <div class="tab-content">

                                        <?php if(isset($dt_project)) { foreach ($dt_project as $rowData) { ?>
                                            <div class="tab-pane fade active in" id="tab1">
                                                <form class="bs-example form-horizontal">

                                                    <div class="row">
                                                        <div class="col-md-6 col-xs-6">
                                                            <dl class="dl-horizontal">
                                                                <dt>Project ID:</dt> <dd> <strong><?= $rowData->id_project ?></strong></dd>
                                                                <br/>
                                                                <dt>Customer:</dt> <dd><?= $rowData->customer ?></dd>
                                                                <br/>
                                                                <dt>Cust. Address:</dt> <dd><?= $rowData->address ?></dd>
                                                                <br/>
                                                                <dt>Cust. Email:</dt> <dd><?= $rowData->email ?></dd>
                                                                <br/>
                                                                <dt>Cust. Phone:</dt> <dd><?= $rowData->phone ?></dd>
                                                            </dl>
                                                        </div>

                                                        <div class="col-md-6 col-xs-6">
                                                            <dl class="dl-horizontal">
                                                                <div class="hidden-print">
                                                                    <dt>Employee:</dt>
                                                                    <dd style="margin-left: 155px">
                                                                        <ol>
                                                                            <?php if(isset($dt_project_employee)) { foreach ($dt_project_employee as $row) { ?>
                                                                                <li>SN : <?= $row->sn_employee ?> ( <?= $row->nama ?> ) </li>
                                                                            <?php } } ?>
                                                                        </ol>
                                                                    </dd>
                                                                </div>
                                                                <div class="visible-print">
                                                                    <dt>Employee:</dt>
                                                                    <dd style="margin-left:-25px">
                                                                        <ol>
                                                                            <?php if(isset($dt_project_employee)) { foreach ($dt_project_employee as $row) { ?>
                                                                                <li>SN : <?= $row->sn_employee ?> ( <?= $row->nama ?> ) </li>
                                                                            <?php } } ?>
                                                                        </ol>
                                                                    </dd>
                                                                </div>
                                                                <br/>

                                                                <dt>Start of Contract: </dt><dd><?= date('d-m-Y',strtotime($rowData->start_date))?></dd>
                                                                <br/>
                                                                <dt>End of Contract: </dt><dd><?= date('d-m-Y',strtotime($rowData->end_date))?></dd>
                                                                <br/>
                                                                <dt>Utility/Non-Utility: </dt><dd class="text-uc"><?= $rowData->utility ?></dd>
                                                                <br/>
                                                                <dt>Project Location: </dt><dd><?= $rowData->kota ?></dd>
                                                            </dl>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">GENSET LIST</div>
                                                                <div class="panel-body">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped table-bordered">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>No</th>
                                                                                <th>SN</th>
                                                                                <th>Model</th>
                                                                                <th>Brand</th>
                                                                                <th>Spec</th>
                                                                                <th>Capacity (Mw) </th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>

                                                                            <?php $no=1; if(isset($dt_project_genset)){ foreach($dt_project_genset as $row) { ?>

                                                                                <tr>
                                                                                    <td><?= $no++; ?></td>
                                                                                    <td><?= $row->sn_genset?></td>
                                                                                    <td><?= $row->model?></td>
                                                                                    <td><?= $row->brand?></td>
                                                                                    <td><?= $row->spec?></td>
                                                                                    <td><?= $row->capacity?> Mw</td>
                                                                                </tr>
                                                                            <?php } } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="hidden-print">
                                                        <div class="line line-dashed"></div>
                                                        <div class="row">
                                                            <div class="pull-left">
                                                                <div class="col-lg-offset-2 col-lg-10">
                                                                    <a href="<?= site_url('project')?>" class="btn btn-sm btn-default"> <i class="fa fa-chevron-left"></i> Back</a>
                                                                </div>
                                                            </div>

                                                            <button href="#" class="btn btn-sm btn-dark pull-right m-r-md" onClick="window.print();">
                                                                <i class="fa fa-print"></i> Print
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        <?php } } ?>
                                    </div>
                                </div>

                            </section>

                        </div>
                    </div>
                </section>
            </section>

            <!--======================= NAV TOGGLE ========================= -->
            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav">
            </a>

        </section>
    </section>
</section>