<section>
    <section class="hbox stretch">
        <!--======================= SIDEBAR MENU ========================= -->
        <?php $this->load->view('element/_sidebar_menu')?>

        <!--======================= CONTENT HERE ========================= -->
        <section id="content">
            <section class="vbox">
                <section class="scrollable padder">

                    <!--======================= HEADER CONTENT ========================= -->
                    <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                        <li><a href="<?= site_url('home')?>"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Employee Project</li>
                    </ul>
                    <div class="m-b-md">
                        <h3 class="m-b-none"><i class="fa fa-2x fa-cogs"></i> Update Employee Project </h3>
                    </div>

                    <!--======================= CONTENT WRAPPER ========================= -->
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel panel-default">

                                <header class="panel-heading text-right bg-light">
                                    <ul class="nav nav-tabs pull-left">
                                        <li class="active"><a href="#tab1" data-toggle="tab"> <i class="fa fa-list text-default"></i> Update Data </a></li>
                                    </ul>
                                    <span class="hidden-sm">&nbsp;</span>
                                </header>

                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="tab1">

                                            <?php if(isset($dt_project)) { foreach ($dt_project as $rowData) { ?>
                                                <form method="post" action="<?= site_url('project/update_data')?>">
                                                    <div class="bs-example form-horizontal">
                                                        <input type="hidden" name="update" value="<?php echo date('d M Y')?>">
                                                        <input type="hidden" name="user_update" value="<?= $this->session->userdata('ID')?>">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                    <label class="col-lg-4 control-label">PROJECT ID</label>
                                                                    <div class="col-lg-8">
                                                                        <input name="id_project" type="text" class="form-control" value="<?= $rowData->id_project;?>" readonly>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-lg-4 control-label">CUSTOMER</label>
                                                                    <div class="col-lg-8">
                                                                        <select id="id_customer" name="id_customer" class="select2-option" style="width:100%">
                                                                            <?php if(isset($dt_customer)){foreach ($dt_customer as $row){
                                                                                if($row->id_customer == $rowData->id_customer){
                                                                                    $selected = "selected=selected";
                                                                                }else{
                                                                                    $selected = "";
                                                                                }?>
                                                                                <option <?php echo $selected; ?> value="<?php echo $row->id_customer ?>">
                                                                                    <?php echo $row->customer ?>
                                                                                </option>
                                                                            <?php } }?>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div id="detailCustomer"></div>
                                                            </div>

                                                            <div class="col-md-7">
                                                                <div class="form-group">
                                                                    <label class="col-lg-3 control-label">START OF CONTRACT</label>
                                                                    <div class="col-lg-9">
                                                                        <input name="start_date" class="input-sm input-s datepicker-input form-control" type="text" data-date-format="yyyy-mm-dd" value="<?= $rowData->start_date?>">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-lg-3 control-label">END OF CONTRACT</label>
                                                                    <div class="col-lg-9">
                                                                        <input name="end_date" class="input-sm input-s datepicker-input form-control" type="text" data-date-format="yyyy-mm-dd" value="<?= $rowData->end_date?>">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-lg-3 control-label">UTILITY</label>
                                                                    <div class="col-lg-9">
                                                                        <select name="utility" class="select2-option" style="width:100%">
                                                                            <?php if($rowData->utility == "utility"){
                                                                                $selected = "selected=selected";?>
                                                                                <option <?php echo $selected; ?> value="utility">UTILITY</option>
                                                                                <option value="non-utility">UTILITY</option>
                                                                            <?php } elseif($rowData->utility == "non-utility"){
                                                                                $selected = "selected=selected";?>
                                                                                <option value="utility">UTILITY</option>
                                                                                <option <?php echo $selected; ?> value="non-utility">NON-UTILITY</option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-lg-3 control-label">PROJECT LOCATION</label>
                                                                    <div class="col-lg-9">
                                                                        <select name="lokasi" class="select2-option" style="width:100%">
                                                                            <?php if(isset($dt_kota)){foreach ($dt_kota as $row){
                                                                                if($row->id_kota == $rowData->lokasi){
                                                                                    $selected = "selected=selected";
                                                                                }else{
                                                                                    $selected = "";
                                                                                }?>
                                                                                <option <?php echo $selected; ?> value="<?php echo $row->id_kota ?>">
                                                                                    <?php echo $row->kota ?>
                                                                                </option>
                                                                            <?php } }?>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div id="recentEmployee" class="form-group">
                                                                    <label class="col-lg-3 control-label">PILIHAN EMPLOYEE SEBELUMNYA
                                                                        <a id="changeEmp" href="#" class="label label-info">Change Employee</a>
                                                                    </label>
                                                                    <div class="col-lg-9">
                                                                        <div class="panel panel-default">
                                                                            <div class="panel-body">
                                                                                <div class="table-responsive">
                                                                                    <table class="table table-striped">
                                                                                        <thead>
                                                                                        <tr>
                                                                                            <th>SN</th>
                                                                                            <th>NAME</th>
                                                                                        </tr>
                                                                                        </thead>
                                                                                        <tbody>

                                                                                        <?php if(isset($dt_project_employee)){ foreach($dt_project_employee as $row) { ?>

                                                                                            <tr>
                                                                                                <td><?= $row->sn_employee?></td>
                                                                                                <td><?= $row->nama?></td>
                                                                                            </tr>
                                                                                        <?php } } ?>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div id="pilEmployee" class="form-group hide">
                                                                    <label class="col-lg-3 control-label">EMPLOYEE</label>
                                                                    <div class="col-lg-9">
                                                                        <select name="id_employee[]" class="select2-option" style="width:100%" multiple>
                                                                            <?php if(isset($dt_employee)){ foreach($dt_employee as $row ) { ?>
                                                                                <option value="<?= $row->id_employee?>">
                                                                                    SN : <?= $row->sn_employee ?> ( <?= $row->nama ?> )
                                                                                </option>
                                                                            <?php } }?>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div id="recentGenset" class="form-group">
                                                                    <label class="col-lg-3 control-label">PILIHAN GENSET SEBELUMNYA
                                                                        <a id="changeGenset" href="#" class="label label-info">Change Genset</a>
                                                                    </label>
                                                                    <div class="col-lg-9">
                                                                        <div class="panel panel-default">
                                                                            <div class="panel-body">
                                                                                <div class="table-responsive">
                                                                                    <table class="table table-striped">
                                                                                        <thead>
                                                                                        <tr>
                                                                                        <tr>
                                                                                            <th>SN</th>
                                                                                            <th>Model</th>
                                                                                            <th>Brand</th>
                                                                                            <th>Spec</th>
                                                                                            <th>Capacity (Mw) </th>
                                                                                        </tr>
                                                                                        </tr>
                                                                                        </thead>
                                                                                        <tbody>

                                                                                        <?php if(isset($dt_project_genset)){ foreach($dt_project_genset as $row) { ?>

                                                                                            <tr>
                                                                                                <td><?= $row->sn_genset?></td>
                                                                                                <td><?= $row->model?></td>
                                                                                                <td><?= $row->brand?></td>
                                                                                                <td><?= $row->spec?></td>
                                                                                                <td><?= $row->capacity?> Mw</td>
                                                                                            </tr>
                                                                                        <?php } } ?>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="pilGenset" class="row hide">
                                                        <div class="col-md-12">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">GENSETS LIST <small class="text-danger"><em><u>checklist seluruh genset yang ingin dipilih jika ada perubahan</u></em></small></div>
                                                                <div class="panel-body">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped" data-ride="datatables">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    <input type="checkbox" style="width: 14px !important;">
                                                                                </th>
                                                                                <th>SN</th>
                                                                                <th>Brand</th>
                                                                                <th>Type / Spec</th>
                                                                                <th>Capacity (Mw) </th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>

                                                                            <?php if(isset($dt_genset)){ foreach($dt_genset as $row) { ?>

                                                                                <tr>
                                                                                    <td>
                                                                                        <input type="checkbox" name="id_genset[]" value="<?= $row->id_genset?>" style="width: 14px !important;" >
                                                                                    </td>
                                                                                    <td><?= $row->sn_genset?></td>
                                                                                    <td><?= $row->brand?></td>
                                                                                    <td><?= $row->spec?></td>
                                                                                    <td><?= $row->capacity?> Mw</td>
                                                                                </tr>
                                                                            <?php } } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="line line-dashed"></div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <a href="<?= site_url('project')?>" class="btn btn-sm btn-default">Cancel & Back To Project List</a> &nbsp;
                                                            <button type="submit" class="btn btn-sm btn-dark">Update Data</button>
                                                        </div>
                                                    </div>

                                                </form>
                                            <?php } } ?>

                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
            </section>

            <!--======================= NAV TOGGLE ========================= -->
            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav">
            </a>

        </section>
    </section>
</section>

<script type="text/javascript">
    $('#changeEmp').click(function(){
        $('#recentEmployee').addClass('hide');
        $('#pilEmployee').removeClass('hide');
    });
    $('#changeGenset').click(function(){
        $('#recentGenset').addClass('hide');
        $('#pilGenset').removeClass('hide');
    });

    $(document).ready(function(){
        var id_customer = {id_customer:$("#id_customer option:selected").val()};
        $.ajax({
            type: "POST",
            url : "<?php echo base_url('project/ajaxLoadCust'); ?>",
            data: id_customer,
            success: function(data){
                $('#detailCustomer').html(data);
            }
        });

        $("#id_customer").change(function(){
            var id_customer = {id_customer:$("#id_customer option:selected").val()};
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('project/ajaxLoadCust'); ?>",
                data: id_customer,
                success: function(data){
                    $('#detailCustomer').html(data);
                }
            });
        });
    });
</script>