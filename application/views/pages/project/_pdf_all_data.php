<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title><?= $title; ?></title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" href="<?= base_url('assets/images/favicon.png')?>">

    <!--  CSS BASE-->
    <link rel="stylesheet" href="<?= base_url('assets/css/app.css')?>" type="text/css" />
    <link rel="stylesheet" href="<?= base_url('assets/css/font.css')?>" type="text/css" />
</head>
<body>

<div class="container">
    <div class="row">
        <div class="m-b-md col-xs-12">
            <img class="thumb-lg" src="<?= base_url('assets/images/logo-sewatama.png') ?>" alt="Logo Sewatama"/>
            <div class="pull-right"><?= date("d F Y")?></div>
        </div>
        <div class="col-xs-12 well">
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th width="8%">No</th>
                        <th>PROJECT ID</th>
                        <th>CUSTOMER NAME</th>
                        <th>CONTRACT DATE</th>
                        <th>UTILITY</th>
                        <th>LOCATION</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php $no=1; if(isset($dt_project)){ foreach($dt_project as $row) { ?>

                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $row->id_project?></td>
                            <td><?= $row->customer?></td>
                            <td>
                                Start : <?= date("d F Y",strtotime($row->start_date));?>
                                <br/>
                                End : <?= date("d F Y",strtotime($row->end_date));?>
                            </td>
                            <td><?= $row->customer?></td>
                            <td><?= $row->kota?></td>
                        </tr>
                    <?php } } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
