<?php if(isset($ajax_customer)) { foreach ($ajax_customer as $row) { ?>

    <div class="form-group">
        <label class="col-lg-4 control-label">EMAIL</label>
        <div class="col-lg-8">
            <input class="form-control" size="16" type="text" value="<?= $row->email?>" readonly>
        </div>
    </div>

    <div class="form-group">
        <label class="col-lg-4 control-label">ADDRESS</label>
        <div class="col-lg-8">
            <textarea rows="5" class="form-control" readonly><?= $row->address?></textarea>
        </div>
    </div>

<?php } } ?>