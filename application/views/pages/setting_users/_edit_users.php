<section>
    <section class="hbox stretch">
        <!--======================= SIDEBAR MENU ========================= -->
        <?php $this->load->view('element/_sidebar_menu')?>

        <!--======================= CONTENT HERE ========================= -->
        <section id="content">
            <section class="vbox">
                <section class="scrollable padder">

                    <!--======================= HEADER CONTENT ========================= -->
                    <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                        <li><a href="<?= site_url('home')?>"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
                        <li class="active">Setting Users</li>
                    </ul>
                    <div class="m-b-md">
                        <h3 class="m-b-none"><i class="fa fa-2x fa-cogs"></i> Setting Users</h3>
                    </div>

                    <!--======================= CONTENT WRAPPER ========================= -->
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel panel-default">

                                <header class="panel-heading text-right bg-light">
                                    <ul class="nav nav-tabs pull-left">
                                        <li class="active"><a href="#tab1" data-toggle="tab"> <i class="fa fa-edit text-default"></i> Edit Data </a></li>
                                    </ul>
                                    <span class="hidden-sm">&nbsp;</span>
                                </header>

                                <div class="panel-body">

                                    <div class="tab-content">

                                        <div class="tab-pane fade active in" id="tab1">

                                            <?php if(isset($dt_users)) { foreach ($dt_users as $row) { ?>

                                            <form class="bs-example form-horizontal" method="post" action="<?= site_url('set_user/update_data')?>" enctype="multipart/form-data">

                                                <input name="id_user" type="hidden" class="form-control" value="<?= $row->id_user?>">

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Name</label>
                                                    <div class="col-lg-10">
                                                        <input name="name" type="text" class="form-control" value="<?= $row->name?>" required="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Username</label>
                                                    <div class="col-lg-10">
                                                        <input name="username" type="text" class="form-control" value="<?= $row->username?>" required="">
                                                        <span class="help-block m-b-none">Username used for login process</span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Password</label>
                                                    <div class="col-lg-10">
                                                        <input name="password" type="password" class="form-control" required="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Foto</label>
                                                    <div class="col-lg-10">
                                                        <div class="thumb-md"  style="margin-right: 15px">
                                                            <?php if(isset($row->user_img)) { ?>
                                                                <img class="img-responsive" src="<?php echo base_url('uploads/photos/'.$row->user_img)?>" alt="foto karyawan">
                                                            <?php } else { ?>
                                                                <img class="img-responsive" src="<?php echo base_url('assets/images/avatar_default.jpg')?>" alt="foto karyawan">
                                                            <?php } ?>
                                                        </div>
                                                        <br/>
                                                        <input id="userfile" name="userfile" type="file" class="filestyle" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline input-s">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-lg-offset-2 col-lg-10">
                                                        <button type="submit" class="btn btn-sm btn-dark">Update Data</button>
                                                        <a href="<?= site_url('set_user')?>" class="btn btn-sm btn-default">Cancel</a>
                                                    </div>
                                                </div>

                                            </form>

                                            <?php } } ?>

                                        </div>
                                    </div>
                                </div>

                            </section>

                        </div>
                    </div>
                </section>
            </section>

            <!--======================= NAV TOGGLE ========================= -->
            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav">
            </a>

        </section>
    </section>
</section>