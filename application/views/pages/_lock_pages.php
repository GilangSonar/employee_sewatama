<div class="modal-over">

    <div class="modal-center animated fadeInUp text-center" style="width:200px;margin:-80px 0 0 -100px;">

        <div class="thumb-md">
            <img src="<?= base_url('uploads/photos/'.$this->session->userdata('IMG'))?>" class="img-circle b-a b-light b-3x">
        </div>
        <p class="text-white h4 m-t m-b"><?= $this->session->userdata('NAME')?></p>

        <form action="<?= site_url('login/cek_login') ?>" method="post">
            <div class="input-group">
                <input name="username" value="<?= $this->session->userdata('USERNAME')?>" type="hidden">
                <input name="password" type="password" class="form-control text-sm" placeholder="Enter pwd to continue">
            <span class="input-group-btn">
                <button class="btn btn-success" type="submit">
                    <i class="fa fa-arrow-right"></i>
                </button>
            </span>
            </div>
        </form>

    </div>
</div>