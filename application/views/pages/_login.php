<!DOCTYPE html>
<html lang="en" class="bg-dark">
<head>
    <meta charset="utf-8" />
    <title><?= $title; ?></title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <link rel="stylesheet" href="<?= base_url('assets/css/app.css')?>" type="text/css" />
    <link rel="stylesheet" href="<?= base_url('assets/css/font.css')?>" type="text/css" />

    <!--[if lt IE 9]>
    <script src="<?= base_url('assets/js/ie/html5shiv.js')?>"></script>
    <script src="<?= base_url('assets/s/ie/respond.min.js')?>j"></script>
    <script src="<?= base_url('assets/js/ie/excanvas.js')?>"></script>
    <![endif]-->

    <link rel="shortcut icon" href="<?= base_url('assets/images/favicon.png')?>">
</head>

<body>
<section id="content" class="m-t-lg wrapper-md animated fadeInUp">

    <div class="container aside-xxl">
        <a class="navbar-brand block" href="<?= site_url('')?>"> SEWATAMA
            <h5>Employee Management</h5>
            <!-- NOTIF -->
            <?php
            $message = $this->session->flashdata('notif');
            if($message){
                echo '<h5 class="text-uc text-danger">'.$message .'</h5>';
            }?>
        </a>
        <section class="panel panel-default bg-white m-t-lg">
            <header class="panel-heading text-center"> <strong>Sign in</strong> </header>

            <form action="<?= site_url('login/cek_login')?>" class="panel-body wrapper-lg" method="post">

                <div class="form-group">
                    <label class="control-label">Username
                    </label>
                    <input name="username" type="text" placeholder="Username..." class="form-control input-lg" required="">
                </div>

                <div class="form-group">
                    <label class="control-label">Password
                    </label>
                    <input name="password" type="password" placeholder="Password..." class="form-control input-lg" required="">
                </div>

                <div class="line line-dashed"></div>
<!--                <a href="--><?//= site_url('home')?><!--" class="btn btn-dark">Sign in</a>-->
                <button type="submit" class="btn btn-primary">Sign in</button>
                <div class="line line-dashed"></div>

            </form>
        </section>
    </div>
</section>

<footer id="footer">
    <div class="text-center padder">
        <p>
            <small>SEWATAMA Employee Management<br>&copy; <?php echo date('Y')?></small>
        </p>
    </div>
</footer>

<!--================ SCRIPT ====================== -->
<script src="<?= base_url('assets/js/app.js')?>"></script>


</body>
</html>