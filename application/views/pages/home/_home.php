<script>
    $(document).ready(function(){
        $(function () {

// ============== CHART BY LOCATION =====================
            $('#pie-charts1').highcharts({
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: 'View Project By Locations'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Total ',
                    data: [
                        <?php isset($dt_charts_area) ? $dt_charts_area : $dt_charts_area = array();
                        foreach ($dt_charts_area as $row) { ?>
                        [ '<?= $row->lokasi?>', <?= $row->jml ?>],
                        <?php }
                        ?>
                    ]
                }]
            });

// ============== CHART BY CUSTOMER =====================
            $('#pie-charts2').highcharts({
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: 'View Project By Customer'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Total ',
                    data: [
                        <?php isset($dt_charts_customer) ? $dt_charts_customer : $dt_charts_customer = array();
                        foreach ($dt_charts_customer as $row) { ?>
                        [ '<?= $row->customer?>', <?= $row->jml ?>],
                        <?php }
                        ?>
                    ]
                }]
            });
        });
    });

</script>


<section>
    <section class="hbox stretch">
        <!--======================= SIDEBAR MENU ========================= -->
        <?php $this->load->view('element/_sidebar_menu')?>

        <!--======================= CONTENT HERE ========================= -->
        <section id="content">
            <section class="vbox">
                <section class="scrollable padder">

                    <!--======================= HEADER CONTENT ========================= -->
                    <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                        <li><a href="<?= site_url()?>"><i class="fa fa-home"></i> Home</a></li>
                    </ul>
                    <div class="m-b-md">
                        <h3 class="m-b-none"> <i class="fa fa-2x fa-home"></i> Home</h3> <small>Hello, <?= $this->session->userdata('NAME')?> </small>
                    </div>

                    <!--======================= CONTENT WRAPPER ========================= -->
                    <section class="panel panel-default">
                        <div class="row m-l-none m-r-none bg-light lter">

                            <div class="col-sm-6 col-md-3 padder-v b-r b-light">
                <span class="fa-stack fa-2x pull-left m-r-sm">
                    <i class="fa fa-circle fa-stack-2x text-info"></i>
                    <i class="fa fa-tasks fa-stack-1x text-white"></i>
                </span>
                                <a class="clear" href="#">
                    <span class="h3 block m-t-xs"><strong><?= $count_project?></strong>
                    </span>
                                    <small class="text-muted text-uc">Total Project</small>
                                </a>
                            </div>

                            <div class="col-sm-6 col-md-3 padder-v b-r b-light">
                <span class="fa-stack fa-2x pull-left m-r-sm">
                    <i class="fa fa-circle fa-stack-2x text-info"></i>
                    <i class="fa fa-users fa-stack-1x text-white"></i>
                </span>
                                <a class="clear" href="#">
                    <span class="h3 block m-t-xs"><strong><?= $count_employee?></strong>
                    </span>
                                    <small class="text-muted text-uc">Total Employee</small>
                                </a>
                            </div>

                            <div class="col-sm-6 col-md-3 padder-v b-r b-light">
                <span class="fa-stack fa-2x pull-left m-r-sm">
                    <i class="fa fa-circle fa-stack-2x text-info"></i>
                    <i class="fa fa-male fa-stack-1x text-white"></i>
                </span>
                                <a class="clear" href="#">
                    <span class="h3 block m-t-xs"><strong><?= $count_customer?></strong>
                    </span>
                                    <small class="text-muted text-uc">Total Customer</small>
                                </a>
                            </div>

                            <div class="col-sm-6 col-md-3 padder-v b-r b-light">
                <span class="fa-stack fa-2x pull-left m-r-sm">
                    <i class="fa fa-circle fa-stack-2x text-info"></i>
                    <i class="fa fa-gears fa-stack-1x text-white"></i>
                </span>
                                <a class="clear" href="#">
                    <span class="h3 block m-t-xs"><strong><?= $count_genset?></strong>
                    </span>
                                    <small class="text-muted text-uc">Total Gensets</small>
                                </a>
                            </div>

                        </div>
                    </section>

                    <div class="row">
                        <div class="col-md-6">
                            <section class="panel panel-default">
                                <header class="panel-heading font-bold">
                                    Project By Locations
                                </header>

                                <div class="panel-body">
                                    <div id="pie-charts1" style="height:210px">
                                    </div>
                                </div>
                            </section>
                        </div>

                        <div class="col-md-6">
                            <section class="panel panel-default">
                                <header class="panel-heading font-bold">
                                    Project By Customer
                                </header>

                                <div class="panel-body">
                                    <div id="pie-charts2" style="height:210px">
                                    </div>
                                </div>
                            </section>
                        </div>

                    </div>
                </section>
            </section>

            <!--======================= NAV TOGGLE ========================= -->
            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav">
            </a>

        </section>
    </section>
</section>