<section>
    <section class="hbox stretch">
        <!--======================= SIDEBAR MENU ========================= -->
        <?php $this->load->view('element/_sidebar_menu')?>

        <!--======================= CONTENT HERE ========================= -->
        <section id="content">
            <section class="vbox">
                <section class="scrollable padder">

                    <!--======================= HEADER CONTENT ========================= -->
                    <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                        <li><a href="<?= site_url('home')?>"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
                        <li class="active">Setting Users</li>
                    </ul>
                    <div class="m-b-md">
                        <h3 class="m-b-none"><i class="fa fa-2x fa-cogs"></i> Setting Users</h3>
                    </div>

                    <!--======================= CONTENT WRAPPER ========================= -->
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel panel-default">

                                <header class="panel-heading text-right bg-light">
                                    <ul class="nav nav-tabs pull-left">
                                        <li class="active"><a href="#tab1" data-toggle="tab"> <i class="fa fa-edit text-default"></i> Edit Data </a></li>
                                    </ul>
                                    <span class="hidden-sm">&nbsp;</span>
                                </header>

                                <div class="panel-body">

                                    <div class="tab-content">

                                        <div class="tab-pane fade active in" id="tab1">

                                            <?php if(isset($dt_customer)) { foreach ($dt_customer as $row) { ?>

                                            <form class="bs-example form-horizontal" method="post" action="<?= site_url('set_customer/update_data')?>">

                                                <input name="id_customer" type="hidden" class="form-control" value="<?= $row->id_customer?>">

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Customer</label>
                                                    <div class="col-lg-10">
                                                        <input name="customer" type="text" class="form-control" value="<?= $row->customer?>" required="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Email</label>
                                                    <div class="col-lg-10">
                                                        <input name="email" type="email" class="form-control" value="<?= $row->email?>" required="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Phone</label>
                                                    <div class="col-lg-10">
                                                        <input name="phone" type="text" class="form-control" value="<?= $row->phone?>" required="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Address</label>
                                                    <div class="col-lg-10">
                                                        <textarea name="address" class="form-control" rows="3"><?= $row->address?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-lg-offset-2 col-lg-10">
                                                        <button type="submit" class="btn btn-sm btn-dark">Update Data</button>
                                                        <a href="<?= site_url('set_customer')?>" class="btn btn-sm btn-default">Cancel</a>
                                                    </div>
                                                </div>

                                            </form>

                                            <?php } } ?>

                                        </div>
                                    </div>
                                </div>

                            </section>

                        </div>
                    </div>
                </section>
            </section>

            <!--======================= NAV TOGGLE ========================= -->
            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav">
            </a>

        </section>
    </section>
</section>
</section>