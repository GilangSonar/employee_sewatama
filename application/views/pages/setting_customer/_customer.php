<section>
    <section class="hbox stretch">
        <!--======================= SIDEBAR MENU ========================= -->
        <?php $this->load->view('element/_sidebar_menu')?>

        <!--======================= CONTENT HERE ========================= -->
        <section id="content">
            <section class="vbox">
                <section class="scrollable padder">

                    <!--======================= HEADER CONTENT ========================= -->
                    <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                        <li><a href="<?= site_url('home')?>"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
                        <li class="active">Setting Customer</li>
                    </ul>
                    <div class="m-b-md">
                        <h3 class="m-b-none"><i class="fa fa-2x fa-cogs"></i> Setting Customer</h3>
                    </div>

                    <!--======================= CONTENT WRAPPER ========================= -->
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel panel-default">

                                <header class="panel-heading text-right bg-light">
                                    <ul class="nav nav-tabs pull-left">
                                        <li class="active"><a href="#tab1" data-toggle="tab"> <i class="fa fa-list text-default"></i> All Record </a></li>
                                        <li class=""><a href="#tab2" data-toggle="tab"><i class="fa fa-plus-circle text-default"></i> Add New Customer</a></li>
                                    </ul>
                                    <span class="hidden-sm">&nbsp;</span>
                                </header>

                                <div class="panel-body">

                                    <div class="tab-content">

                                        <div class="tab-pane fade active in" id="tab1">
                                            <div class="table-responsive">
                                                <table class="table table-striped m-b-none" data-ride="datatables">
                                                    <thead>
                                                    <tr>
                                                        <th width="8%">No</th>
                                                        <th>Customer</th>
                                                        <th>Email</th>
                                                        <th>Phone</th>
                                                        <th>Address</th>
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <?php $no=1; if(isset($dt_customer)){ foreach($dt_customer as $row) { ?>

                                                    <tr>
                                                        <td><?= $no++; ?></td>
                                                        <td><?= $row->customer?></td>
                                                        <td><?= $row->email?></td>
                                                        <td><?= $row->phone?></td>
                                                        <td><?= $row->address?></td>
                                                        <td class="text-center">

                                                            <div class="btn-group"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></a>
                                                                <ul class="dropdown-menu pull-right">
                                                                    <li>
                                                                        <a href="<?= site_url('set_customer/edit_pages/'.$row->id_customer)?>">
                                                                            <i class="fa fa-pencil-square text-info"></i> Edit
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#alertDelete" data-toggle="modal">
                                                                            <i class="fa fa-times-circle text-danger"></i> Delete
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                        <!-- MODAL ALERT DELETE-->
                                                        <div class="modal" id="alertDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title" id="myModalLabel">DELETE DATA</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="alert alert-danger text-center">
                                                                            <h4>Are You Sure Want To Delete This Data ? </h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        <a href="<?= site_url('set_customer/delete_data/'.$row->id_customer)?>" class="btn btn-danger">Delete</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    <?php } } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <!--======================== ADD NEW FORMS ============================= -->
                                        <div class="tab-pane fade" id="tab2">
                                            <form class="bs-example form-horizontal" method="post" action="<?= site_url('set_customer/input_data')?>">
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Customer</label>
                                                    <div class="col-lg-10">
                                                        <input name="customer" type="text" class="form-control" placeholder="Customer Name..." required="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Email</label>
                                                    <div class="col-lg-10">
                                                        <input name="email" type="email" class="form-control" placeholder="Customer Email..." required="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Phone</label>
                                                    <div class="col-lg-10">
                                                        <input name="phone" type="text" class="form-control" placeholder="Customer Phone..." required="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Address</label>
                                                    <div class="col-lg-10">
                                                        <textarea name="address" class="form-control" rows="3"></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-lg-offset-2 col-lg-10">
                                                        <button type="submit" class="btn btn-sm btn-dark">Save Data</button>
                                                    </div>
                                                </div>

                                            </form>

                                        </div>
                                    </div>
                                </div>

                            </section>

                        </div>
                    </div>
                </section>
            </section>

            <!--======================= NAV TOGGLE ========================= -->
            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav">
            </a>

        </section>
    </section>
</section>
</section>