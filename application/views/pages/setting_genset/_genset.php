<?php if ($this->session->flashdata('act_tab_brand') == true) : ?>
    <script>
        $(function(){
            $('#liTab1,#tab1').removeClass('active');
            $('#tabSetGenset a[href="#tab3"]').tab('show');
        });
    </script>
<?php endif; ?>

<section>
    <section class="hbox stretch">
        <!--======================= SIDEBAR MENU ========================= -->
        <?php $this->load->view('element/_sidebar_menu')?>

        <!--======================= CONTENT HERE ========================= -->
        <section id="content">
            <section class="vbox">
                <section class="scrollable padder">

                    <!--======================= HEADER CONTENT ========================= -->
                    <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                        <li><a href="<?= site_url('home')?>"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
                        <li class="active">Setting Gensets</li>
                    </ul>
                    <div class="m-b-md">
                        <h3 class="m-b-none"><i class="fa fa-2x fa-cogs"></i> Setting Gensets</h3>
                    </div>

                    <!--======================= CONTENT WRAPPER ========================= -->
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel panel-default">

                                <header class="panel-heading text-right bg-light">
                                    <ul id="tabSetGenset" class="nav nav-tabs pull-left">
                                        <li id="liTab1" class="active"><a href="#tab1" data-toggle="tab"> <i class="fa fa-list text-default"></i> All Record </a></li>
                                        <li><a href="#tab2" data-toggle="tab"><i class="fa fa-plus-circle text-default"></i> Add New Gensets</a></li>
                                        <li><a href="#tab3" data-toggle="tab"><i class="fa fa-archive text-default"></i> Brand of Gensets</a></li>
                                    </ul>
                                    <span class="hidden-sm">&nbsp;</span>
                                </header>

                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="tab1">
                                            <div class="table-responsive">
                                                <table class="table table-striped m-b-none" data-ride="datatables">
                                                    <thead>
                                                    <tr>
                                                        <th width="8%">No</th>
                                                        <th>SN</th>
                                                        <th>Model</th>
                                                        <th>Brand</th>
                                                        <th>Spec</th>
                                                        <th>Capacity (Mw) </th>
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <?php $no=1; if(isset($dt_genset)){ foreach($dt_genset as $row) { ?>

                                                    <tr>
                                                        <td><?= $no++; ?></td>
                                                        <td><?= $row->sn_genset?></td>
                                                        <td><?= $row->model?></td>
                                                        <td><?= $row->brand?></td>
                                                        <td><?= $row->spec?></td>
                                                        <td><?= $row->capacity?> Mw</td>
                                                        <td class="text-center">

                                                            <div class="btn-group"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></a>
                                                                <ul class="dropdown-menu pull-right">
                                                                    <li>
                                                                        <a href="<?= site_url('set_genset/edit_pages/'.$row->id_genset)?>">
                                                                            <i class="fa fa-pencil-square text-info"></i> Edit
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#alertDelete" data-toggle="modal">
                                                                            <i class="fa fa-times-circle text-danger"></i> Delete
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                        <!-- MODAL ALERT DELETE-->
                                                        <div class="modal" id="alertDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title" id="myModalLabel">DELETE DATA</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="alert alert-danger text-center">
                                                                            <h4>Are You Sure Want To Delete This Data ? </h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        <a href="<?= site_url('set_genset/delete_data/'.$row->id_genset)?>" class="btn btn-danger">Delete</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    <?php } } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <!--======================== ADD NEW FORMS ============================= -->
                                        <div class="tab-pane fade" id="tab2">
                                            <form class="bs-example form-horizontal" method="post" action="<?= site_url('set_genset/input_data')?>">
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Brand</label>
                                                    <div class="col-lg-10">
                                                        <select name="id_brand" class="select2-option" style="width:260px" required="">
                                                            <option value="">:: Pilih Brand Genset :: </option>
                                                            <?php if(isset($dt_brand)){ foreach ($dt_brand as $row) { ?>
                                                                <option value="<?= $row->id_brand?>"><?= $row->brand?></option>
                                                            <?php } } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">SN GENSET</label>
                                                    <div class="col-lg-10">
                                                        <input name="sn_genset" type="text" class="form-control" placeholder="Serial Number" required="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">MODEL</label>
                                                    <div class="col-lg-10">
                                                        <input name="model" type="text" class="form-control" placeholder="Model Number" required="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Specification</label>
                                                    <div class="col-lg-10">
                                                        <textarea name="spec" class="form-control" rows="3"></textarea>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-lg-2 control-label">Capacity</label>
                                                    <div class="col-lg-10">
                                                        <input name="capacity" type="text" class="form-control" placeholder="Capacity in MW..." required="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-lg-offset-2 col-lg-10">
                                                        <button type="submit" class="btn btn-sm btn-dark">Save Data</button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>

                                        <!--======================== BRAND OF GENSET ============================= -->
                                        <div class="tab-pane fade" id="tab3">
                                            <div class="row">

                                                <div class="col-md-4">
                                                    <div class="header">
                                                        <h3>Add New Brand</h3>
                                                    </div>
                                                    <form class="bs-example form-horizontal" method="post" action="<?= site_url('set_genset/input_data_brand')?>">
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">Brand Name</label>
                                                            <div class="col-lg-10">
                                                                <input name="brand" type="text" class="form-control" placeholder="Brand of Genset... " required="" maxlength="25">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-lg-offset-2 col-lg-10">
                                                                <button type="submit" class="btn btn-sm btn-dark">Add Data</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>

                                                <div class="col-md-8">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped m-b-none" data-ride="datatables">
                                                            <thead>
                                                            <tr>
                                                                <th width="8%">No</th>
                                                                <th>Brand</th>
                                                                <th class="text-center">Action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            <?php $no=1; if(isset($dt_brand)){ foreach($dt_brand as $row) { ?>

                                                                <tr>
                                                                    <td><?= $no++; ?></td>
                                                                    <td><?= $row->brand?></td>
                                                                    <td class="text-center">

                                                                        <div class="btn-group"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></a>
                                                                            <ul class="dropdown-menu pull-right">
                                                                                <li>
                                                                                    <a href="#editBrand<?= $row->id_brand?>" data-toggle="modal">
                                                                                        <i class="fa fa-pencil-square text-info"></i> Edit
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#deleteBrand" data-toggle="modal">
                                                                                        <i class="fa fa-times-circle text-danger"></i> Delete
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <!-- MODAL EDIT BRAND-->
                                                                <div class="modal" id="editBrand<?= $row->id_brand?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                    <div class="modal-dialog">

                                                                        <form role="form" class="bs-example" method="post" action="<?= site_url('set_genset/update_data_brand')?>">

                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                    <h4 class="modal-title" id="myModalLabel">UPDATE DATA</h4>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    <div class="form-group">
                                                                                        <label>Brand Name</label>
                                                                                        <input name="brand" type="text" class="form-control" value="<?= $row->brand?>" autofocus="">
                                                                                    </div>

                                                                                    <input name="id_brand" type="hidden" value="<?= $row->id_brand?>"/>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="submit" class="btn btn-dark">Update Data</button>
                                                                                </div>
                                                                            </div>

                                                                        </form>

                                                                    </div>
                                                                </div>

                                                                <!-- MODAL ALERT DELETE BRAND-->
                                                                <div class="modal" id="deleteBrand" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                <h4 class="modal-title" id="myModalLabel">DELETE DATA</h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div class="alert alert-danger text-center">
                                                                                    <h4>Are You Sure Want To Delete This Data ? </h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                <a href="<?= site_url('set_genset/delete_data_brand/'.$row->id_brand)?>" class="btn btn-danger">Delete</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            <?php } } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </section>

                        </div>
                    </div>
                </section>
            </section>

            <!--======================= NAV TOGGLE ========================= -->
            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav">
            </a>

        </section>
    </section>
</section>
