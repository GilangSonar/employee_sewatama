<section>
    <section class="hbox stretch">
        <!--======================= SIDEBAR MENU ========================= -->
        <?php $this->load->view('element/_sidebar_menu')?>

        <!--======================= CONTENT HERE ========================= -->
        <section id="content">
            <section class="vbox">
                <section class="scrollable padder">

                    <!--======================= HEADER CONTENT ========================= -->
                    <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                        <li><a href="<?= site_url('home')?>"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="#"><i class="fa fa-cogs"></i> Setting Genset</a></li>
                        <li class="active">Update Genset</li>
                    </ul>
                    <div class="m-b-md">
                        <h3 class="m-b-none"><i class="fa fa-2x fa-cogs"></i> Setting Genset</h3>
                    </div>

                    <!--======================= CONTENT WRAPPER ========================= -->
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel panel-default">

                                <header class="panel-heading text-right bg-light">
                                    <ul class="nav nav-tabs pull-left">
                                        <li class="active"><a href="#tab1" data-toggle="tab"> <i class="fa fa-edit text-default"></i> Edit Data </a></li>
                                    </ul>
                                    <span class="hidden-sm">&nbsp;</span>
                                </header>

                                <div class="panel-body">

                                    <div class="tab-content">

                                        <div class="tab-pane fade active in" id="tab1">

                                            <?php if(isset($dt_genset)) { foreach ($dt_genset as $rowData) { ?>

                                                <form class="bs-example form-horizontal" method="post" action="<?= site_url('set_genset/update_data')?>">

                                                    <input type="hidden" name="id_genset" value="<?= $rowData->id_genset?>"/>

                                                    <div class="form-group">
                                                        <label class="col-lg-2 control-label">Brand</label>
                                                        <div class="col-lg-10">
                                                            <select name="id_brand" class="select2-option" style="width:260px" data-required="true">
                                                                <?php if(isset($dt_brand)){foreach ($dt_brand as $row){
                                                                    if($row->id_brand == $rowData->id_brand){
                                                                        $selected = "selected=selected";
                                                                    }else{
                                                                        $selected = "";
                                                                    }?>
                                                                    <option <?php echo $selected; ?> value="<?php echo $row->id_brand ?>">
                                                                        <?php echo $row->brand ?>
                                                                    </option>
                                                                <?php } }?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-lg-2 control-label">SN GENSET</label>
                                                        <div class="col-lg-10">
                                                            <input name="sn_genset" type="text" class="form-control" value="<?= $rowData->sn_genset ?>" required="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-lg-2 control-label">MODEL</label>
                                                        <div class="col-lg-10">
                                                            <input name="model" type="text" class="form-control" value="<?= $rowData->model ?>" required="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-lg-2 control-label">Type / Specification</label>
                                                        <div class="col-lg-10">
                                                            <textarea name="spec" class="form-control" rows="3"><?= $rowData->spec ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-lg-2 control-label">Capacity</label>
                                                        <div class="col-lg-10">
                                                            <input name="capacity" type="text" class="form-control" value="<?= $rowData->capacity ?>" required="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-lg-offset-2 col-lg-10">
                                                            <button type="submit" class="btn btn-sm btn-dark">Update Data</button>
                                                        </div>
                                                    </div>

                                                </form>

                                            <?php } } ?>

                                        </div>
                                    </div>
                                </div>

                            </section>

                        </div>
                    </div>
                </section>
            </section>

            <!--======================= NAV TOGGLE ========================= -->
            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav">
            </a>

        </section>
    </section>
</section>
</section>