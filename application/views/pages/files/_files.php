<section>
    <section class="hbox stretch">
        <!--======================= SIDEBAR MENU ========================= -->
        <?php $this->load->view('element/_sidebar_menu')?>

        <!--======================= CONTENT HERE ========================= -->
        <section id="content">
            <section class="vbox">
                <section class="scrollable padder">

                    <!--======================= HEADER CONTENT ========================= -->
                    <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                        <li><a href="<?= site_url('home')?>"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active"><a href="#"><i class="fa fa-file-text-o"></i> Employee Files</a></li>
                    </ul>
                    <div class="m-b-md">
                        <h3 class="m-b-none"><i class="fa fa-2x fa-file-text-o"></i> Employee Files</h3>
                    </div>

                    <!--======================= CONTENT WRAPPER ========================= -->
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel panel-default">

                                <header class="panel-heading text-right bg-light">
                                    <ul id="tabMenuEmp" class="nav nav-tabs pull-left">
                                        <li id="liTab1" class="active"><a href="#tab1" data-toggle="tab"> <i class="fa fa-list text-default"></i> All Record </a></li>
                                        <li><a href="#tab2" data-toggle="tab"><i class="fa fa-plus-circle text-default"></i> Add New Files</a></li>
                                    </ul>
                                    <span class="hidden-sm">&nbsp;</span>
                                </header>

                                <div class="panel-body">

                                    <div class="tab-content">

                                        <div class="tab-pane fade active in" id="tab1">

                                            <div class="table-responsive">

                                                <table class="table table-striped m-b-none" data-ride="datatables">

                                                    <thead>
                                                    <tr>
                                                        <th width="8%">No</th>
                                                        <th>SN Employee</th>
                                                        <th>Nama</th>
                                                        <th>File Name - Description</th>
                                                        <th>Update</th>
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $no=1; if(isset($dt_files)){ foreach($dt_files as $row) { ?>
                                                        <tr>
                                                            <td><?= $no++; ?></td>
                                                            <td><?= $row->sn_employee?></td>
                                                            <td><?= $row->nama?></td>
                                                            <td><?= $row->files?> - <?= $row->desc?></td>
                                                            <td><?= date("d F Y",strtotime($row->update))?></td>

                                                            <td class="text-center">

                                                                <div class="btn-group">
                                                                    <a href="<?= site_url('files/download_file/'.$row->files)?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download File">
                                                                        <i class="fa fa-download text-dark"></i>
                                                                    </a>
                                                                    &nbsp;
                                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></a>
                                                                    <ul class="dropdown-menu pull-right">
                                                                        <li>
                                                                            <a href="<?= site_url('files/edit_pages/'.$row->id_files)?>">
                                                                                <i class="fa fa-pencil-square text-info"></i> Edit
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#alertDelete<?=$row->id_files?>" data-toggle="modal">
                                                                                <i class="fa fa-times-circle text-danger"></i> Delete
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <!-- MODAL DELETE -->
                                                        <div class="modal" id="alertDelete<?= $row->id_files?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title" id="myModalLabel">DELETE DATA</h4>
                                                                    </div>
                                                                    <form action="<?= site_url('files/delete_data/'.$row->id_files)?>" method="post">
                                                                        <div class="modal-body">
                                                                            <input name="id_user" type="hidden" value="<?= $row->id_files?>"/>
                                                                            <div class="alert alert-danger text-center">
                                                                                <h4>
                                                                                    Are You Sure Want To Delete This Data ?
                                                                                </h4>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                            <button type="submit" class="btn btn-danger">Delete</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    <?php } } ?>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>

                                        <!--======================== ADD NEW FORMS ============================= -->
                                        <div class="tab-pane fade" id="tab2">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="header">
                                                        <h4>Perhatian..!</h4>
                                                        <p class="text-danger text-uc">
                                                            * Upload file dalam bentuk format .zip <br/>
                                                            * Total kapasitas file .zip maksimal 10 Mb <br/>
                                                            * File-file tersebut terdiri dari :
                                                        </p>
                                                        <ol>
                                                            <li>Scan KTP ( .JPG )</li>
                                                            <li>Scan Kartu Keluarga ( .JPG )</li>
                                                            <li>Scan Ijazah ( .JPG ) <small class="text-danger"><em>Ijazah Pendidikan Terakhir</em></small></li>
                                                            <li>Scan Sertifikat ( .JPG ) <small class="text-danger"><em>Jika Ada</em></small></li>
                                                            <li>CV / Resume ( .DOC / .DOCX )</li>
                                                        </ol>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <form method="post" action="<?= site_url('files/upload_file')?>" enctype="multipart/form-data">
                                                        <p>PILIH EMPLOYEE</p>
                                                        <div class="m-b">
                                                            <select name="id_employee" class="select2-option" style="width:260px" required="">
                                                                <option value="">:: Pilih Nama Employee :: </option>
                                                                <?php if(isset($dt_employee)){ foreach ($dt_employee as $row) { ?>
                                                                    <option value="<?= $row->id_employee?>">SN : <?= $row->sn_employee?> - <?= $row->nama?></option>
                                                                <?php } } ?>
                                                            </select>
                                                        </div>

                                                        <p class="m-t"> Description </p>
                                                        <input class="form-control" type="text" name="desc" maxlength="100"/>

                                                        <p class="m-t">UPLOAD FILES</p>
                                                        <input name="userfile" type="file" class="filestyle" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline input-s" required="">

                                                        <input type="hidden" name="update" value="<?= date('d m Y')?>"/>

                                                        <div class="line line-dashed"></div>
                                                        <button type="submit" class="btn btn-dark btn-sm">
                                                            Upload Now
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </section>

                        </div>
                    </div>
                </section>
            </section>

            <!--======================= NAV TOGGLE ========================= -->
            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav">
            </a>

        </section>
    </section>
</section>

