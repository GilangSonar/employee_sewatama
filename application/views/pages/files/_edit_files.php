<?php
$error_alert = $this->session->flashdata('error');
if(isset($error_alert)) : ?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery.gritter.add({
                title:	'ERROR UPLOAD',
                text:	'<?= $error_alert ?>' ,
                image: 	'<?php echo base_url('assets/images/notif_error.png') ?>',
                sticky: false
            });
        });
    </script>
<?php endif; ?>
<section>
    <section class="hbox stretch">
        <!--======================= SIDEBAR MENU ========================= -->
        <?php $this->load->view('element/_sidebar_menu')?>

        <!--======================= CONTENT HERE ========================= -->
        <section id="content">
            <section class="vbox">
                <section class="scrollable padder">

                    <!--======================= HEADER CONTENT ========================= -->
                    <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                        <li><a href="<?= site_url('home')?>"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active"><a href="#"><i class="fa fa-file-text-o"></i> Update Employee Files</a></li>
                    </ul>
                    <div class="m-b-md">
                        <h3 class="m-b-none"><i class="fa fa-2x fa-file-text-o"></i> Update Employee Files</h3>
                    </div>

                    <!--======================= CONTENT WRAPPER ========================= -->
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel panel-default">

                                <header class="panel-heading text-right bg-light">
                                    <ul id="tabMenuEmp" class="nav nav-tabs pull-left">
                                        <li id="liTab1" class="active"><a href="#tab1" data-toggle="tab"> <i class="fa fa-list text-default"></i> Update Data</a></li>
                                    </ul>
                                    <span class="hidden-sm">&nbsp;</span>
                                </header>

                                <div class="panel-body">

                                    <div class="tab-content">

                                        <div class="tab-pane fade active in" id="tab1">

                                            <?php if(isset($dt_files)) { foreach ($dt_files as $rowData) { ?>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="header">
                                                            <h4>Perhatian..!</h4>
                                                            <p class="text-danger text-uc">
                                                                * Upload file dalam bentuk format .zip <br/>
                                                                * Total kapasitas file .zip maksimal 10 Mb <br/>
                                                                * File-file tersebut terdiri dari :
                                                            </p>
                                                            <ol>
                                                                <li>Scan KTP ( .JPG )</li>
                                                                <li>Scan Kartu Keluarga ( .JPG )</li>
                                                                <li>Scan Ijazah ( .JPG ) <small class="text-danger"><em>Ijazah Pendidikan Terakhir</em></small></li>
                                                                <li>Scan Sertifikat ( .JPG ) <small class="text-danger"><em>Jika Ada</em></small></li>
                                                                <li>CV / Resume ( .DOC / .DOCX )</li>
                                                            </ol>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <form method="post" action="<?= site_url('files/update_upload_file')?>" enctype="multipart/form-data">
                                                            <p>PILIH EMPLOYEE</p>
                                                            <div class="m-b">
                                                                <select name="id_employee" class="select2-option" style="width:260px" required="">
                                                                    <?php if(isset($dt_employee)){foreach ($dt_employee as $row){
                                                                        if($row->id_employee == $rowData->id_employee){
                                                                            $selected = "selected=selected";
                                                                        }else{
                                                                            $selected = "";
                                                                        }?>
                                                                        <option <?= $selected; ?> value="<?= $row->id_employee ?>">
                                                                            SN : <?= $row->sn_employee ?> -  <?= $row->nama ?>
                                                                        </option>
                                                                    <?php } }?>
                                                                </select>
                                                            </div>

                                                            <p class="m-t"> Description </p>
                                                            <input class="form-control" type="text" name="desc" maxlength="100" value="<?= $rowData->desc?>"/>

                                                            <p class="m-t">UPLOAD FILES</p>
                                                            <p class="text-dark">Recent File : <?= $rowData->files ?></p>
                                                            <input name="userfile" type="file" class="filestyle" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline input-s">

                                                            <input type="hidden" name="update" value="<?= date('d m Y')?>"/>
                                                            <input type="hidden" name="id_files" value="<?= $rowData->id_files ?>"/>

                                                            <div class="line line-dashed"></div>
                                                            <button type="submit" class="btn btn-dark btn-sm">
                                                                Update
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            <?php } } ?>
                                        </div>
                                    </div>
                                </div>

                            </section>

                        </div>
                    </div>
                </section>
            </section>

            <!--======================= NAV TOGGLE ========================= -->
            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav">
            </a>

        </section>
    </section>
</section>

