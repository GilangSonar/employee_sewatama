<?php if ($this->session->flashdata('act_tab_upload') == true) : ?>
    <script>
        $(function(){
            $('#liTab1,#tab1').removeClass('active');
            $('#tabMenuEmp a[href="#tab3"]').tab('show');
        });
    </script>
<?php endif; ?>

<section>
    <section class="hbox stretch">
        <!--======================= SIDEBAR MENU ========================= -->
        <?php $this->load->view('element/_sidebar_menu')?>

        <!--======================= CONTENT HERE ========================= -->
        <section id="content">
            <section class="vbox">
                <section class="scrollable padder">

                    <!--======================= HEADER CONTENT ========================= -->
                    <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                        <li><a href="<?= site_url('home')?>"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active"><a href="#"><i class="fa fa-users"></i> Employee Data</a></li>
                    </ul>
                    <div class="m-b-md">
                        <h3 class="m-b-none"><i class="fa fa-2x fa-users"></i> Employee Data</h3>
                    </div>

                    <!--======================= CONTENT WRAPPER ========================= -->
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel panel-default">

                                <header class="panel-heading text-right bg-light">
                                    <ul id="tabMenuEmp" class="nav nav-tabs pull-left">
                                        <li id="liTab1" class="active"><a href="#tab1" data-toggle="tab"> <i class="fa fa-list text-default"></i> All Record </a></li>
                                        <li><a href="#tab2" data-toggle="tab"><i class="fa fa-plus-circle text-default"></i> Add New Employee</a></li>
                                    </ul>
                                    <span class="hidden-sm">&nbsp;</span>
                                </header>

                                <div class="panel-body">

                                    <div class="tab-content">

                                        <div class="tab-pane fade active in" id="tab1">

                                            <div class="table-responsive">

                                                <table class="table table-striped m-b-none" data-ride="datatables">

                                                    <thead>
                                                    <tr>
                                                        <th width="8%">No</th>
                                                        <th class="text-center"><i class="fa fa-picture-o"></i></th>
                                                        <th>SN Employee</th>
                                                        <th>Nama</th>
                                                        <th>Status</th>
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $no=1; if(isset($dt_employee)){ foreach($dt_employee as $row) { ?>
                                                        <tr>
                                                            <td><?= $no++; ?></td>
                                                            <td class="text-center">
                                                                <div class="thumb-md">
                                                                    <?php if(isset($row->employee_img)) { ?>
                                                                        <img class="img-responsive" src="<?php echo base_url('uploads/photos/'.$row->employee_img)?>" alt="foto karyawan">
                                                                    <?php } else { ?>
                                                                        <img class="img-responsive" src="<?php echo base_url('assets/images/avatar_default.jpg')?>" alt="foto karyawan">
                                                                    <?php } ?>
                                                                </div>
                                                            </td>
                                                            <td><?= $row->sn_employee?></td>
                                                            <td><?= $row->nama?></td>
                                                            <td class="text-uc">
                                                                <?= $row->status_employee?>
                                                                <?php if($row->status_employee == 'contract') { ?> <br/>
                                                                    <small class="text-muted text-uc">Start: <?= date("d F Y",strtotime($row->start_contract))?></small>
                                                                    <br/>
                                                                    <small class="text-muted text-uc">End: <?= date("d F Y",strtotime($row->end_contract))?></small>
                                                                <?php } ?>
                                                            </td>

                                                            <td class="text-center">

                                                                <div class="btn-group">
                                                                    <a href="<?= site_url('employee/view_pages/'.$row->id_employee)?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Employee Detail">
                                                                        <i class="fa fa-eye text-dark"></i>
                                                                    </a>
                                                                    &nbsp;
                                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></a>
                                                                    <ul class="dropdown-menu pull-right">
                                                                        <li>
                                                                            <a href="<?= site_url('employee/edit_pages/'.$row->id_employee)?>">
                                                                                <i class="fa fa-pencil-square text-info"></i> Edit
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#alertDelete<?= $row->id_employee?>" data-toggle="modal">
                                                                                <i class="fa fa-times-circle text-danger"></i> Delete
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <!-- MODAL DELETE -->
                                                        <div class="modal" id="alertDelete<?= $row->id_employee?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title" id="myModalLabel">DELETE DATA</h4>
                                                                    </div>
                                                                    <form action="<?= site_url('employee/delete_data/'.$row->id_employee)?>" method="post">
                                                                        <div class="modal-body">
                                                                            <input name="id_user" type="hidden" value="<?= $row->id_employee?>"/>
                                                                            <div class="alert alert-danger text-center">
                                                                                <h4>
                                                                                    PERHATIAN..! Data File Pegawai Yang Bersangkutan Akan Ikut Terhapus
                                                                                </h4>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                            <button type="submit" class="btn btn-danger">Delete</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    <?php } } ?>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>

                                        <!--======================== ADD NEW FORMS ============================= -->
                                        <div class="tab-pane fade" id="tab2">
                                            <div class="wizard clearfix" id="form-wizard" >
                                                <ul class="steps">
                                                    <li data-target="#step1" class="active"><span class="badge badge-info">1</span>
                                                        PROFIL KARYAWAN
                                                    </li>
                                                    <li data-target="#step2"><span class="badge">2</span>
                                                        STATUS PENDIDIKAN & SERTIFIKASI
                                                    </li>
                                                    <li data-target="#step3"><span class="badge">3</span>
                                                        KONTAK & ALAMAT
                                                    </li>
                                                    <li data-target="#step4"><span class="badge">4</span>
                                                        INFO TAMBAHAN
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="step-content">
                                                <form method="post" action="<?= site_url('employee/input_data')?>" enctype="multipart/form-data">

                                                    <div class="step-pane active" id="step1">
                                                        <?php $this->load->view('pages/employee/add_form/_profil')?>
                                                    </div>

                                                    <div class="step-pane" id="step2">
                                                        <?php $this->load->view('pages/employee/add_form/_pendidikan')?>
                                                    </div>

                                                    <div class="step-pane" id="step3">
                                                        <?php $this->load->view('pages/employee/add_form/_kontak')?>
                                                    </div>

                                                    <div class="step-pane" id="step4">
                                                        <?php $this->load->view('pages/employee/add_form/_info')?>
                                                        <div class="line line-dashed"></div>
                                                        <div class="pull-right">
                                                            <button id="btnSubmit" type="submit" class="btn btn-dark btn-sm">
                                                                Save Data
                                                            </button>
                                                        </div>
                                                    </div>

                                                    <div class="actions m-t">
                                                        <button type="button" class="btn btn-default btn-sm btn-prev" data-target="#form-wizard" data-wizard="previous" disabled="disabled">
                                                            Prev
                                                        </button>
                                                        <button type="button" class="btn btn-default btn-sm btn-next" data-target="#form-wizard" data-wizard="next" data-last="Finish">
                                                            Next
                                                        </button>
                                                    </div>

                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </section>

                        </div>
                    </div>
                </section>
            </section>

            <!--======================= NAV TOGGLE ========================= -->
            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav">
            </a>

        </section>
    </section>
</section>

