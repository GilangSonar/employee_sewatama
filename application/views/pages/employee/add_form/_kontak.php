<div class="row">
    <div class="col-md-3">
        <p><strong>KATEGORI ALAMAT</strong></p>
        <div class="line line-dashed"></div>
        <div class="list-group bg-white nav-tabs">
            <a id="kontakBtn" href="#kontak" data-toggle="tab" class="list-group-item active" onclick="kontakFunction()">
                <i class="fa fa-chevron-right icon-muted"></i> <i class="fa fa-phone-square icon-muted fa-fw"></i> KONTAK
            </a>
            <a id="alamat_ktpBtn" href="#alamat_ktp" data-toggle="tab" class="list-group-item" onclick="alamat_KtpFunction()">
                <i class="fa fa-chevron-right icon-muted"></i> <i class="fa fa-credit-card icon-muted fa-fw"></i> ALAMAT KTP
            </a>
            <a id="alamat_skrBtn" href="#alamat_skr" data-toggle="tab" class="list-group-item" onclick="alamat_SkrFunction()">
                <i class="fa fa-chevron-right icon-muted"></i> <i class="fa fa-map-marker icon-muted fa-fw"></i> ALAMAT SEKARANG
            </a>
            <a id="alamat_messBtn" href="#alamat_mess" data-toggle="tab" class="list-group-item" onclick="alamat_MessFunction()">
                <i class="fa fa-chevron-right icon-muted"></i> <i class="fa fa-home icon-muted fa-fw"></i> ALAMAT MESS
            </a>
            <a id="kontakDaruratBtn" href="#kontak_darurat" data-toggle="tab" class="list-group-item" onclick="kontakDaruratFunction()">
                <i class="fa fa-chevron-right icon-muted"></i> <i class="fa fa-phone icon-muted fa-fw"></i> KONTAK DARURAT
            </a>
        </div>
        <div class="line line-dashed"></div>
    </div>

    <div class="col-md-9">
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade active in" id="kontak">
                <p>EMAIL I</p>
                <input name="email1" type="text" class="form-control" data-trigger="change" data-type="email" placeholder="Email Address I">

                <p class="m-t">EMAIL II</p>
                <input name="email2" type="text" class="form-control" data-trigger="change" data-type="email" placeholder="Email Address II">

                <p class="m-t">NO HANDPHONE I</p>
                <input name="hp1" type="text" class="form-control" data-trigger="change" data-type="number" placeholder="No Handphone I">

                <p class="m-t">NO HANDPHONE II</p>
                <input name="hp2" type="text" class="form-control" data-trigger="change" data-type="number" placeholder="No Handphone II">

            </div>
            <div class="tab-pane fade" id="alamat_ktp">

                <p>ALAMAT (Sesuai Alamat KTP)</p>
                <textarea name="alamat_ktp" rows="3" class="form-control" data-trigger="change" data-required="true"></textarea>

                <p class="m-t">TELEPON RUMAH (Sesuai Alamat KTP)</p>
                <input name="tlp_rumah_ktp" type="text" class="form-control" data-trigger="change" data-type="number" placeholder="Telepon Rumah">

                <div class="row">
                    <div class="col-md-6">
                        <p class="m-t">Propinsi (Sesuai Alamat KTP)</p>
                        <div class="m-b">
                            <select name="id_propinsi_ktp" id="selectPropinsiKtp" class="select2-option" style="width: 260px">
                                <option value="">:: Pilih Propinsi :: </option>
                                <?php if(isset($dt_propinsi)){ foreach ($dt_propinsi as $row) { ?>
                                    <option value="<?= $row->id_propinsi?>"><?= $row->propinsi?></option>
                                <?php } } ?>
                            </select>
                        </div>

                        <div id="optKotaKtp"></div>
                    </div>
                    <div class="col-md-6">
                        <div id="optKecamatanKtp"></div>
                        <div id="optKelurahanKtp"></div>
                    </div>
                </div>

                <p class="m-t">KODE POS (Sesuai Alamat KTP)</p>
                <input name="kode_pos_ktp" type="text" class="form-control" data-trigger="change" data-type="number" placeholder="Kode Pos">

            </div>

            <div class="tab-pane fade" id="alamat_skr">
                <p>ALAMAT (Sesuai Alamat Sekarang)</p>
                <textarea name="alamat_skr" rows="3" class="form-control" data-trigger="change"></textarea>

                <p class="m-t">TELEPON RUMAH (Sesuai Alamat Sekarang)</p>
                <input name="tlp_rumah_skr" type="text" class="form-control" data-trigger="change" data-type="number" placeholder="Telepon Rumah">

                <div class="row">
                    <div class="col-md-6">
                        <p class="m-t">Propinsi (Sesuai Alamat Sekarang)</p>
                        <div class="m-b">
                            <select name="id_propinsi_skr" id="selectPropinsiSkr" class="select2-option" style="width: 260px">
                                <option value="">:: Pilih Propinsi :: </option>
                                <?php if(isset($dt_propinsi)){ foreach ($dt_propinsi as $row) { ?>
                                    <option value="<?= $row->id_propinsi?>"><?= $row->propinsi?></option>
                                <?php } } ?>
                            </select>
                        </div>
                        <div id="optKotaSkr"></div>
                    </div>
                    <div class="col-md-6">
                        <div id="optKecamatanSkr"></div>
                        <div id="optKelurahanSkr"></div>
                    </div>
                </div>

                <p class="m-t">KODE POS (Sesuai Alamat Sekarang)</p>
                <input name="kode_pos_skr" type="text" class="form-control" data-trigger="change" data-type="number" placeholder="Kode Pos">

                <p class="m-t">STATUS RUMAH SEKARANG</p>
                <input name="status_rumah_skr" type="text" class="form-control" data-trigger="change" placeholder="Status Kepemilikan Rumah">
            </div>

            <div class="tab-pane fade" id="alamat_mess">
                <p>ALAMAT (Sesuai Alamat Mess)</p>
                <textarea name="alamat_mess" rows="3" class="form-control" data-trigger="change"></textarea>

                <p class="m-t">TELEPON RUMAH (Sesuai Alamat Mess)</p>
                <input name="tlp_rumah_mess" type="text" class="form-control" data-trigger="change" data-type="number" placeholder="Telepon Rumah">

                <div class="row">
                    <div class="col-md-6">
                        <p class="m-t">Propinsi (Sesuai Alamat Mess)</p>
                        <div class="m-b">
                            <select name="id_propinsi_mess" id="selectPropinsiMess" class="select2-option" style="width: 260px">
                                <option value="">:: Pilih Propinsi :: </option>
                                <?php if(isset($dt_propinsi)){ foreach ($dt_propinsi as $row) { ?>
                                    <option value="<?= $row->id_propinsi?>"><?= $row->propinsi?></option>
                                <?php } } ?>
                            </select>
                        </div>
                        <div id="optKotaMess"></div>
                    </div>
                    <div class="col-md-6">
                        <div id="optKecamatanMess"></div>
                        <div id="optKelurahanMess"></div>
                    </div>
                </div>

                <p class="m-t">KODE POS (Sesuai Alamat Mess)</p>
                <input name="kode_pos_mess" type="text" class="form-control" data-trigger="change" data-type="number" placeholder="Kode Pos">
            </div>

            <div class="tab-pane fade" id="kontak_darurat">
                <p>NAMA</p>
                <input name="nama_ecd" type="text" class="form-control" data-trigger="change" placeholder="Nama">

                <p class="m-t">HUBUNGAN KERABAT</p>
                <input name="hubungan_ecd" type="text" class="form-control" data-trigger="change" placeholder="Hubungan Kerabat">

                <p class="m-t">ALAMAT</p>
                <textarea name="alamat_ecd" rows="3" class="form-control" data-trigger="change"></textarea>

                <p class="m-t">TELP RUMAH</p>
                <input name="tlp_rumah_ecd" type="text" class="form-control" data-trigger="change" data-type="number" placeholder="Telepon Rumah">

                <p class="m-t">TELP KANTOR</p>
                <input name="tlp_kantor_ecd" type="text" class="form-control" data-trigger="change" data-type="number" placeholder="Telepon Kantor I">

                <p class="m-t">NO HANDPHONE</p>
                <input name="hp_ecd" type="text" class="form-control" data-trigger="change" data-type="number" placeholder="No Handphone">

                <p class="m-t">EMAIL</p>
                <input name="email_ecd" type="text" class="form-control" data-trigger="change" data-type="email" placeholder="Email Address">
            </div>
            
        </div>
    </div>
</div>

<script>
    function kontakFunction(){
        $('#kontakBtn').addClass('active');
        $('#alamat_ktpBtn,#alamat_messBtn,#alamat_skrBtn,#kontakDaruratBtn').removeClass('active');
    }
    function alamat_KtpFunction(){
        $('#alamat_ktpBtn').addClass('active');
        $('#alamat_messBtn,#alamat_skrBtn,#kontakBtn,#kontakDaruratBtn').removeClass('active');
    }
    function alamat_SkrFunction(){
        $('#alamat_skrBtn').addClass('active');
        $('#alamat_messBtn,#alamat_ktpBtn,#kontakBtn,#kontakDaruratBtn').removeClass('active');
    }
    function alamat_MessFunction(){
        $('#alamat_messBtn').addClass('active');
        $('#alamat_skrBtn,#alamat_ktpBtn,#kontakBtn,#kontakDaruratBtn').removeClass('active');
    }
    function kontakDaruratFunction(){
        $('#kontakDaruratBtn').addClass('active');
        $('#alamat_ktpBtn,#alamat_messBtn,#alamat_skrBtn,#kontakBtn').removeClass('active');
    }

    $(document).ready(function(){
        $("#selectPropinsiKtp").change(function(){
            var kat_alamat = "ktp";
            var id_propinsi_ktp = $("#selectPropinsiKtp option:selected").val();
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('employee/ajaxLoadKota'); ?>",
                data: "id_propinsi=" + id_propinsi_ktp
                    + "&kat_alamat=" + kat_alamat,
                success: function(dataKota){
                    $('#optKotaKtp').html(dataKota);
                    $('.select2-option').select2();
                }
            });
        });
        $("#selectPropinsiSkr").change(function(){
            var kat_alamat = "skr";
            var id_propinsi_skr = $("#selectPropinsiSkr option:selected").val();
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('employee/ajaxLoadKota'); ?>",
                data: "id_propinsi=" + id_propinsi_skr
                    + "&kat_alamat=" + kat_alamat,
                success: function(dataKota){
                    $('#optKotaSkr').html(dataKota);
                    $('.select2-option').select2();
                }
            });
        });
        $("#selectPropinsiMess").change(function(){
            var kat_alamat = "mess";
            var id_propinsi_mess = $("#selectPropinsiMess option:selected").val();
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('employee/ajaxLoadKota'); ?>",
                data: "id_propinsi=" + id_propinsi_mess
                    + "&kat_alamat=" + kat_alamat,
                success: function(dataKota){
                    $('#optKotaMess').html(dataKota);
                    $('.select2-option').select2();
                }
            });
        });
    });

</script>