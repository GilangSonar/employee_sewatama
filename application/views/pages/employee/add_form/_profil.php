<div class="row">
    <div class="col-md-3">
        <p><strong>KATEGORI PROFIL</strong></p>
        <div class="line line-dashed"></div>
        <div class="list-group bg-white nav-tabs">
            <a id="dataDiriBtn" href="#data_diri" data-toggle="tab" class="list-group-item active" onclick="dataDiriFunction()">
                <i class="fa fa-chevron-right icon-muted"></i> <i class="fa fa-user icon-muted fa-fw"></i> Data Diri
            </a>
            <a id="identitasBtn" href="#identitas" data-toggle="tab" class="list-group-item" onclick="identitasFunction()">
                <i class="fa fa-chevron-right icon-muted"></i> <i class="fa fa-credit-card icon-muted fa-fw"></i> No Identitas
            </a>
        </div>
        <div class="line line-dashed"></div>
    </div>

    <div class="col-md-9">
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade active in" id="data_diri">
                <div class="row">
                    <div class="col-md-6">
                        <input type="hidden" name="update" value="<?php echo date('d M Y')?>">
                        <input type="hidden" name="user_update" value="<?= $this->session->userdata('ID')?>">

                        <p>EMPLOYEE ID</p>
                        <input name="id_employee" type="text" class="form-control" data-trigger="change" readonly value="<?= $id_employee;?>">

                        <p class="m-t">SERIAL NUMBER</p>
                        <input name="sn_employee" type="text" class="form-control" data-trigger="change" data-required="true" data-type="number" placeholder="Empployee Serial Number">

                        <p class="m-t">NAMA</p>
                        <input name="nama" type="text" class="form-control" data-trigger="change" data-required="true" placeholder="Employee Name">

                        <p class="m-t">TEMPAT LAHIR</p>
                        <div class="m-b">
                            <select name="tempat_lahir" class="select2-option" style="width:260px" data-required="true">
                                <option value="">:: Pilih Kota Kelahiran :: </option>
                                <?php if(isset($dt_kota)){ foreach ($dt_kota as $row) { ?>
                                    <option value="<?= $row->id_kota?>"><?= $row->kota?></option>
                                <?php } } ?>
                            </select>
                        </div>

                        <p class="m-t">TANGGAL LAHIR</p>
                        <input name="tgl_lahir" type="text" class="combodate form-control" data-format="YYYY-MM-DD" data-template="YYYY MMM D" data-required="true">

                        <p class="m-t">JENIS KELAMIN</p>
                        <div class="m-b">
                            <select name="jns_kelamin" class="select2-option" style="width:260px" data-required="true">
                                <option value="">:: Pilih Jenis Kelamin :: </option>
                                <option value="laki">LAKI-LAKI</option>
                                <option value="perempuan">PEREMPUAN</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <p>GOLONGAN DARAH</p>
                        <div class="m-b">
                            <select name="gol_darah" class="select2-option" style="width:260px">
                                <option value="">:: Pilih Golongan Darah :: </option>
                                <option value="a">A</option>
                                <option value="b">B</option>
                                <option value="ab">AB</option>
                                <option value="o">O</option>
                            </select>
                        </div>

                        <p class="m-t" style="margin-top: 19px">STATUS PERKAWINAN</p>
                        <div class="m-b">
                            <select id="statusKawin" name="status_kawin" class="select2-option" style="width:260px" data-required="true">
                                <option value="">:: Pilih Status :: </option>
                                <option value="lajang">Lajang</option>
                                <option value="nikah">Menikah</option>
                                <option value="cerai">Cerai</option>
                            </select>
                        </div>

                        <div id="tanggal_kawin" style="display: none; margin-top: 19px">
                            <p class="m-t">SEJAK TANGGAL</p>
                            <input id="input_tgl_kawin" name="tgl_kawin" class="input-sm input-s datepicker-input form-control" data-required="false" size="16" type="text">
                        </div>

                        <p class="m-t" style="margin-top: 19px">AGAMA</p>
                        <div class="m-b">
                            <select name="agama" class="select2-option" style="width:260px" data-required="true">
                                <option value="">:: Pilih Agama :: </option>
                                <option value="islam">ISLAM</option>
                                <option value="protestan">KRISTEN PROTESTAN</option>
                                <option value="katholik">KRISTEN KATHOLIK</option>
                                <option value="hindu">HINDU</option>
                                <option value="budha">BUDHA</option>
                            </select>
                        </div>

                        <p class="m-t" style="margin-top: 19px">KEWARGANEGARAAN</p>
                        <div class="m-b">
                            <select name="kewarganegaraan" class="select2-option" style="width:260px" data-required="true">
                                <option value="">:: Pilih Kewarganegaraan :: </option>
                                <option value="wni">WNI</option>
                                <option value="wna">WNA</option>
                            </select>
                        </div>

                        <p class="m-t">UPLOAD FOTO KARYAWAN</p>
                        <input id="userfile" name="userfile" type="file" class="filestyle" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline input-s">
                    </div>

                </div>
            </div>

            <div class="tab-pane fade" id="identitas">
                <div class="row">
                    <div class="col-md-6">
                        <p>NO IDENTTAS (KTP)</p>
                        <input name="no_ktp" type="text" class="form-control" data-trigger="change" data-type="number" placeholder="Nomor KTP">

                        <p class="m-t">NO IDENTTAS (SIM)</p>
                        <input name="no_sim" type="text" class="form-control" data-trigger="change" data-type="number" placeholder="Nomor SIM">

                        <p class="m-t">NO IDENTTAS (PASSPORT)</p>
                        <input name="no_passport" type="text" class="form-control" data-trigger="change" data-type="number" placeholder="Nomor PASSPORT">

                        <p class="m-t">NO NPWP</p>
                        <input name="no_npwp" type="text" class="form-control" data-trigger="change" data-type="number" placeholder="Nomor PASSPORT">

                        <p class="m-t">NO JAMSOSTEK</p>
                        <input name="no_jamsostek" type="text" class="form-control" data-trigger="change" data-type="number" placeholder="Nomor PASSPORT">
                    </div>

                    <div class="col-md-6">
                        <p>MASA BERLAKU</p>
                        <input name="masa_berlaku" class="input-sm input-s datepicker-input form-control" size="16" type="text" data-date-format="dd-mm-yyyy" >
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>


<script>
    function dataDiriFunction(){
        $('#dataDiriBtn').addClass('active');
        $('#identitasBtn').removeClass('active');
    }
    function identitasFunction(){
        $('#identitasBtn').addClass('active');
        $('#dataDiriBtn').removeClass('active');
    }

    $(document).ready(function(){
        $("#statusKawin").change(function(){
            var status_kawin = $("#statusKawin option:selected").val();
            if(status_kawin != "lajang"){
                $('#tanggal_kawin').fadeIn();
                $('#input_tgl_kawin').attr("data-required",'true');
            }else{
                $('#tanggal_kawin').fadeOut();
                $('#input_tgl_kawin').attr("data-required",'false');
            }
        });
    });
</script>