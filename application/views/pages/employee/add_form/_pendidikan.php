<p class="m-t">PENDIDIKAN TERAKHIR</p>
<input name="pendidikan" type="text" class="form-control" data-trigger="change" data-required="true" placeholder="Pendidikan Terakhir">

<p class="m-t">SEKOLAH / AKADEMI</p>
<input name="akademi" type="text" class="form-control" data-trigger="change" data-required="true" placeholder="Nama Sekolah / Akademi">

<p class="m-t">JURUSAN / KEAHLIAN</p>
<input name="jurusan" type="text" class="form-control" data-trigger="change" data-required="true" placeholder="Jurusan / Keahlian Yang Diambil">

<p class="m-t">TAHUN KELULUSAN</p>
<input name="thn_lulus" type="text" class="combodate form-control" data-format="YYYY" data-template="YYYY" data-required="true">

<p class="m-t">SERTIFIKAT YANG DIMILIKI</p>
<input name="sertifikat" type="text" class="form-control" data-trigger="change" placeholder="Sertifikat Yang Dimiliki">