<div class="row">
    <div class="col-md-3">
        <p><strong>KATEGORI INFO</strong></p>
        <div class="line line-dashed"></div>
        <div class="list-group bg-white nav-tabs">
            <a id="statusBtn" href="#status" data-toggle="tab" class="list-group-item active" onclick="statusFunction()">
                <i class="fa fa-chevron-right icon-muted"></i> <i class="fa fa-file-text-o icon-muted fa-fw"></i> Status Kepegawaian
            </a>
            <a id="penugasanBtn" href="#penugasan" data-toggle="tab" class="list-group-item" onclick="penugasanFunction()">
                <i class="fa fa-chevron-right icon-muted"></i> <i class="fa fa-info icon-muted fa-fw"></i> Status Penugasan
            </a>
            <a id="pendidikan_anakBtn" href="#pendidikan_anak" data-toggle="tab" class="list-group-item" onclick="pendidikanAnakFunction()">
                <i class="fa fa-chevron-right icon-muted"></i> <i class="fa fa-user-md icon-muted fa-fw"></i> Status Pendidikan Anak
            </a>
        </div>
        <div class="line line-dashed"></div>
    </div>

    <div class="col-md-9">
        <div id="myTabContent" class="tab-content">

            <div class="tab-pane fade active in" id="status">
                <div class="row">

                    <div class="col-md-6">
                        <p>PILIH STATUS: </p>
                        <div class="m-b">
                            <select name="status_employee" id="statusEmployee" class="select2-option" style="width:260px">
                                <option value="">:: Pilih Jawaban :: </option>
                                <option value="permanent">Permanent</option>
                                <option value="contract">Contract</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">

                        <div id="tanggal_contract" style="display: none;">
                            <p>START CONTRACT</p>
                            <input name="start_contract" class="datepicker-input form-control" size="16" type="text" data-date-format="dd-mm-yyyy" >

                            <p class="m-t">END CONTRACT</p>
                            <input name="end_contract" class="datepicker-input form-control" size="16" type="text" data-date-format="dd-mm-yyyy" >
                        </div>

                    </div>

                </div>
            </div>

            <div class="tab-pane fade" id="penugasan">
                <div class="row">
                    <div class="col-md-6">
                        <p>JUMLAH ANAK</p>
                        <input name="jml_anak" type="text" class="form-control" data-trigger="change" placeholder="Jumlah Anak">

                        <p class="m-t">KEPENDUDUKAN ISTRI</p>
                        <select name="kependudukan_istri" class="select2-option" style="width:260px">
                            <option value="">:: Pilih Jawaban :: </option>
                            <option value="dalam">Dalam Daerah</option>
                            <option value="luar">Luar Daerah</option>
                        </select>
                    </div>

                    <div class="col-md-6">
                        <p>APAKAH MEMBAWA KELUARGA DI SITE ? </p>
                        <div class="m-b">
                            <select name="keluarga_onsite" id="bawaKeluarga" class="select2-option" style="width:260px">
                                <option value="">:: Pilih Jawaban :: </option>
                                <option value="ya">Ya</option>
                                <option value="tidak">Tidak</option>
                            </select>
                        </div>

                        <div id="tanggal_bawa" style="display: none;">
                            <p>SEJAK TANGGAL</p>
                            <input name="tgl_keluarga_onsite" class="datepicker-input form-control" size="16" type="text" data-date-format="dd-mm-yyyy" >
                        </div>
                    </div>

                </div>
            </div>

            <div class="tab-pane fade" id="pendidikan_anak">
                <p class="m-t">APAKAH ANAK SEKOLAH ? </p>
                <div class="m-b">
                    <select name="status_sekolah_anak" id="anakSekolah" class="select2-option" style="width:260px">
                        <option value="">:: Pilih Jawaban :: </option>
                        <option value="ya">Ya</option>
                        <option value="tidak">Tidak</option>
                    </select>
                </div>

                <div id="anak_keberapa" style="display: none; margin-top: 19px">
                    <p class="m-t">ANAK KE BERAPA ? </p>
                    <input name="anak_keberapa" type="text" class="form-control" data-trigger="change" data-type="number">
                </div>

                <div id="alamat_sekolah" style="display: none">
                    <p class="m-t">ALAMAT SEKOLAH</p>
                    <textarea name="alamat_sekolah_anak" rows="3" class="form-control" data-trigger="change"></textarea>
                </div>

            </div>
            
        </div>
    </div>
</div>

<script>
    function statusFunction(){
        $('#statusBtn').addClass('active');
        $('#penugasanBtn,#pendidikan_anakBtn').removeClass('active');
    }
    function penugasanFunction(){
        $('#penugasanBtn').addClass('active');
        $('#pendidikan_anakBtn,#statusBtn').removeClass('active');
    }
    function pendidikanAnakFunction(){
        $('#pendidikan_anakBtn').addClass('active');
        $('#penugasanBtn,#statusBtn').removeClass('active');
    }

    $(document).ready(function(){
        $("#statusEmployee").change(function(){
            var status_employee = $("#statusEmployee option:selected").val();
            if(status_employee == "contract"){
                $('#tanggal_contract').fadeIn();
            }else{
                $('#tanggal_contract').fadeOut();
            }
        });
        $("#bawaKeluarga").change(function(){
            var bawa_keluarga = $("#bawaKeluarga option:selected").val();
            if(bawa_keluarga == "ya"){
                $('#tanggal_bawa').fadeIn();
            }else{
                $('#tanggal_bawa').fadeOut();
            }
        });
        $("#anakSekolah").change(function(){
            var anak_sekolah = $("#anakSekolah option:selected").val();
            if(anak_sekolah == "ya"){
                $('#anak_keberapa').fadeIn();
                $('#alamat_sekolah').fadeIn();
            }else{
                $('#anak_keberapa').fadeOut();
                $('#alamat_sekolah').fadeOut();
            }
        });
    });
</script>