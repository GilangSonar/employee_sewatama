<div class="row">
    <div class="col-md-3">
        <p><strong>KATEGORI PROFIL</strong></p>
        <div class="line line-dashed"></div>
        <div class="list-group bg-white nav-tabs">
            <a id="dataDiriBtn" href="#data_diri" data-toggle="tab" class="list-group-item active" onclick="dataDiriFunction()">
                <i class="fa fa-chevron-right icon-muted"></i> <i class="fa fa-user icon-muted fa-fw"></i> Data Diri
            </a>
            <a id="identitasBtn" href="#identitas" data-toggle="tab" class="list-group-item" onclick="identitasFunction()">
                <i class="fa fa-chevron-right icon-muted"></i> <i class="fa fa-credit-card icon-muted fa-fw"></i> No Identitas
            </a>
        </div>
        <div class="line line-dashed"></div>
    </div>

    <?php if(isset($dt_employee)) { foreach ($dt_employee as $rowData) { ?>

    <div class="col-md-9">
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade active in" id="data_diri">
                <div class="row">
                    <div class="col-md-6">
                        <input type="hidden" name="update" value="<?php echo date('d M Y')?>">
                        <input type="hidden" name="user_update" value="<?= $this->session->userdata('ID')?>">

                        <p>EMPLOYEE ID</p>
                        <input name="id_employee" type="text" class="form-control" data-trigger="change" readonly value="<?= $rowData->id_employee?>">

                        <p class="m-t">SERIAL NUMBER</p>
                        <input name="sn_employee" type="text" class="form-control" data-trigger="change" data-required="true" data-type="number" value="<?= $rowData->sn_employee?>">

                        <p class="m-t">NAMA</p>
                        <input name="nama" type="text" class="form-control" data-trigger="change" data-required="true" value="<?= $rowData->nama?>">

                        <p class="m-t">TEMPAT LAHIR</p>
                        <div class="m-b">
                            <select name="tempat_lahir" class="select2-option" style="width:260px" data-required="true">
                                <?php if(isset($dt_kota)){foreach ($dt_kota as $row){
                                    if($row->id_kota == $rowData->tempat_lahir){
                                        $selected = "selected=selected";
                                    }else{
                                        $selected = "";
                                    }?>
                                    <option <?php echo $selected; ?> value="<?php echo $row->id_kota ?>">
                                        <?php echo $row->kota ?>
                                    </option>
                                <?php } }?>
                            </select>
                        </div>

                        <p class="m-t">TANGGAL LAHIR</p>
                        <input name="tgl_lahir" type="text" class="combodate form-control" data-format="YYYY-MM-DD" data-template="YYYY MMM D" data-required="true" value="<?= $rowData->tgl_lahir?>">

                        <p class="m-t">JENIS KELAMIN</p>
                        <div class="m-b">
                            <select name="jns_kelamin" class="select2-option" style="width:260px" data-required="true">
                                <?php if($rowData->jns_kelamin == "laki"){
                                    $selected = "selected=selected";?>
                                    <option <?php echo $selected; ?> value="laki">LAKI-LAKI</option>
                                    <option value="perempuan">PEREMPUAN</option>
                                <?php } elseif($rowData->jns_kelamin == "perempuan"){
                                    $selected = "selected=selected";?>
                                    <option value="laki">LAKI-LAKI</option>
                                    <option <?php echo $selected; ?> value="perempuan">PEREMPUAN</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <p>GOLONGAN DARAH</p>
                        <div class="m-b">
                            <select name="gol_darah" class="select2-option" style="width:260px">
                                <?php if($rowData->gol_darah == "a"){
                                    $selected = "selected=selected";?>
                                    <option <?php echo $selected; ?> value="a">A</option>
                                    <option value="b">B</option>
                                    <option value="ab">AB</option>
                                    <option value="o">O</option>
                                <?php } elseif($rowData->gol_darah == "b"){
                                    $selected = "selected=selected";?>
                                    <option value="a">A</option>
                                    <option <?php echo $selected; ?> value="b">B</option>
                                    <option value="ab">AB</option>
                                    <option value="o">O</option>
                                <?php } elseif($rowData->gol_darah == "ab"){
                                    $selected = "selected=selected";?>
                                    <option value="a">A</option>
                                    <option value="b">B</option>
                                    <option <?php echo $selected; ?> value="ab">AB</option>
                                    <option value="o">O</option>
                                <?php } elseif($rowData->gol_darah == "o"){
                                    $selected = "selected=selected";?>
                                    <option value="a">A</option>
                                    <option value="b">B</option>
                                    <option value="ab">AB</option>
                                    <option <?php echo $selected; ?> value="o">O</option>
                                <?php } ?>

                            </select>
                        </div>

                        <p class="m-t" style="margin-top: 19px">STATUS PERKAWINAN</p>
                        <div class="m-b">
                            <select id="statusKawin" name="status_kawin" class="select2-option" style="width:260px" data-required="true">
                                <?php if($rowData->status_kawin == "lajang"){
                                    $selected = "selected=selected";?>
                                    <option <?php echo $selected; ?> value="lajang">Lajang</option>
                                    <option value="nikah">Menikah</option>
                                    <option value="cerai">Cerai</option>
                                <?php } elseif($rowData->status_kawin == "nikah"){
                                    $selected = "selected=selected";?>
                                    <option value="lajang">Lajang</option>
                                    <option <?php echo $selected; ?> value="nikah">Menikah</option>
                                    <option value="cerai">Cerai</option>
                                <?php } elseif($rowData->status_kawin == "cerai"){
                                    $selected = "selected=selected";?>
                                    <option value="lajang">Lajang</option>
                                    <option value="nikah">Menikah</option>
                                    <option <?php echo $selected; ?> value="cerai">Cerai</option>
                                <?php } ?>
                            </select>
                        </div>

                        <div id="tanggal_kawin" style="display: none; margin-top: 19px">
                            <p class="m-t">SEJAK TANGGAL</p>
                            <input id="input_tgl_kawin" name="tgl_kawin" class="input-sm input-s datepicker-input form-control" size="16" type="text" data-date-format="yyyy-mm-dd" value="<?= $rowData->tgl_kawin?>">
                        </div>

                        <p class="m-t" style="margin-top: 19px">AGAMA</p>
                        <div class="m-b">
                            <select name="agama" class="select2-option" style="width:260px" data-required="true">

                                <?php if($rowData->agama == "islam"){
                                    $selected = "selected=selected";?>
                                    <option <?php echo $selected; ?> value="islam">ISLAM</option>
                                    <option value="protestan">KRISTEN PROTESTAN</option>
                                    <option value="katholik">KRISTEN KATHOLIK</option>
                                    <option value="hindu">HINDU</option>
                                    <option value="budha">BUDHA</option>
                                <?php } elseif($rowData->agama == "protestan"){
                                    $selected = "selected=selected";?>
                                    <option value="islam">ISLAM</option>
                                    <option <?php echo $selected; ?> value="protestan">KRISTEN PROTESTAN</option>
                                    <option value="katholik">KRISTEN KATHOLIK</option>
                                    <option value="hindu">HINDU</option>
                                    <option value="budha">BUDHA</option>
                                <?php } elseif($rowData->agama == "katholik"){
                                    $selected = "selected=selected";?>
                                    <option value="islam">ISLAM</option>
                                    <option value="protestan">KRISTEN PROTESTAN</option>
                                    <option <?php echo $selected; ?> value="katholik">KRISTEN KATHOLIK</option>
                                    <option value="hindu">HINDU</option>
                                    <option value="budha">BUDHA</option>
                                <?php } elseif($rowData->agama == "hindu"){
                                    $selected = "selected=selected";?>
                                    <option value="islam">ISLAM</option>
                                    <option value="protestan">KRISTEN PROTESTAN</option>
                                    <option value="katholik">KRISTEN KATHOLIK</option>
                                    <option <?php echo $selected; ?> value="hindu">HINDU</option>
                                    <option value="budha">BUDHA</option>
                                <?php } elseif($rowData->agama == "budha"){
                                    $selected = "selected=selected";?>
                                    <option value="islam">ISLAM</option>
                                    <option value="protestan">KRISTEN PROTESTAN</option>
                                    <option value="katholik">KRISTEN KATHOLIK</option>
                                    <option value="hindu">HINDU</option>
                                    <option <?php echo $selected; ?> value="budha">BUDHA</option>
                                <?php } ?>

                            </select>
                        </div>

                        <p class="m-t" style="margin-top: 19px">KEWARGANEGARAAN</p>
                        <div class="m-b">
                            <select name="kewarganegaraan" class="select2-option" style="width:260px" data-required="true">

                                <?php if($rowData->kewarganegaraan == "wni"){
                                    $selected = "selected=selected";?>
                                    <option <?php echo $selected; ?> value="wni">WNI</option>
                                    <option value="wna">WNA</option>
                                <?php } elseif($rowData->kewarganegaraan == "wna"){
                                    $selected = "selected=selected";?>
                                    <option value="wni">WNI</option>
                                    <option <?php echo $selected; ?> value="wna">WNA</option>
                                <?php } ?>

                            </select>
                        </div>

                        <p class="m-t">UPDATE FOTO KARYAWAN <br/> <small class="text-danger">Abaikan jika tidak ada perubahan foto</small></p>
                        <div class="thumbnail col-lg-3"  style="margin-right: 15px">
                            <?php if(isset($rowData->employee_img)) { ?>
                                <img class="img-responsive" src="<?php echo base_url('uploads/photos/'.$rowData->employee_img)?>" alt="foto karyawan">
                            <?php } else { ?>
                                <img class="img-responsive" src="<?php echo base_url('assets/images/avatar_default.jpg')?>" alt="foto karyawan">
                            <?php } ?>
                        </div>
                        <input id="userfile" name="userfile" type="file" class="filestyle" data-icon="true" data-classButton="btn btn-default" data-classInput="form-control inline input-s">
                    </div>

                </div>
            </div>

            <div class="tab-pane fade" id="identitas">
                <div class="row">
                    <div class="col-md-6">
                        <p>NO IDENTTAS (KTP)</p>
                        <input name="no_ktp" type="text" class="form-control" data-trigger="change" data-type="number" value="<?= $rowData->no_ktp?>">

                        <p class="m-t">NO IDENTTAS (SIM)</p>
                        <input name="no_sim" type="text" class="form-control" data-trigger="change" data-type="number" value="<?= $rowData->no_sim?>">

                        <p class="m-t">NO IDENTTAS (PASSPORT)</p>
                        <input name="no_passport" type="text" class="form-control" data-trigger="change" data-type="number" value="<?= $rowData->no_passport?>">

                        <p class="m-t">NO NPWP</p>
                        <input name="no_npwp" type="text" class="form-control" data-trigger="change" data-type="number" value="<?= $rowData->no_npwp?>">

                        <p class="m-t">NO JAMSOSTEK</p>
                        <input name="no_jamsostek" type="text" class="form-control" data-trigger="change" data-type="number" value="<?= $rowData->no_jamsostek?>">
                    </div>

                    <div class="col-md-6">
                        <p>MASA BERLAKU</p>
                        <input name="masa_berlaku" class="input-sm input-s datepicker-input form-control" size="16" type="text" data-date-format="yyyy-mm-dd" value="<?= $rowData->masa_berlaku?>">
                    </div>

                </div>
            </div>

        </div>
    </div>

    <?php } } ?>
</div>


<script>
    function dataDiriFunction(){
        $('#dataDiriBtn').addClass('active');
        $('#identitasBtn').removeClass('active');
    }
    function identitasFunction(){
        $('#identitasBtn').addClass('active');
        $('#dataDiriBtn').removeClass('active');
    }

    $(document).ready(function(){
        var status_kawin = $("#statusKawin option:selected").val();
        if(status_kawin != "lajang"){
            $('#tanggal_kawin').show();
            $('#input_tgl_kawin').attr("data-required",'true');
        }

        $("#statusKawin").change(function(){
            var status_kawin = $("#statusKawin option:selected").val();
            if(status_kawin != "lajang"){
                $('#tanggal_kawin').fadeIn();
                $('#input_tgl_kawin').attr("data-required",'true');
            }else{
                $('#tanggal_kawin').fadeOut();
                $('#input_tgl_kawin').attr("data-required",'false');
            }
        });

    });
</script>