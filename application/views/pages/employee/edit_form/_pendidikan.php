<?php if(isset($dt_employee)) { foreach ($dt_employee as $rowData) { ?>

    <p class="m-t">PENDIDIKAN TERAKHIR</p>
    <input name="pendidikan" type="text" class="form-control" data-trigger="change" data-required="true" value="<?= $rowData->pendidikan?>">

    <p class="m-t">SEKOLAH / AKADEMI</p>
    <input name="akademi" type="text" class="form-control" data-trigger="change" data-required="true" value="<?= $rowData->akademi?>">

    <p class="m-t">JURUSAN / KEAHLIAN</p>
    <input name="jurusan" type="text" class="form-control" data-trigger="change" data-required="true" value="<?= $rowData->jurusan?>">

    <p class="m-t">TAHUN KELULUSAN</p>
    <input name="thn_lulus" type="text" class="combodate form-control" data-format="YYYY" data-template="YYYY" data-required="true" value="<?= $rowData->thn_lulus?>">

    <p class="m-t">SERTIFIKAT YANG DIMILIKI</p>
    <input name="sertifikat" type="text" class="form-control" data-trigger="change" value="<?= $rowData->sertifikat?>">

<?php } } ?>