<div class="row">
<div class="col-md-3">
    <p><strong>KATEGORI ALAMAT</strong></p>
    <div class="line line-dashed"></div>
    <div class="list-group bg-white nav-tabs">
        <a id="kontakBtn" href="#kontak" data-toggle="tab" class="list-group-item active" onclick="kontakFunction()">
            <i class="fa fa-chevron-right icon-muted"></i> <i class="fa fa-phone-square icon-muted fa-fw"></i> KONTAK
        </a>
        <a id="alamat_ktpBtn" href="#alamat_ktp" data-toggle="tab" class="list-group-item" onclick="alamat_KtpFunction()">
            <i class="fa fa-chevron-right icon-muted"></i> <i class="fa fa-credit-card icon-muted fa-fw"></i> ALAMAT KTP
        </a>
        <a id="alamat_skrBtn" href="#alamat_skr" data-toggle="tab" class="list-group-item" onclick="alamat_SkrFunction()">
            <i class="fa fa-chevron-right icon-muted"></i> <i class="fa fa-map-marker icon-muted fa-fw"></i> ALAMAT SEKARANG
        </a>
        <a id="alamat_messBtn" href="#alamat_mess" data-toggle="tab" class="list-group-item" onclick="alamat_MessFunction()">
            <i class="fa fa-chevron-right icon-muted"></i> <i class="fa fa-home icon-muted fa-fw"></i> ALAMAT MESS
        </a>
        <a id="kontakDaruratBtn" href="#kontak_darurat" data-toggle="tab" class="list-group-item" onclick="kontakDaruratFunction()">
            <i class="fa fa-chevron-right icon-muted"></i> <i class="fa fa-phone icon-muted fa-fw"></i> KONTAK DARURAT
        </a>
    </div>
    <div class="line line-dashed"></div>
</div>

<?php if(isset($dt_employee)) { foreach ($dt_employee as $rowData) { ?>

    <div class="col-md-9">
    <div id="myTabContent" class="tab-content">
    <div class="tab-pane fade active in" id="kontak">
        <p>EMAIL I</p>
        <input name="email1" type="text" class="form-control" data-trigger="change" data-type="email" value="<?= $rowData->email1; ?>">

        <p class="m-t">EMAIL II</p>
        <input name="email2" type="text" class="form-control" data-trigger="change" data-type="email" value="<?= $rowData->email2; ?>">

        <p class="m-t">NO HANDPHONE I</p>
        <input name="hp1" type="text" class="form-control" data-trigger="change" data-type="number" value="<?= $rowData->hp1; ?>">

        <p class="m-t">NO HANDPHONE II</p>
        <input name="hp2" type="text" class="form-control" data-trigger="change" data-type="number" value="<?= $rowData->hp2; ?>">

    </div>
    <div class="tab-pane fade" id="alamat_ktp">

        <p>ALAMAT (Sesuai Alamat KTP)</p>
        <textarea name="alamat_ktp" rows="3" class="form-control" data-trigger="change" data-required="true"><?= $rowData->alamat_ktp; ?></textarea>

        <p class="m-t">TELEPON RUMAH (Sesuai Alamat KTP)</p>
        <input name="tlp_rumah_ktp" type="text" class="form-control" data-trigger="change" data-type="number" value="<?= $rowData->tlp_rumah_ktp; ?>">

        <p class="m-t"><button id="btnEditKtp" class="btn btn-xs btn-info"> EDIT LOKASI KTP </button></p>

        <div id="lokasiKtp">
            <?php if(isset($dt_lokasi)){foreach ($dt_lokasi as $row){ ?>
                <div class="row">
                    <div class="col-md-6">
                        <p>PROPINSI (Sesuai Alamat KTP)</p>
                        <input type="text" class="form-control" value="<?= $row->propinsi_ktp; ?>" readonly>
                        <input name="id_propinsi_ktp" type="hidden" value="<?= $row->id_propinsi_ktp; ?>">

                        <p class="m-t">KOTA (Sesuai Alamat KTP)</p>
                        <input type="text" class="form-control" value="<?= $row->kota_ktp; ?>" readonly>
                        <input name="id_kota_ktp" type="hidden" value="<?= $row->id_kota_ktp; ?>">

                    </div>
                    <div class="col-md-6">
                        <p>KECAMATAN (Sesuai Alamat KTP)</p>
                        <input type="text" class="form-control" value="<?= $row->kecamatan_ktp; ?>" readonly>
                        <input name="id_kecamatan_ktp" type="hidden" value="<?= $row->id_kecamatan_ktp; ?>">

                        <p class="m-t">KELURAHAN (Sesuai Alamat KTP)</p>
                        <input type="text" class="form-control" value="<?= $row->kelurahan_ktp; ?>" readonly>
                        <input name="id_kelurahan_ktp" type="hidden" value="<?= $row->id_kelurahan_ktp; ?>">
                    </div>
                </div>
            <?php } }?>
        </div>

        <div id="editLokasiKtp" class="row hide">
            <div class="col-md-6">
                <p>PROPINSI (Sesuai Alamat KTP)</p>
                <div class="m-b">
                    <select name="id_propinsi_ktp" id="selectPropinsiKtp" class="select2-option" style="width: 260px">
                        <?php if(isset($dt_propinsi)){foreach ($dt_propinsi as $row){
                            if($row->id_propinsi == $rowData->id_propinsi_ktp){
                                $selected = "selected=selected";
                            }else{
                                $selected = "";
                            }?>
                            <option <?php echo $selected; ?> value="<?php echo $row->id_propinsi ?>">
                                <?php echo $row->propinsi ?>
                            </option>
                        <?php } }?>
                    </select>
                </div>
                <div id="optKotaKtp"></div>

            </div>
            <div class="col-md-6">
                <div id="optKecamatanKtp"></div>
                <div id="optKelurahanKtp"></div>
            </div>
        </div>

        <p class="m-t">KODE POS (Sesuai Alamat KTP)</p>
        <input name="kode_pos_ktp" type="text" class="form-control" data-trigger="change" data-type="number" value="<?= $rowData->kode_pos_ktp?> ">

    </div>

    <div class="tab-pane fade" id="alamat_skr">
        <p>ALAMAT (Sesuai Alamat Sekarang)</p>
        <textarea name="alamat_skr" rows="3" class="form-control" data-trigger="change"><?= $rowData->alamat_skr; ?></textarea>

        <p class="m-t">TELEPON RUMAH (Sesuai Alamat Sekarang)</p>
        <input name="tlp_rumah_skr" type="text" class="form-control" data-trigger="change" data-type="number" value="<?= $rowData->tlp_rumah_skr; ?>">

        <p class="m-t"><button id="btnEditSkr" class="btn btn-xs btn-info"> EDIT LOKASI SEKARANG </button></p>

        <div id="lokasiSkr">
            <?php if(isset($dt_lokasi)){foreach ($dt_lokasi as $row){ ?>
                <div class="row">
                    <div class="col-md-6">
                        <p>PROPINSI (Sesuai Alamat Sekarang)</p>
                        <input type="text" class="form-control" value="<?= $row->propinsi_skr; ?>" readonly>
                        <input name="id_propinsi_skr" type="hidden" value="<?= $row->id_propinsi_skr; ?>">

                        <p class="m-t">KOTA (Sesuai Alamat Sekarang)</p>
                        <input type="text" class="form-control" value="<?= $row->kota_skr; ?>" readonly>
                        <input name="id_kota_skr" type="hidden" value="<?= $row->id_kota_skr; ?>">

                    </div>
                    <div class="col-md-6">
                        <p>KECAMATAN (Sesuai Alamat Sekarang)</p>
                        <input type="text" class="form-control" value="<?= $row->kecamatan_skr; ?>" readonly>
                        <input name="id_kecamatan_skr" type="hidden" value="<?= $row->id_kecamatan_skr; ?>">

                        <p class="m-t">KELURAHAN (Sesuai Alamat Sekarang)</p>
                        <input type="text" class="form-control" value="<?= $row->kelurahan_skr; ?>" readonly>
                        <input name="id_kelurahan_skr" type="hidden" value="<?= $row->id_kelurahan_skr; ?>">
                    </div>
                </div>
            <?php } }?>
        </div>

        <div id="editLokasiSkr" class="row hide">
            <div class="col-md-6">
                <p class="m-t">PROPINSI (Sesuai Alamat Sekarang)</p>
                <div class="m-b">
                    <select name="id_propinsi_skr" id="selectPropinsiSkr" class="select2-option" style="width: 260px">
                        <?php if(isset($dt_propinsi)){foreach ($dt_propinsi as $row){
                            if($row->id_propinsi == $rowData->id_propinsi_skr){
                                $selected = "selected=selected";
                            }else{
                                $selected = "";
                            }?>
                            <option <?php echo $selected; ?> value="<?php echo $row->id_propinsi ?>">
                                <?php echo $row->propinsi ?>
                            </option>
                        <?php } }?>
                    </select>
                </div>
                <div id="optKotaSkr"></div>
            </div>
            <div class="col-md-6">
                <div id="optKecamatanSkr"></div>
                <div id="optKelurahanSkr"></div>
            </div>
        </div>

        <p class="m-t">KODE POS (Sesuai Alamat Sekarang)</p>
        <input name="kode_pos_skr" type="text" class="form-control" data-trigger="change" data-type="number" value="<?= $rowData->kode_pos_skr; ?>">

        <p class="m-t">STATUS RUMAH SEKARANG</p>
        <input name="status_rumah_skr" type="text" class="form-control" data-trigger="change" value="<?= $rowData->status_rumah_skr; ?>">
    </div>

    <div class="tab-pane fade" id="alamat_mess">
        <p>ALAMAT (Sesuai Alamat Mess)</p>
        <textarea name="alamat_mess" rows="3" class="form-control" data-trigger="change"><?= $rowData->alamat_mess; ?></textarea>

        <p class="m-t">TELEPON RUMAH (Sesuai Alamat Mess)</p>
        <input name="tlp_rumah_mess" type="text" class="form-control" data-trigger="change" data-type="number" value="<?= $rowData->tlp_rumah_mess; ?>">

        <p class="m-t"><button id="btnEditMess" class="btn btn-xs btn-info"> EDIT LOKASI MESS </button></p>

        <div id="lokasiMess">
            <?php if(isset($dt_lokasi)){foreach ($dt_lokasi as $row){ ?>
                <div class="row">
                    <div class="col-md-6">
                        <p>PROPINSI (Sesuai Alamat Mess)</p>
                        <input type="text" class="form-control" value="<?= $row->propinsi_mess; ?>" readonly>
                        <input name="id_propinsi_mess" type="hidden" value="<?= $row->id_propinsi_mess; ?>">

                        <p class="m-t">KOTA (Sesuai Alamat Mess)</p>
                        <input type="text" class="form-control" value="<?= $row->kota_mess; ?>" readonly>
                        <input name="id_kota_mess" type="hidden" value="<?= $row->id_kota_mess; ?>">

                    </div>
                    <div class="col-md-6">
                        <p>KECAMATAN (Sesuai Alamat Mess)</p>
                        <input type="text" class="form-control" value="<?= $row->kecamatan_mess; ?>" readonly>
                        <input name="id_kecamatan_mess" type="hidden" value="<?= $row->id_kecamatan_mess; ?>">

                        <p class="m-t">KELURAHAN (Sesuai Alamat Mess)</p>
                        <input type="text" class="form-control" value="<?= $row->kelurahan_mess; ?>" readonly>
                        <input name="id_kelurahan_mess" type="hidden" value="<?= $row->id_kelurahan_mess; ?>">
                    </div>
                </div>
            <?php } }?>
        </div>

        <div id="editLokasiMess" class="row hide">
            <div class="col-md-6">
                <p class="m-t">PROPINSI (Sesuai Alamat Mess)</p>
                <div class="m-b">
                    <select name="id_propinsi_mess" id="selectPropinsiMess" class="select2-option" style="width: 260px">
                        <?php if(isset($dt_propinsi)){foreach ($dt_propinsi as $row){
                            if($row->id_propinsi == $rowData->id_propinsi_mess){
                                $selected = "selected=selected";
                            }else{
                                $selected = "";
                            }?>
                            <option <?php echo $selected; ?> value="<?php echo $row->id_propinsi ?>">
                                <?php echo $row->propinsi ?>
                            </option>
                        <?php } }?>
                    </select>
                </div>
                <div id="optKotaMess"></div>
            </div>
            <div class="col-md-6">
                <div id="optKecamatanMess"></div>
                <div id="optKelurahanMess"></div>
            </div>
        </div>

        <p class="m-t">KODE POS (Sesuai Alamat Mess)</p>
        <input name="kode_pos_mess" type="text" class="form-control" data-trigger="change" data-type="number" value="<?= $rowData->kode_pos_mess; ?>">
    </div>

    <div class="tab-pane fade" id="kontak_darurat">
        <p>NAMA</p>
        <input name="nama_ecd" type="text" class="form-control" data-trigger="change" value="<?= $rowData->nama_ecd; ?>">

        <p class="m-t">HUBUNGAN KERABAT</p>
        <input name="hubungan_ecd" type="text" class="form-control" data-trigger="change" value="<?= $rowData->hubungan_ecd; ?>">

        <p class="m-t">ALAMAT</p>
        <textarea name="alamat_ecd" rows="3" class="form-control" data-trigger="change"><?= $rowData->alamat_ecd; ?></textarea>

        <p class="m-t">TELP RUMAH</p>
        <input name="tlp_rumah_ecd" type="text" class="form-control" data-trigger="change" data-type="number" value="<?= $rowData->tlp_rumah_ecd; ?>">

        <p class="m-t">TELP KANTOR</p>
        <input name="tlp_kantor_ecd" type="text" class="form-control" data-trigger="change" data-type="number" value="<?= $rowData->tlp_kantor_ecd; ?>">

        <p class="m-t">NO HANDPHONE</p>
        <input name="hp_ecd" type="text" class="form-control" data-trigger="change" data-type="number" value="<?= $rowData->hp_ecd; ?>">

        <p class="m-t">EMAIL</p>
        <input name="email_ecd" type="text" class="form-control" data-trigger="change" data-type="email" value="<?= $rowData->email_ecd; ?>">
    </div>

    </div>
    </div>

<?php } } ?>

</div>

<script>
    $('#btnEditKtp').toggle(
        function(){
            $('#lokasiKtp').addClass('hide');
            $('#editLokasiKtp').removeClass('hide');
            $(this).text("CANCEL").removeClass('btn-info').addClass('btn-danger');
        },
        function(){
            $('#lokasiKtp').removeClass('hide');
            $('#editLokasiKtp').addClass('hide');
            $(this).text("EDIT LOKASI KTP").removeClass('btn-danger').addClass('btn-info');
        }
    );
    $('#btnEditSkr').toggle(
        function(){
            $('#lokasiSkr').addClass('hide');
            $('#editLokasiSkr').removeClass('hide');
            $(this).text("CANCEL").removeClass('btn-info').addClass('btn-danger');
        },
        function(){
            $('#lokasiSkr').removeClass('hide');
            $('#editLokasiSkr').addClass('hide');
            $(this).text("EDIT LOKASI SEKARANG").removeClass('btn-danger').addClass('btn-info');
        }
    );
    $('#btnEditMess').toggle(
        function(){
            $('#lokasiMess').addClass('hide');
            $('#editLokasiMess').removeClass('hide');
            $(this).text("CANCEL").removeClass('btn-info').addClass('btn-danger');
        },
        function(){
            $('#lokasiMess').removeClass('hide');
            $('#editLokasiMess').addClass('hide');
            $(this).text("EDIT LOKASI MESS").removeClass('btn-danger').addClass('btn-info');
        }
    );
    function kontakFunction(){
        $('#kontakBtn').addClass('active');
        $('#alamat_ktpBtn,#alamat_messBtn,#alamat_skrBtn,#kontakDaruratBtn').removeClass('active');
    }
    function alamat_KtpFunction(){
        $('#alamat_ktpBtn').addClass('active');
        $('#alamat_messBtn,#alamat_skrBtn,#kontakBtn,#kontakDaruratBtn').removeClass('active');
    }
    function alamat_SkrFunction(){
        $('#alamat_skrBtn').addClass('active');
        $('#alamat_messBtn,#alamat_ktpBtn,#kontakBtn,#kontakDaruratBtn').removeClass('active');
    }
    function alamat_MessFunction(){
        $('#alamat_messBtn').addClass('active');
        $('#alamat_skrBtn,#alamat_ktpBtn,#kontakBtn,#kontakDaruratBtn').removeClass('active');
    }
    function kontakDaruratFunction(){
        $('#kontakDaruratBtn').addClass('active');
        $('#alamat_ktpBtn,#alamat_messBtn,#alamat_skrBtn,#kontakBtn').removeClass('active');
    }

    $(document).ready(function(){
        $("#selectPropinsiKtp").change(function(){
            var kat_alamat = "ktp";
            var id_propinsi_ktp = $("#selectPropinsiKtp option:selected").val();
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('employee/ajaxLoadKota'); ?>",
                data: "id_propinsi=" + id_propinsi_ktp
                    + "&kat_alamat=" + kat_alamat,
                success: function(dataKota){
                    $('#optKotaKtp').html(dataKota);
                    $('.select2-option').select2();
                }
            });
        });
        $("#selectPropinsiSkr").change(function(){
            var kat_alamat = "skr";
            var id_propinsi_skr = $("#selectPropinsiSkr option:selected").val();
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('employee/ajaxLoadKota'); ?>",
                data: "id_propinsi=" + id_propinsi_skr
                    + "&kat_alamat=" + kat_alamat,
                success: function(dataKota){
                    $('#optKotaSkr').html(dataKota);
                    $('.select2-option').select2();
                }
            });
        });
        $("#selectPropinsiMess").change(function(){
            var kat_alamat = "mess";
            var id_propinsi_mess = $("#selectPropinsiMess option:selected").val();
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('employee/ajaxLoadKota'); ?>",
                data: "id_propinsi=" + id_propinsi_mess
                    + "&kat_alamat=" + kat_alamat,
                success: function(dataKota){
                    $('#optKotaMess').html(dataKota);
                    $('.select2-option').select2();
                }
            });
        });
    });

</script>