<?php
if( isset($kategori)) :
    if ($kategori == "ktp" ) { ?>

        <p class="m-t">KABUPATEN/KOTA (Sesuai Alamat KTP)</p>
        <div class="m-b">
            <select name="id_kota_ktp" id="selectKotaKtp" class="select2-option" style="width: 260px">
                <option value="">:: Pilih Kabupaten / Kota :: </option>
                <?php if(isset($ajax_kota)){ foreach ($ajax_kota as $row) { ?>
                    <option value="<?= $row->id_kota?>"><?= $row->kota?></option>
                <?php } } ?>
            </select>
        </div>

    <?php } elseif ($kategori == "skr") { ?>

        <p class="m-t">KABUPATEN/KOTA (Sesuai Alamat Sekarang)</p>
        <div class="m-b">
            <select name="id_kota_skr" id="selectKotaSkr" class="select2-option" style="width: 260px">
                <option value="">:: Pilih Kabupaten / Kota :: </option>
                <?php if(isset($ajax_kota)){ foreach ($ajax_kota as $row) { ?>
                    <option value="<?= $row->id_kota?>"><?= $row->kota?></option>
                <?php } } ?>
            </select>
        </div>

    <?php } elseif ($kategori == "mess") { ?>

        <p class="m-t">KABUPATEN/KOTA (Sesuai Alamat Mess)</p>
        <div class="m-b">
            <select name="id_kota_mess" id="selectKotaMess" class="select2-option" style="width: 260px">
                <option value="">:: Pilih Kabupaten / Kota :: </option>
                <?php if(isset($ajax_kota)){ foreach ($ajax_kota as $row) { ?>
                    <option value="<?= $row->id_kota?>"><?= $row->kota?></option>
                <?php } } ?>
            </select>
        </div>

    <?php }
endif;
?>

<script>
    $(document).ready(function(){
        $("#selectKotaKtp").change(function(){
            var kat_alamat = "ktp";
            var id_kota_ktp = $("#selectKotaKtp option:selected").val();
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('employee/ajaxLoadKecamatan'); ?>",
                data: "id_kota=" + id_kota_ktp
                    + "&kat_alamat=" + kat_alamat,
                success: function(dataKec){
                    $('#optKecamatanKtp').html(dataKec);
                    $('.select2-option').select2();
                }
            });
        });
        $("#selectKotaSkr").change(function(){
            var kat_alamat = "skr";
            var id_kota_skr = $("#selectKotaSkr option:selected").val();
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('employee/ajaxLoadKecamatan'); ?>",
                data: "id_kota=" + id_kota_skr
                    + "&kat_alamat=" + kat_alamat,
                success: function(dataKec){
                    $('#optKecamatanSkr').html(dataKec);
                    $('.select2-option').select2();
                }
            });
        });
        $("#selectKotaMess").change(function(){
            var kat_alamat = "mess";
            var id_kota_mess = $("#selectKotaMess option:selected").val();
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('employee/ajaxLoadKecamatan'); ?>",
                data: "id_kota=" + id_kota_mess
                    + "&kat_alamat=" + kat_alamat,
                success: function(dataKec){
                    $('#optKecamatanMess').html(dataKec);
                    $('.select2-option').select2();
                }
            });
        });
    });
</script>