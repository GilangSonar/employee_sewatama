<?php
if( isset($kategori)) :
    if ($kategori == "ktp" ) { ?>

        <p class="m-t">KECAMATAN (Sesuai Alamat KTP)</p>
        <div class="m-b">
            <select name="id_kecamatan_ktp" id="selectKecamatanKtp" class="select2-option" style="width: 260px">
                <option value="">:: Pilih Kecamatan :: </option>
                <?php if(isset($ajax_kecamatan)){ foreach ($ajax_kecamatan as $row) { ?>
                    <option value="<?= $row->id_kecamatan?>"><?= $row->kecamatan?></option>
                <?php } } ?>
            </select>
        </div>

    <?php } elseif ($kategori == "skr") { ?>

        <p class="m-t">KECAMATAN (Sesuai Alamat Sekarang)</p>
        <div class="m-b">
            <select name="id_kecamatan_skr" id="selectKecamatanSkr" class="select2-option" style="width: 260px">
                <option value="">:: Pilih Kecamatan :: </option>
                <?php if(isset($ajax_kecamatan)){ foreach ($ajax_kecamatan as $row) { ?>
                    <option value="<?= $row->id_kecamatan?>"><?= $row->kecamatan?></option>
                <?php } } ?>
            </select>
        </div>

    <?php } elseif ($kategori == "mess") { ?>

        <p class="m-t">KECAMATAN (Sesuai Alamat Mess)</p>
        <div class="m-b">
            <select name="id_kecamatan_mess" id="selectKecamatanMess" class="select2-option" style="width: 260px">
                <option value="">:: Pilih Kecamatan :: </option>
                <?php if(isset($ajax_kecamatan)){ foreach ($ajax_kecamatan as $row) { ?>
                    <option value="<?= $row->id_kecamatan?>"><?= $row->kecamatan?></option>
                <?php } } ?>
            </select>
        </div>

    <?php }
endif;
?>

<script>
    $(document).ready(function(){
        $("#selectKecamatanKtp").change(function(){
            var kat_alamat = "ktp";
            var id_kecamatan_ktp = $("#selectKecamatanKtp option:selected").val();
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('employee/ajaxLoadKelurahan'); ?>",
                data: "id_kecamatan=" + id_kecamatan_ktp
                    + "&kat_alamat=" + kat_alamat,
                success: function(dataKel){
                    $('#optKelurahanKtp').html(dataKel);
                    $('.select2-option').select2();
                }
            });
        });
        $("#selectKecamatanSkr").change(function(){
            var kat_alamat = "skr";
            var id_kecamatan_skr = $("#selectKecamatanSkr option:selected").val();
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('employee/ajaxLoadKelurahan'); ?>",
                data: "id_kecamatan=" + id_kecamatan_skr
                    + "&kat_alamat=" + kat_alamat,
                success: function(dataKel){
                    $('#optKelurahanSkr').html(dataKel);
                    $('.select2-option').select2();
                }
            });
        });
        $("#selectKecamatanMess").change(function(){
            var kat_alamat = "mess";
            var id_kecamatan_mess = $("#selectKecamatanMess option:selected").val();
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('employee/ajaxLoadKelurahan'); ?>",
                data: "id_kecamatan=" + id_kecamatan_mess
                    + "&kat_alamat=" + kat_alamat,
                success: function(dataKel){
                    $('#optKelurahanMess').html(dataKel);
                    $('.select2-option').select2();
                }
            });
        });
    });
</script>