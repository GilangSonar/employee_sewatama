<section>
    <section class="hbox stretch">
        <!--======================= SIDEBAR MENU ========================= -->
        <?php $this->load->view('element/_sidebar_menu')?>

        <!--======================= CONTENT HERE ========================= -->
        <?php if(isset($dt_employee)) { foreach ($dt_employee as $rowData) { ?>
            <section id="content">
                <section class="vbox">
                    <section class="scrollable">
                        <section class="hbox stretch">

                            <aside class="aside-xl bg-light lter b-r">
                                <section class="vbox">
                                    <section class="scrollable">
                                        <div class="wrapper">
                                            <div class="clearfix m-b">
                                                <a href="#" class="pull-left thumb-md m-r">
                                                    <?php if(isset($rowData->employee_img)) { ?>
                                                        <img class="img-responsive" src="<?php echo base_url('uploads/photos/'.$rowData->employee_img)?>" alt="foto karyawan">
                                                    <?php } else { ?>
                                                        <img class="img-circle" src="<?php echo base_url('assets/images/photos/avatar_default.jpg')?>" alt="foto karyawan">
                                                    <?php } ?>
                                                </a>
                                                <div class="clear">
                                                    <div class="h3 m-t-xs m-b-xs"><?= $rowData->nama?></div>
                                                    <small class="text-muted text-uc"> SN : <?= $rowData->sn_employee?></small>
                                                    <br/>
                                                    <small class="text-muted text-uc">Status : <?= $rowData->status_employee?></small>
                                                    <br/>
                                                    <?php if($rowData->status_employee == 'contract'){ ?>
                                                        <small class="text-muted">Start : <?= date("d F Y",strtotime($rowData->start_contract))?></small>
                                                        <br/>
                                                        <small class="text-muted">End : <?= date("d F Y",strtotime($rowData->end_contract))?></small>
                                                    <?php } ?>

                                                </div>
                                            </div>
                                            <div class="panel wrapper panel-success">
                                                <p>
                                                    <small class="text-uc text-md text-muted">
                                                        <i class="fa fa-tasks"></i> Project Record
                                                    </small>
                                                </p>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <a href="#">
                                                            <span class="m-b-xs h4 block">10</span> <small class="text-muted">Current Project</small>
                                                        </a>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <a href="#">
                                                            <span class="m-b-xs h4 block">25</span> <small class="text-muted">Recent Project</small>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div>
                                                <small class="text-uc text-md text-muted"><i class="fa fa-mobile-phone"></i> HP </small>
                                                <p><?=$rowData->hp1?> <?php if($rowData->hp2!=null){ echo " / " .$rowData->hp2; } ?></p>
                                                <div class="line line-dashed"></div>

                                                <small class="text-uc text-md text-muted"><i class="fa fa-envelope"></i> Email </small>
                                                <p><?=$rowData->email1?> <?php if($rowData->email2!=null){ echo " / " .$rowData->email2; } ?></p>
                                                <div class="line line-dashed"></div>

                                                <?php if(isset($dt_lokasi)) { foreach ($dt_lokasi as $row) { ?>

                                                    <small class="text-uc text-md text-muted"><i class="fa fa-location-arrow"></i> Alamat ktp</small>
                                                    <p>
                                                        <?= $rowData->alamat_ktp?>, 
                                                        Kec: <?= $row->kecamatan_ktp?>, 
                                                        Kel: <?= $row->kelurahan_ktp?>, 
                                                        <?= $row->kota_ktp?>,
                                                        <?= $row->propinsi_ktp?>. 
                                                        <?= $rowData->kode_pos_ktp?>
                                                    </p>
                                                    <div class="line line-dashed"></div>

                                                    <small class="text-uc text-md text-muted"><i class="fa fa-home"></i> Alamat Sekarang</small>
                                                    <p>
                                                        <?= $rowData->alamat_skr?>,
                                                        Kec: <?= $row->kecamatan_skr?>,
                                                        Kel: <?= $row->kelurahan_skr?>,
                                                        <?= $row->kota_skr?>,
                                                        <?= $row->propinsi_skr?>.
                                                        <?= $rowData->kode_pos_skr?>
                                                    </p>
                                                    <p>
                                                        Status Rumah : <?= $rowData->status_rumah_skr?>
                                                    </p>
                                                    <div class="line line-dashed"></div>

                                                    <small class="text-uc text-md text-muted"><i class="fa fa-building-o"></i> Alamat Mess</small>
                                                    <p>
                                                        <?= $rowData->alamat_mess?>,
                                                        Kec: <?= $row->kecamatan_mess?>,
                                                        Kel: <?= $row->kelurahan_mess?>,
                                                        <?= $row->kota_mess?>,
                                                        <?= $row->propinsi_mess?>.
                                                        <?= $rowData->kode_pos_mess?>
                                                    </p>
                                                    <div class="line line-dashed"></div>
                                                <?php } } ?>

                                            </div>
                                        </div>
                                    </section>
                                </section>
                            </aside>

                            <aside class="bg-white">
                                <section class="vbox">

                                    <header class="header bg-light bg-gradient">
                                        <ul class="nav nav-tabs nav-white">
                                            <li class="active"><a href="#profil" data-toggle="tab">Profil</a></li>
                                            <li class=""><a href="#pendidikan" data-toggle="tab">Status pendidikan</a></li>
                                            <li class=""><a href="#darurat" data-toggle="tab">Kontak Darurat</a></li>
                                            <li class=""><a href="#info" data-toggle="tab">Info Tambahan</a></li>
                                        </ul>
                                    </header>

                                    <section class="scrollable">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="profil">
                                                <div class="wrapper">
                                                    <small class="text-uc text-muted">SN Employee</small>
                                                    <p><?=$rowData->sn_employee?></p>
                                                    <div class="line"></div>
                                                    <small class="text-uc text-muted">nama</small>
                                                    <p><?=$rowData->nama?></p>
                                                    <div class="line"></div>
                                                    <small class="text-uc text-muted">tempat / tanggal lahir</small>
                                                    <p><?=$rowData->kota?> / <?= date("d F Y",strtotime($rowData->tgl_lahir))?></p>
                                                    <div class="line"></div>
                                                    <small class="text-uc text-muted">Jenis Kelamin</small>
                                                    <p><?php if($rowData->jns_kelamin == 'laki') { echo "Laki-Laki" ;}else{ echo "Perempuan" ;}?></p>
                                                    <div class="line"></div>

                                                </div>
                                            </div>

                                            <div class="tab-pane" id="pendidikan">
                                                <div class="text-center wrapper">
                                                    <h2>STATUS PENDIDIKAN</h2>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="darurat">
                                                <div class="text-center wrapper">
                                                    <h2>KONTAK DARURAT (Emergency Contact Detail)</h2>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="info">
                                                <div class="text-center wrapper">
                                                    <h2>INFO TAMBAHAN</h2>
                                                </div>
                                            </div>

                                        </div>
                                    </section>
                                </section>
                            </aside>
                        </section>
                    </section>
                </section>

                <!--======================= NAV TOGGLE ========================= -->
                <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav">
                </a>
            </section>
        <?php } } ?>

    </section>
</section>