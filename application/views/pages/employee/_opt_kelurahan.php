<?php
if( isset($kategori)) :
if ($kategori == "ktp" ) { ?>

    <p class="m-t">KELURAHAN / DESA (Sesuai Alamat KTP )</p>
    <div class="m-b">
        <select name="id_kelurahan_ktp" id="selectKelurahanKtp" class="select2-option" style="width: 260px">
            <option value="">:: Pilih Kelurahan / Desa :: </option>
            <?php if(isset($ajax_kelurahan)){ foreach ($ajax_kelurahan as $row) { ?>
                <option value="<?= $row->id_kelurahan?>"><?= $row->kelurahan?></option>
            <?php } } ?>
        </select>
    </div>

<?php } elseif ($kategori == "skr") { ?>

    <p class="m-t">KELURAHAN / DESA (Sesuai Alamat Sekarang )</p>
    <div class="m-b">
        <select name="id_kelurahan_skr" id="selectKelurahanSkr" class="select2-option" style="width: 260px">
            <option value="">:: Pilih Kelurahan / Desa :: </option>
            <?php if(isset($ajax_kelurahan)){ foreach ($ajax_kelurahan as $row) { ?>
                <option value="<?= $row->id_kelurahan?>"><?= $row->kelurahan?></option>
            <?php } } ?>
        </select>
    </div>

<?php } elseif ($kategori == "mess") { ?>

    <p class="m-t">KELURAHAN / DESA (Sesuai Alamat Mess )</p>
    <div class="m-b">
        <select name="id_kelurahan_mess" id="selectKelurahanMess" class="select2-option" style="width: 260px">
            <option value="">:: Pilih Kelurahan / Desa :: </option>
            <?php if(isset($ajax_kelurahan)){ foreach ($ajax_kelurahan as $row) { ?>
                <option value="<?= $row->id_kelurahan?>"><?= $row->kelurahan?></option>
            <?php } } ?>
        </select>
    </div>

<?php }
endif;
?>