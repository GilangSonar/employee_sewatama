<section>
    <section class="hbox stretch">
        <!--======================= SIDEBAR MENU ========================= -->
        <?php $this->load->view('element/_sidebar_menu')?>

        <!--======================= CONTENT HERE ========================= -->
        <section id="content">
            <section class="vbox">
                <section class="scrollable padder">

                    <!--======================= HEADER CONTENT ========================= -->
                    <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                        <li><a href="<?= site_url('home')?>"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active"><a href="#"><i class="fa fa-users"></i> Employee Data</a></li>
                    </ul>
                    <div class="m-b-md">
                        <h3 class="m-b-none"><i class="fa fa-2x fa-users"></i> Update Employee Data</h3>
                    </div>

                    <!--======================= CONTENT WRAPPER ========================= -->
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel panel-default">

                                <header class="panel-heading text-right bg-light">
                                    <ul class="nav nav-tabs pull-left">
                                        <li class="active"><a href="#tab1" data-toggle="tab"> <i class="fa fa-list text-default"></i> Update Employee Data </a></li>
                                    </ul>
                                    <span class="hidden-sm">&nbsp;</span>
                                </header>

                                <div class="panel-body">

                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="tab1">

                                            <div class="wizard clearfix" id="form-wizard" >
                                                <ul class="steps">
                                                    <li data-target="#step1" class="active"><span class="badge badge-info">1</span>
                                                        PROFIL KARYAWAN
                                                    </li>
                                                    <li data-target="#step2"><span class="badge">2</span>
                                                        STATUS PENDIDIKAN & SERTIFIKASI
                                                    </li>
                                                    <li data-target="#step3"><span class="badge">3</span>
                                                        KONTAK & ALAMAT
                                                    </li>
                                                    <li data-target="#step4"><span class="badge">4</span>
                                                        INFO TAMBAHAN
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="step-content">

                                                <form method="post" action="<?= site_url('employee/update_data')?>" enctype="multipart/form-data">

                                                    <div class="step-pane active" id="step1">
                                                        <?php $this->load->view('pages/employee/edit_form/_profil')?>
                                                    </div>

                                                    <div class="step-pane" id="step2">
                                                        <?php $this->load->view('pages/employee/edit_form/_pendidikan')?>
                                                    </div>

                                                    <div class="step-pane" id="step3">
                                                        <?php $this->load->view('pages/employee/edit_form/_kontak')?>
                                                    </div>

                                                    <div class="step-pane" id="step4">
                                                        <?php $this->load->view('pages/employee/edit_form/_info')?>
                                                        <div class="line line-dashed"></div>
                                                        <div class="pull-right">
                                                            <button id="btnSubmit" type="submit" class="btn btn-dark btn-sm">
                                                                Update Data
                                                            </button>
                                                        </div>
                                                    </div>

                                                    <div class="actions m-t">
                                                        <button type="button" class="btn btn-default btn-sm btn-prev" data-target="#form-wizard" data-wizard="previous" disabled="disabled">
                                                            Prev
                                                        </button>
                                                        <button type="button" class="btn btn-default btn-sm btn-next" data-target="#form-wizard" data-wizard="next" data-last="Finish">
                                                            Next
                                                        </button>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </section>

                        </div>
                    </div>
                </section>
            </section>

            <!--======================= NAV TOGGLE ========================= -->
            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav">
            </a>

        </section>
    </section>
</section>
