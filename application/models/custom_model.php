<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Custom_model extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function getAllData($table){
        return $this->db->get($table)->result();
    }
    function getAllDataSorting($table,$order_id,$order){
        $this->db->from($table);
        $this->db->order_by($order_id,$order);
        return $this->db->get()->result();
    }
    function getLimitData($table,$limit = 0,$order_key,$order_type){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->limit($limit);
        $this->db->order_by($order_key,$order_type);
        $q = $this->db->get();
        return $q->result();
    }
    function getSelectedData($table,$id){
        return $this->db->get_where($table, $id);
    }
    function updateData($table,$data,$field_key){
        $this->db->update($table,$data,$field_key);
    }
    function deleteData($table,$data){
        $this->db->delete($table,$data);
    }
    function insertData($table,$data){
        $this->db->insert($table,$data);
    }
    function manualQuery($q){
        return $this->db->query($q)->result();
    }


    // ============== CREATE AUTOMATIC CODE ================
    function kodeEmployee(){
        $q = $this->db->query("select MAX(RIGHT(id_employee,4)) as code_max from t_employee");
        $code = "";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = sprintf("%04s", $tmp);
            }
        }else{
            $code = "0001";
        }
        return "EMP-".$code;
    }

    function kodeProject(){
        $q = $this->db->query("select MAX(RIGHT(id_project,3)) as code_max from t_project");
        $code = "";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = sprintf("%03s", $tmp);
            }
        }else{
            $code = "001";
        }
        return "PJ-".date("m")."-".date("Y")."-".$code;
    }


// ============== QUERY PROJECT DATA ================
    function getProjectData(){
        $this->db->select('*');
        $this->db->from('t_project AS a');
        $this->db->join('t_customer AS b','a.id_customer=b.id_customer','left');
        $this->db->join('t_kota AS c','a.lokasi=c.id_kota','left');
        $this->db->order_by('a.update','ASC');
        $query = $this->db->get();
        return $query->result();
    }

    function getProjectDataByID($id){
        $this->db->select('*');
        $this->db->from('t_project AS a');
        $this->db->join('t_customer AS b','a.id_customer=b.id_customer','left');
        $this->db->join('t_project_genset AS c','a.id_project=c.id_project','left');
        $this->db->join('view_genset AS d','c.id_genset=d.id_genset','left');
        $this->db->join('t_kota AS e','a.lokasi=e.id_kota','left');
        $this->db->where('a.id_project',$id);
        $this->db->group_by('a.id_project');
        $query = $this->db->get();
        return $query->result();
    }
    function getProjectEmployeeByID($id){
        $this->db->select('*');
        $this->db->from('t_project_employee AS a');
        $this->db->join('t_employee AS b','a.id_employee=b.id_employee','left');
        $this->db->where('a.id_project',$id);
        $query = $this->db->get();
        return $query->result();
    }
    function countProjectEmployeeByID($id){
        $this->db->select('*');
        $this->db->from('t_project_employee');
        $this->db->where($id);
        $query = $this->db->count_all();
        return $query;
    }
    function getProjectGensetByID($id){
        $this->db->select('*');
        $this->db->from('t_project_genset AS a');
        $this->db->join('view_genset AS b','a.id_genset=b.id_genset','left');
        $this->db->where('a.id_project',$id);
        $query = $this->db->get();
        return $query->result();
    }

// ============== QUERY AJAX LOKASI ================
    function getAjaxKota($idProp){
        $this->db->select('*');
        $this->db->from('t_kota');
        $this->db->where('id_propinsi',$idProp);
        $query = $this->db->get();
        return $query->result();
    }

    function getAjaxKecamatan($idKota){
        $this->db->select('*');
        $this->db->from('t_kecamatan');
        $this->db->where('id_kota',$idKota);
        $query = $this->db->get();
        return $query->result();
    }

    function getAjaxKelurahan($idKec){
        $this->db->select('*');
        $this->db->from('t_kelurahan');
        $this->db->where('id_kecamatan',$idKec);
        $query = $this->db->get();
        return $query->result();
    }

// ============== QUERY LOKASI EMPLOYEE ================
    function getDetailEmployee($id){
        $this->db->select('*');
        $this->db->from('t_employee AS a');
        $this->db->join('t_kota AS b','a.tempat_lahir=b.id_kota','left');
        $this->db->where($id);
        $query = $this->db->get();
        return $query->result();
    }
    function getLokasiEmployee($id){
        $this->db->select('*');
        $this->db->from('view_lokasi_employee');
        $this->db->where($id);
        $query = $this->db->get();
        return $query->result();
    }

    function login($username, $password) {
        $this->db->select('*');
        $this->db->from('t_user');
        $this->db->where('username', $username);
        $this->db->where('password', MD5($password));
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

// ============== FILES ================
    function getDataFiles(){
        $this->db->select('a.*, b.id_employee, b.sn_employee, b.nama');
        $this->db->from('t_files AS a');
        $this->db->join('t_employee AS b','a.id_employee=b.id_employee','left');
        $query = $this->db->get();
        return $query->result();
    }
    function getDataFilesById($id){
        $this->db->select('a.*, b.id_employee, b.sn_employee, b.nama');
        $this->db->from('t_files AS a');
        $this->db->join('t_employee AS b','a.id_employee=b.id_employee','left');
        $this->db->where('a.id_files',$id);
        $query = $this->db->get();
        return $query->result();
    }

// ============== QUERY CHARTS ================
    function charts_area(){
        $this->db->select('a.id_project,b.kota as lokasi,count(*) as jml');
        $this->db->from('t_project AS a');
        $this->db->join('t_kota AS b','a.lokasi=b.id_kota','left');
        $this->db->group_by('kota');
        $query = $this->db->get();
        return $query->result();
    }

    function charts_customer(){
        $this->db->select('a.id_project,b.customer,count(*) as jml');
        $this->db->from('t_project AS a');
        $this->db->join('t_customer AS b','a.id_customer=b.id_customer','left');
        $this->db->group_by('customer');
        $query = $this->db->get();
        return $query->result();
    }

}